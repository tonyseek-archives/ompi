/*
  OMPi OpenMP Compiler
  == Copyright since 2001 the OMPi Team
  == Department of Computer Science, University of Ioannina

  This file is part of OMPi.

  OMPi is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  OMPi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OMPi; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* ort_barrier.c */

/* 
 * 2010/12/18:
 *   First time around.
 */
 
#include "ort_prive.h"


/* Is is assumed here that only 1 thread calls this (so as to avoid
 * expensive bookkeeping). Reinitializations cause no harm.
 */
void ort_default_barrier_init(ort_defbar_t *bar, int nthr)
{
  if (nthr > MAX_BAR_THREADS)
    ort_error(1, "barrier cannot support > %d threads; "
                 "change MAX_BAR_THREADS in ort.h\n", MAX_BAR_THREADS);
    
  bar->nthr = nthr;
  for (--nthr;  nthr >= 0; nthr--)
  {
    bar->arrived[nthr].value = 0;
    bar->arrived2[nthr].value = 0;
    bar->released[nthr].value = 0;
  }
}


void ort_default_barrier_destroy(ort_defbar_t *bar)
{
}


void task_barrier_wait(ort_defbar_t *bar, int eeid)
{
#if !defined(AVOID_OMPI_DEFAULT_TASKS)
  ort_eecb_t   *me = __MYCB;
  int time = 0; 
  
  if (eeid > 0)
  {
    bar->arrived2[eeid].value = 1;
    for (;(bar->arrived2[eeid].value == 1); time++)
    {
      ort_taskwait(1);
      
      if(time == BAR_YIELD)
      {
	time = -1;
	ee_yield();
      }
    }
    
    /* We check again to make sure that we searched once */
    ort_taskwait(1);
    
        
    bar->released[eeid].value = 1;
    for ( ; (bar->released[eeid].value == 1); time++)
      if (time == BAR_YIELD)
      {
        time = -1;
	ee_yield();
      }
  }
  else     /* Let the master do the work */
  {
    /* Wait for task completion */
    for (eeid = 1; eeid < bar->nthr; eeid++)
    {
      for ( ; (bar->arrived2[eeid].value == 0); time++)
	if (time == BAR_YIELD)
	{
	  ort_taskwait(1);
	  
	  time = -1;
	  ee_yield();
	}
	
      ort_taskwait(1);
    }
    
    /* No more tasks */
    me->parent->tasking.never_task = 0;

    /* Release my mates */
    for (eeid = 1; eeid < bar->nthr; eeid++)
    {
      bar->arrived[eeid].value = 0;
      bar->arrived2[eeid].value = 0;
    }
    
    /* Gather them again to ensure that all threads have executed tasks */
    for (eeid = 1; eeid < bar->nthr; eeid++)
      for ( ; (bar->released[eeid].value == 0); time++)
	if (time == BAR_YIELD)
	{
	  time = -1;
	  ee_yield();
	}
    
    /* Release them from barrier */
    for (eeid = 1; eeid < bar->nthr; eeid++)
      bar->released[eeid].value = 0;
    FENCE;
  }
#endif
}


void ort_default_barrier_wait(ort_defbar_t *bar, int eeid)
{
#if !defined(AVOID_OMPI_DEFAULT_TASKS)
  ort_eecb_t   *me = __MYCB;
  int time = 0;
  volatile int *task_exist = &(me->parent->tasking.never_task);
  
  if (*task_exist == 1)
    ort_execute_my_tasks(me);

  if (eeid > 0)
  {
    bar->arrived[eeid].value = 1;
    for ( ; (bar->arrived[eeid].value == 1); time++)
    {
      if(*task_exist == 1)
      {
	task_barrier_wait(bar, eeid);
	return;
      }
      
      if(time == BAR_YIELD)
      {
	time = -1;
	ee_yield();
      }
    }
    
    /* We check again to make sure that we searched once */
    if(*task_exist == 1)
    {
      task_barrier_wait(bar, eeid);
      return;
    }
    
    bar->released[eeid].value = 1;
    for ( ; (bar->released[eeid].value == 1); time++)
      if (time == BAR_YIELD)
      {
        time = -1;
	ee_yield();
      }
  }
  else     /* Let the master do the work */
  {
    /* Ensure that all my mates are in the barrier */
    for (eeid = 1; eeid < bar->nthr; eeid++)
    {
      for ( ; (bar->arrived[eeid].value == 0); time++)
      {  /* Try to help */
	if(*task_exist == 1)
	{
	  task_barrier_wait(bar, 0);
	  return;
	}

	if (time == BAR_YIELD)
	{
	  time = -1;
	  ee_yield();
	}
      }
      
      /* Try to help */
      if(*task_exist == 1)
      {
	task_barrier_wait(bar, 0);
	return;
      }
    }

    /* Release my mates */
    for (eeid = 1; eeid < bar->nthr; eeid++)
      bar->arrived[eeid].value = 0;
    
    /* Gather them again to ensure that all threads have checked for tasks */
    for (eeid = 1; eeid < bar->nthr; eeid++)
      for ( ; (bar->released[eeid].value == 0); time++)
	if (time == BAR_YIELD)
	{
	  time = -1;
	  ee_yield();
	}

    /* Release them from barrier */
    for (eeid = 1; eeid < bar->nthr; eeid++)
      bar->released[eeid].value = 0;
    FENCE;
  }
#endif
}


void ort_barrier_me(void)
{
  ort_eecb_t *me = __MYCB;
  if (me->num_siblings == 1)
    return;
  ee_barrier_wait(&me->parent->barrier, me->thread_num);
}


void parallel_barrier_wait(ort_defbar_t *bar, int eeid)
{
#if !defined(AVOID_OMPI_DEFAULT_TASKS)
  ort_eecb_t   *me = __MYCB;
  int time = 0;
  volatile int *task_exist = &(me->parent->tasking.never_task);

  if (*task_exist == 1)
    ort_execute_my_tasks(me);

  if (eeid > 0)
  {
    bar->arrived[eeid].value = 1;
    for (;(bar->arrived[eeid].value == 1); time++)
    {
      /* ort_taskwait must be called once only  */
      if(*task_exist == 1)
        ort_taskwait(1);
      
      if(time == BAR_YIELD)
      {
	time = -1;
	ee_yield();
      }
    }
    
    if(*task_exist == 1)
      ort_taskwait(1);
  }
  else     /* Let the master do the work */
  {
    /* Ensure that all my mates are in the barrier */
    for (eeid = 1; eeid < bar->nthr; eeid++)
    {
      for (;(bar->arrived[eeid].value == 0); time++)
      {
        /* Try to help */
        if(*task_exist == 1)
          ort_taskwait(1);
        
	if(time == BAR_YIELD)
	{
	  time = -1;
	  ee_yield();
	}
      }
      
      /* Try to help */
      if(*task_exist == 1)
        ort_taskwait(1);
    }

    /* Release my mates */
    for (eeid = 1; eeid < bar->nthr; eeid++)
      bar->arrived[eeid].value = 0;
    
    /* No more tasks */
    *task_exist = 0;
  }
#endif
}

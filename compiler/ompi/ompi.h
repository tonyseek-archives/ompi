/*
  OMPi OpenMP Compiler
  == Copyright since 2001 the OMPi Team
  == Department of Computer Science, University of Ioannina

  This file is part of OMPi.

  OMPi is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  OMPi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OMPi; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* ompi.h -- the core */

#ifndef __OMPI_H__
#define __OMPI_H__

#include "scanner.h"
#include "symtab.h"

extern int    enableOpenMP;  /* If 0, ignore OpenMP constructs */
extern int    enableOmpix;   /* Enable OMPi-extensions */
extern char   *filename;     /* The file we parse */
extern symtab stab;          /* The symbol table */
extern int    testingmode;   /* Internal tests */
extern int    processmode;   /* If 1, turn on process mode */
extern int    threadmode;

extern int    enableCodeDup; /* Duplicate code where appropriate for speed */

/* This is for taking care of main() in the parsed code;
 * OMPi generates its own main() and replaces the original one.
 */
extern char *MAIN_NEWNAME;   /* Rename the original main */
extern int  hasMainfunc;     /* 1 if main() function is defined in the file */
extern int  mainfuncRettype; /* 0 = int, 1 = void */
extern int  needMemcpy;      /* 1 if generated code includes memcpy() calls */
extern int  needLimits;      /* 1 if need limits.h constants (min/max) */
extern int  needFloat;       /* 1 if need float.h constants (min/max) */

/* These are implemented in parser.y
 */
extern aststmt parse_file(char *fname, int *error);
extern astexpr parse_expression_string(char *format, ...);
extern aststmt parse_blocklist_string(char *format, ...);
extern aststmt parse_and_declare_blocklist_string(char *format, ...);

/* Utilities
 */
extern void exit_error(int exitvalue, char *format, ...);
extern void warning(char *format, ...);

#endif


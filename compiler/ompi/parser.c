
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 1 "parser.y"

/*
  OMPi OpenMP Compiler
  == Copyright since 2001 the OMPi Team
  == Department of Computer Science, University of Ioannina

  This file is part of OMPi.

  OMPi is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  OMPi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OMPi; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* parser.y */

/* 
 * 2010/11/10:
 *   dropped OpenMP-specific for parsing; fewer rules, less code
 * 2009/05/11:
 *   added AUTO schedule type
 * 2009/05/03:
 *   added ATNODE ompix clause
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <stdarg.h>
#include <ctype.h>
#include <assert.h>
#include "scanner.h"
#include "ompi.h"
#include "ast.h"
#include "symtab.h"
#include "ast_free.h"
#include "ast_copy.h"
#include "ast_vars.h"
#include "x_arith.h"

void    check_uknown_var(char *name);
void    parse_error(int exitvalue, char *format, ...);
void    parse_warning(char *format, ...);
void    yyerror(char *s);
void    check_for_main_and_declare(astspec s, astdecl d);
void    add_declaration_links(astspec s, astdecl d);
astdecl fix_known_typename(astspec s);

aststmt pastree = NULL;       /* The generated AST */
aststmt pastree_stmt = NULL;  /* For when parsing statment strings */
astexpr pastree_expr = NULL;  /* For when parsing expression strings */
int     checkDecls = 1;       /* 0 when scanning strings (no context check) */
int     tempsave;
int     isTypedef  = 0;       /* To keep track of typedefs */

char    *parsingstring;       /* For error reporting when parsing string */


/* Line 189 of yacc.c  */
#line 143 "parser.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     START_SYMBOL_EXPRESSION = 258,
     START_SYMBOL_BLOCKLIST = 259,
     IDENTIFIER = 260,
     TYPE_NAME = 261,
     CONSTANT = 262,
     STRING_LITERAL = 263,
     PTR_OP = 264,
     INC_OP = 265,
     DEC_OP = 266,
     LEFT_OP = 267,
     RIGHT_OP = 268,
     LE_OP = 269,
     GE_OP = 270,
     EQ_OP = 271,
     NE_OP = 272,
     AND_OP = 273,
     OR_OP = 274,
     MUL_ASSIGN = 275,
     DIV_ASSIGN = 276,
     MOD_ASSIGN = 277,
     ADD_ASSIGN = 278,
     SUB_ASSIGN = 279,
     LEFT_ASSIGN = 280,
     RIGHT_ASSIGN = 281,
     AND_ASSIGN = 282,
     XOR_ASSIGN = 283,
     OR_ASSIGN = 284,
     SIZEOF = 285,
     TYPEDEF = 286,
     EXTERN = 287,
     STATIC = 288,
     AUTO = 289,
     REGISTER = 290,
     RESTRICT = 291,
     CHAR = 292,
     SHORT = 293,
     INT = 294,
     LONG = 295,
     SIGNED = 296,
     UNSIGNED = 297,
     FLOAT = 298,
     DOUBLE = 299,
     CONST = 300,
     VOLATILE = 301,
     VOID = 302,
     INLINE = 303,
     UBOOL = 304,
     UCOMPLEX = 305,
     UIMAGINARY = 306,
     STRUCT = 307,
     UNION = 308,
     ENUM = 309,
     ELLIPSIS = 310,
     CASE = 311,
     DEFAULT = 312,
     IF = 313,
     ELSE = 314,
     SWITCH = 315,
     WHILE = 316,
     DO = 317,
     FOR = 318,
     GOTO = 319,
     CONTINUE = 320,
     BREAK = 321,
     RETURN = 322,
     __BUILTIN_VA_ARG = 323,
     __BUILTIN_OFFSETOF = 324,
     __BUILTIN_TYPES_COMPATIBLE_P = 325,
     __ATTRIBUTE__ = 326,
     PRAGMA_OMP = 327,
     PRAGMA_OMP_THREADPRIVATE = 328,
     OMP_PARALLEL = 329,
     OMP_SECTIONS = 330,
     OMP_NOWAIT = 331,
     OMP_ORDERED = 332,
     OMP_SCHEDULE = 333,
     OMP_STATIC = 334,
     OMP_DYNAMIC = 335,
     OMP_GUIDED = 336,
     OMP_RUNTIME = 337,
     OMP_AUTO = 338,
     OMP_SECTION = 339,
     OMP_AFFINITY = 340,
     OMP_SINGLE = 341,
     OMP_MASTER = 342,
     OMP_CRITICAL = 343,
     OMP_BARRIER = 344,
     OMP_ATOMIC = 345,
     OMP_FLUSH = 346,
     OMP_PRIVATE = 347,
     OMP_FIRSTPRIVATE = 348,
     OMP_LASTPRIVATE = 349,
     OMP_SHARED = 350,
     OMP_DEFAULT = 351,
     OMP_NONE = 352,
     OMP_REDUCTION = 353,
     OMP_COPYIN = 354,
     OMP_NUMTHREADS = 355,
     OMP_COPYPRIVATE = 356,
     OMP_FOR = 357,
     OMP_IF = 358,
     OMP_TASK = 359,
     OMP_UNTIED = 360,
     OMP_TASKWAIT = 361,
     OMP_COLLAPSE = 362,
     OMP_FINAL = 363,
     OMP_MERGEABLE = 364,
     OMP_TASKYIELD = 365,
     OMP_READ = 366,
     OMP_WRITE = 367,
     OMP_CAPTURE = 368,
     OMP_UPDATE = 369,
     OMP_MIN = 370,
     OMP_MAX = 371,
     PRAGMA_OMPIX = 372,
     OMPIX_TASKDEF = 373,
     OMPIX_IN = 374,
     OMPIX_OUT = 375,
     OMPIX_INOUT = 376,
     OMPIX_TASKSYNC = 377,
     OMPIX_UPONRETURN = 378,
     OMPIX_ATNODE = 379,
     OMPIX_DETACHED = 380,
     OMPIX_ATWORKER = 381,
     OMPIX_TASKSCHEDULE = 382,
     OMPIX_STRIDE = 383,
     OMPIX_START = 384,
     OMPIX_SCOPE = 385,
     OMPIX_NODES = 386,
     OMPIX_WORKERS = 387,
     OMPIX_LOCAL = 388,
     OMPIX_GLOBAL = 389,
     OMPIX_TIED = 390
   };
#endif
/* Tokens.  */
#define START_SYMBOL_EXPRESSION 258
#define START_SYMBOL_BLOCKLIST 259
#define IDENTIFIER 260
#define TYPE_NAME 261
#define CONSTANT 262
#define STRING_LITERAL 263
#define PTR_OP 264
#define INC_OP 265
#define DEC_OP 266
#define LEFT_OP 267
#define RIGHT_OP 268
#define LE_OP 269
#define GE_OP 270
#define EQ_OP 271
#define NE_OP 272
#define AND_OP 273
#define OR_OP 274
#define MUL_ASSIGN 275
#define DIV_ASSIGN 276
#define MOD_ASSIGN 277
#define ADD_ASSIGN 278
#define SUB_ASSIGN 279
#define LEFT_ASSIGN 280
#define RIGHT_ASSIGN 281
#define AND_ASSIGN 282
#define XOR_ASSIGN 283
#define OR_ASSIGN 284
#define SIZEOF 285
#define TYPEDEF 286
#define EXTERN 287
#define STATIC 288
#define AUTO 289
#define REGISTER 290
#define RESTRICT 291
#define CHAR 292
#define SHORT 293
#define INT 294
#define LONG 295
#define SIGNED 296
#define UNSIGNED 297
#define FLOAT 298
#define DOUBLE 299
#define CONST 300
#define VOLATILE 301
#define VOID 302
#define INLINE 303
#define UBOOL 304
#define UCOMPLEX 305
#define UIMAGINARY 306
#define STRUCT 307
#define UNION 308
#define ENUM 309
#define ELLIPSIS 310
#define CASE 311
#define DEFAULT 312
#define IF 313
#define ELSE 314
#define SWITCH 315
#define WHILE 316
#define DO 317
#define FOR 318
#define GOTO 319
#define CONTINUE 320
#define BREAK 321
#define RETURN 322
#define __BUILTIN_VA_ARG 323
#define __BUILTIN_OFFSETOF 324
#define __BUILTIN_TYPES_COMPATIBLE_P 325
#define __ATTRIBUTE__ 326
#define PRAGMA_OMP 327
#define PRAGMA_OMP_THREADPRIVATE 328
#define OMP_PARALLEL 329
#define OMP_SECTIONS 330
#define OMP_NOWAIT 331
#define OMP_ORDERED 332
#define OMP_SCHEDULE 333
#define OMP_STATIC 334
#define OMP_DYNAMIC 335
#define OMP_GUIDED 336
#define OMP_RUNTIME 337
#define OMP_AUTO 338
#define OMP_SECTION 339
#define OMP_AFFINITY 340
#define OMP_SINGLE 341
#define OMP_MASTER 342
#define OMP_CRITICAL 343
#define OMP_BARRIER 344
#define OMP_ATOMIC 345
#define OMP_FLUSH 346
#define OMP_PRIVATE 347
#define OMP_FIRSTPRIVATE 348
#define OMP_LASTPRIVATE 349
#define OMP_SHARED 350
#define OMP_DEFAULT 351
#define OMP_NONE 352
#define OMP_REDUCTION 353
#define OMP_COPYIN 354
#define OMP_NUMTHREADS 355
#define OMP_COPYPRIVATE 356
#define OMP_FOR 357
#define OMP_IF 358
#define OMP_TASK 359
#define OMP_UNTIED 360
#define OMP_TASKWAIT 361
#define OMP_COLLAPSE 362
#define OMP_FINAL 363
#define OMP_MERGEABLE 364
#define OMP_TASKYIELD 365
#define OMP_READ 366
#define OMP_WRITE 367
#define OMP_CAPTURE 368
#define OMP_UPDATE 369
#define OMP_MIN 370
#define OMP_MAX 371
#define PRAGMA_OMPIX 372
#define OMPIX_TASKDEF 373
#define OMPIX_IN 374
#define OMPIX_OUT 375
#define OMPIX_INOUT 376
#define OMPIX_TASKSYNC 377
#define OMPIX_UPONRETURN 378
#define OMPIX_ATNODE 379
#define OMPIX_DETACHED 380
#define OMPIX_ATWORKER 381
#define OMPIX_TASKSCHEDULE 382
#define OMPIX_STRIDE 383
#define OMPIX_START 384
#define OMPIX_SCOPE 385
#define OMPIX_NODES 386
#define OMPIX_WORKERS 387
#define OMPIX_LOCAL 388
#define OMPIX_GLOBAL 389
#define OMPIX_TIED 390




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 70 "parser.y"

  char      name[2048];  /* A general string */
  int       type;        /* A general integer */
  char     *string;      /* A dynamically allocated string (only for 2 rules) */
  symbol    symb;        /* A symbol */
  astexpr   expr;        /* An expression node in the AST */
  astspec   spec;        /* A declaration specifier node in the AST */
  astdecl   decl;        /* A declarator node in the AST */
  aststmt   stmt;        /* A statement node in the AST */
  ompcon    ocon;        /* An OpenMP construct */
  ompdir    odir;        /* An OpenMP directive */
  ompclause ocla;        /* An OpenMP clause */

  oxcon     xcon;        /* OMPi extensions */
  oxdir     xdir;
  oxclause  xcla;



/* Line 214 of yacc.c  */
#line 469 "parser.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 481 "parser.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  162
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   2798

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  161
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  174
/* YYNRULES -- Number of rules.  */
#define YYNRULES  478
/* YYNRULES -- Number of states.  */
#define YYNSTATES  847

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   390

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     160,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   149,     2,     2,     2,   151,   144,     2,
     136,   137,   145,   146,   143,   147,   140,   150,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   157,   159,
     152,   158,   153,   156,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   138,     2,   139,   154,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   141,   155,   142,   148,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     8,    11,    13,    15,    18,    20,
      22,    24,    28,    30,    35,    39,    44,    48,    53,    57,
      61,    65,    69,    72,    75,    82,    90,    92,    96,    98,
     101,   104,   107,   110,   115,   122,   129,   136,   138,   140,
     142,   144,   146,   148,   150,   155,   157,   161,   165,   169,
     171,   175,   179,   181,   185,   189,   191,   195,   199,   203,
     207,   209,   213,   217,   219,   223,   225,   229,   231,   235,
     237,   241,   243,   247,   249,   255,   257,   261,   263,   265,
     267,   269,   271,   273,   275,   277,   279,   281,   283,   285,
     289,   291,   294,   298,   300,   302,   305,   307,   310,   312,
     315,   317,   320,   322,   326,   328,   329,   334,   336,   338,
     340,   342,   344,   346,   348,   350,   352,   354,   356,   358,
     360,   362,   364,   366,   368,   370,   372,   374,   379,   383,
     389,   395,   398,   401,   403,   405,   407,   410,   414,   417,
     419,   422,   424,   427,   429,   433,   435,   439,   442,   447,
     453,   459,   466,   469,   471,   475,   477,   481,   483,   485,
     487,   489,   491,   494,   496,   500,   504,   509,   514,   520,
     526,   533,   540,   545,   551,   556,   560,   565,   567,   570,
     573,   577,   579,   582,   584,   588,   590,   594,   597,   599,
     602,   604,   608,   610,   613,   615,   617,   620,   624,   627,
     631,   635,   640,   644,   649,   652,   656,   660,   665,   667,
     669,   673,   678,   680,   683,   687,   692,   695,   697,   700,
     704,   707,   710,   712,   714,   716,   718,   720,   722,   724,
     726,   730,   735,   739,   742,   743,   748,   750,   753,   755,
     757,   759,   761,   763,   766,   772,   780,   786,   792,   800,
     802,   809,   817,   825,   833,   842,   851,   860,   870,   877,
     885,   893,   902,   906,   909,   912,   915,   919,   921,   924,
     926,   928,   930,   932,   934,   935,   940,   941,   945,   946,
     952,   953,   958,   960,   963,   965,   967,   969,   971,   973,
     975,   977,   979,   981,   983,   985,   987,   989,   991,   993,
     995,   998,  1003,  1004,  1007,  1011,  1013,  1015,  1016,  1022,
    1023,  1029,  1032,  1037,  1038,  1041,  1045,  1047,  1049,  1051,
    1053,  1058,  1059,  1067,  1068,  1076,  1081,  1083,  1085,  1087,
    1089,  1091,  1093,  1096,  1101,  1102,  1105,  1109,  1111,  1113,
    1117,  1119,  1122,  1126,  1130,  1133,  1138,  1139,  1142,  1146,
    1148,  1150,  1153,  1159,  1160,  1163,  1167,  1169,  1171,  1173,
    1176,  1182,  1183,  1186,  1190,  1192,  1194,  1197,  1202,  1203,
    1206,  1210,  1212,  1214,  1215,  1221,  1223,  1224,  1230,  1232,
    1235,  1239,  1242,  1246,  1251,  1255,  1259,  1263,  1267,  1270,
    1274,  1279,  1284,  1289,  1293,  1298,  1299,  1304,  1307,  1311,
    1317,  1318,  1324,  1325,  1331,  1332,  1338,  1339,  1345,  1346,
    1352,  1357,  1362,  1363,  1371,  1372,  1378,  1380,  1382,  1384,
    1386,  1388,  1390,  1392,  1394,  1396,  1398,  1400,  1404,  1406,
    1410,  1412,  1414,  1418,  1419,  1425,  1426,  1429,  1433,  1438,
    1443,  1448,  1450,  1452,  1454,  1456,  1460,  1464,  1466,  1468,
    1469,  1474,  1477,  1478,  1484,  1485,  1488,  1492,  1497,  1502,
    1507,  1514,  1516,  1520,  1522,  1528,  1533,  1537,  1542,  1543,
    1546,  1550,  1555,  1560,  1565,  1567,  1569,  1571,  1575
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     162,     0,    -1,   233,    -1,     3,   184,    -1,     4,   226,
      -1,     5,    -1,     8,    -1,   164,     8,    -1,     5,    -1,
       7,    -1,   164,    -1,   136,   184,   137,    -1,   165,    -1,
     166,   138,   184,   139,    -1,     5,   136,   137,    -1,     5,
     136,   167,   137,    -1,   166,   136,   137,    -1,   166,   136,
     167,   137,    -1,   166,   140,     5,    -1,   166,     9,     5,
      -1,   166,   140,   216,    -1,   166,     9,   216,    -1,   166,
      10,    -1,   166,    11,    -1,   136,   213,   137,   141,   218,
     142,    -1,   136,   213,   137,   141,   218,   143,   142,    -1,
     182,    -1,   167,   143,   182,    -1,   166,    -1,    10,   168,
      -1,    11,   168,    -1,   169,   170,    -1,    30,   168,    -1,
      30,   136,   213,   137,    -1,    68,   136,   182,   143,   213,
     137,    -1,    69,   136,   213,   143,     5,   137,    -1,    70,
     136,   213,   143,   213,   137,    -1,   144,    -1,   145,    -1,
     146,    -1,   147,    -1,   148,    -1,   149,    -1,   168,    -1,
     136,   213,   137,   170,    -1,   170,    -1,   171,   145,   170,
      -1,   171,   150,   170,    -1,   171,   151,   170,    -1,   171,
      -1,   172,   146,   171,    -1,   172,   147,   171,    -1,   172,
      -1,   173,    12,   172,    -1,   173,    13,   172,    -1,   173,
      -1,   174,   152,   173,    -1,   174,   153,   173,    -1,   174,
      14,   173,    -1,   174,    15,   173,    -1,   174,    -1,   175,
      16,   174,    -1,   175,    17,   174,    -1,   175,    -1,   176,
     144,   175,    -1,   176,    -1,   177,   154,   176,    -1,   177,
      -1,   178,   155,   177,    -1,   178,    -1,   179,    18,   178,
      -1,   179,    -1,   180,    19,   179,    -1,   180,    -1,   180,
     156,   184,   157,   181,    -1,   181,    -1,   168,   183,   182,
      -1,   158,    -1,    20,    -1,    21,    -1,    22,    -1,    23,
      -1,    24,    -1,    25,    -1,    26,    -1,    27,    -1,    28,
      -1,    29,    -1,   182,    -1,   184,   143,   182,    -1,   181,
      -1,   187,   159,    -1,   187,   188,   159,    -1,   302,    -1,
     191,    -1,   191,   187,    -1,   192,    -1,   192,   187,    -1,
     203,    -1,   203,   187,    -1,   204,    -1,   204,   187,    -1,
     189,    -1,   188,   143,   189,    -1,   205,    -1,    -1,   205,
     158,   190,   217,    -1,    31,    -1,    32,    -1,    33,    -1,
      34,    -1,    35,    -1,    47,    -1,    37,    -1,    38,    -1,
      39,    -1,    40,    -1,    43,    -1,    44,    -1,    41,    -1,
      42,    -1,    49,    -1,    50,    -1,    51,    -1,   193,    -1,
     200,    -1,   216,    -1,   194,   141,   195,   142,    -1,   194,
     141,   142,    -1,   194,     5,   141,   195,   142,    -1,   194,
     216,   141,   195,   142,    -1,   194,     5,    -1,   194,   216,
      -1,    52,    -1,    53,    -1,   196,    -1,   195,   196,    -1,
     197,   198,   159,    -1,   197,   159,    -1,   192,    -1,   192,
     197,    -1,   203,    -1,   203,   197,    -1,   199,    -1,   198,
     143,   199,    -1,   205,    -1,   205,   157,   185,    -1,   157,
     185,    -1,    54,   141,   201,   142,    -1,    54,     5,   141,
     201,   142,    -1,    54,   141,   201,   143,   142,    -1,    54,
       5,   141,   201,   143,   142,    -1,    54,     5,    -1,   202,
      -1,   201,   143,   202,    -1,   163,    -1,   163,   158,   185,
      -1,    45,    -1,    36,    -1,    46,    -1,    48,    -1,   206,
      -1,   207,   206,    -1,     5,    -1,   136,   205,   137,    -1,
     206,   138,   139,    -1,   206,   138,   208,   139,    -1,   206,
     138,   182,   139,    -1,   206,   138,   208,   182,   139,    -1,
     206,   138,    33,   182,   139,    -1,   206,   138,    33,   208,
     182,   139,    -1,   206,   138,   208,    33,   182,   139,    -1,
     206,   138,   145,   139,    -1,   206,   138,   208,   145,   139,
      -1,   206,   136,   209,   137,    -1,   206,   136,   137,    -1,
     206,   136,   212,   137,    -1,   145,    -1,   145,   208,    -1,
     145,   207,    -1,   145,   208,   207,    -1,   203,    -1,   208,
     203,    -1,   210,    -1,   210,   143,    55,    -1,   211,    -1,
     210,   143,   211,    -1,   187,   205,    -1,   187,    -1,   187,
     214,    -1,     5,    -1,   212,   143,     5,    -1,   197,    -1,
     197,   214,    -1,   207,    -1,   215,    -1,   207,   215,    -1,
     136,   214,   137,    -1,   138,   139,    -1,   215,   138,   139,
      -1,   138,   182,   139,    -1,   215,   138,   182,   139,    -1,
     138,   145,   139,    -1,   215,   138,   145,   139,    -1,   136,
     137,    -1,   215,   136,   137,    -1,   136,   209,   137,    -1,
     215,   136,   209,   137,    -1,     6,    -1,   182,    -1,   141,
     218,   142,    -1,   141,   218,   143,   142,    -1,   217,    -1,
     219,   217,    -1,   218,   143,   217,    -1,   218,   143,   219,
     217,    -1,   220,   158,    -1,   221,    -1,   220,   221,    -1,
     138,   185,   139,    -1,   140,     5,    -1,   140,   216,    -1,
     223,    -1,   224,    -1,   228,    -1,   229,    -1,   230,    -1,
     232,    -1,   243,    -1,   321,    -1,     5,   157,   222,    -1,
      56,   185,   157,   222,    -1,    57,   157,   222,    -1,   141,
     142,    -1,    -1,   141,   225,   226,   142,    -1,   227,    -1,
     226,   227,    -1,   186,    -1,   222,    -1,   244,    -1,   314,
      -1,   159,    -1,   184,   159,    -1,    58,   136,   184,   137,
     222,    -1,    58,   136,   184,   137,   222,    59,   222,    -1,
      60,   136,   184,   137,   222,    -1,    61,   136,   184,   137,
     222,    -1,    62,   222,    61,   136,   184,   137,   159,    -1,
     231,    -1,    63,   136,   159,   159,   137,   222,    -1,    63,
     136,   184,   159,   159,   137,   222,    -1,    63,   136,   159,
     184,   159,   137,   222,    -1,    63,   136,   159,   159,   184,
     137,   222,    -1,    63,   136,   184,   159,   184,   159,   137,
     222,    -1,    63,   136,   184,   159,   159,   184,   137,   222,
      -1,    63,   136,   159,   184,   159,   184,   137,   222,    -1,
      63,   136,   184,   159,   184,   159,   184,   137,   222,    -1,
      63,   136,   186,   159,   137,   222,    -1,    63,   136,   186,
     184,   159,   137,   222,    -1,    63,   136,   186,   159,   184,
     137,   222,    -1,    63,   136,   186,   184,   159,   184,   137,
     222,    -1,    64,     5,   159,    -1,    65,   159,    -1,    66,
     159,    -1,    67,   159,    -1,    67,   184,   159,    -1,   234,
      -1,   233,   234,    -1,   235,    -1,   186,    -1,   322,    -1,
     236,    -1,   239,    -1,    -1,   187,   205,   237,   224,    -1,
      -1,   205,   238,   224,    -1,    -1,   187,   205,   240,   242,
     224,    -1,    -1,   205,   241,   242,   224,    -1,   186,    -1,
     242,   186,    -1,   246,    -1,   253,    -1,   261,    -1,   268,
      -1,   272,    -1,   276,    -1,   287,    -1,   289,    -1,   295,
      -1,   300,    -1,   280,    -1,   294,    -1,   297,    -1,   292,
      -1,   293,    -1,   222,    -1,   247,   245,    -1,    72,    74,
     248,   160,    -1,    -1,   248,   249,    -1,   248,   143,   249,
      -1,   250,    -1,   303,    -1,    -1,   103,   136,   251,   184,
     137,    -1,    -1,   100,   136,   252,   184,   137,    -1,   254,
     231,    -1,    72,   102,   255,   160,    -1,    -1,   255,   256,
      -1,   255,   143,   256,    -1,   257,    -1,   303,    -1,    76,
      -1,    77,    -1,    78,   136,   260,   137,    -1,    -1,    78,
     136,   260,   143,   258,   184,   137,    -1,    -1,    78,   136,
      85,   143,   259,   184,   137,    -1,   107,   136,   184,   137,
      -1,    79,    -1,    80,    -1,    81,    -1,    82,    -1,    83,
      -1,     1,    -1,   262,   265,    -1,    72,    75,   263,   160,
      -1,    -1,   263,   264,    -1,   263,   143,   264,    -1,   303,
      -1,    76,    -1,   141,   266,   142,    -1,   245,    -1,   267,
     245,    -1,   266,   267,   245,    -1,    72,    84,   160,    -1,
     269,   245,    -1,    72,    86,   270,   160,    -1,    -1,   270,
     271,    -1,   270,   143,   271,    -1,   303,    -1,    76,    -1,
     273,   231,    -1,    72,    74,   102,   274,   160,    -1,    -1,
     274,   275,    -1,   274,   143,   275,    -1,   250,    -1,   257,
      -1,   303,    -1,   277,   265,    -1,    72,    74,    75,   278,
     160,    -1,    -1,   278,   279,    -1,   278,   143,   279,    -1,
     250,    -1,   303,    -1,   281,   245,    -1,    72,   104,   282,
     160,    -1,    -1,   282,   283,    -1,   282,   143,   283,    -1,
     284,    -1,   303,    -1,    -1,   103,   136,   285,   184,   137,
      -1,   105,    -1,    -1,   108,   136,   286,   184,   137,    -1,
     109,    -1,   288,   245,    -1,    72,    87,   160,    -1,   290,
     245,    -1,    72,    88,   160,    -1,    72,    88,   291,   160,
      -1,   136,     5,   137,    -1,    72,   106,   160,    -1,    72,
     110,   160,    -1,    72,    89,   160,    -1,   296,   228,    -1,
      72,    90,   160,    -1,    72,    90,   112,   160,    -1,    72,
      90,   111,   160,    -1,    72,    90,   114,   160,    -1,    72,
      91,   160,    -1,    72,    91,   298,   160,    -1,    -1,   136,
     299,   312,   137,    -1,   301,   245,    -1,    72,    77,   160,
      -1,    73,   136,   313,   137,   160,    -1,    -1,    92,   304,
     136,   312,   137,    -1,    -1,   101,   305,   136,   312,   137,
      -1,    -1,    93,   306,   136,   312,   137,    -1,    -1,    94,
     307,   136,   312,   137,    -1,    -1,    95,   308,   136,   312,
     137,    -1,    96,   136,    95,   137,    -1,    96,   136,    97,
     137,    -1,    -1,    98,   136,   311,   309,   157,   312,   137,
      -1,    -1,    99,   310,   136,   312,   137,    -1,   146,    -1,
     145,    -1,   147,    -1,   144,    -1,   154,    -1,   155,    -1,
      18,    -1,    19,    -1,   115,    -1,   116,    -1,     5,    -1,
     312,   143,     5,    -1,     5,    -1,   313,   143,     5,    -1,
     315,    -1,   316,    -1,   117,   122,   160,    -1,    -1,   117,
     127,   317,   318,   160,    -1,    -1,   318,   319,    -1,   318,
     143,   319,    -1,   128,   136,   182,   137,    -1,   129,   136,
     182,   137,    -1,   130,   136,   320,   137,    -1,   135,    -1,
     105,    -1,   131,    -1,   132,    -1,   132,   143,   134,    -1,
     132,   143,   133,    -1,   322,    -1,   330,    -1,    -1,   324,
     236,   323,   224,    -1,   324,   236,    -1,    -1,   117,   118,
     325,   326,   160,    -1,    -1,   326,   327,    -1,   326,   143,
     327,    -1,   119,   136,   328,   137,    -1,   120,   136,   328,
     137,    -1,   121,   136,   328,   137,    -1,    98,   136,   311,
     157,   328,   137,    -1,   329,    -1,   328,   143,   329,    -1,
       5,    -1,     5,   138,   156,     5,   139,    -1,     5,   138,
     182,   139,    -1,   331,   334,   159,    -1,   117,   104,   332,
     160,    -1,    -1,   332,   333,    -1,   332,   143,   333,    -1,
     124,   136,   145,   137,    -1,   124,   136,   182,   137,    -1,
     126,   136,   182,   137,    -1,   135,    -1,   105,    -1,   125,
      -1,     5,   136,   137,    -1,     5,   136,   167,   137,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   307,   307,   308,   309,   326,   347,   351,   374,   397,
     401,   405,   413,   417,   427,   434,   441,   445,   449,   453,
     462,   466,   470,   474,   478,   482,   490,   494,   502,   506,
     510,   514,   521,   525,   534,   539,   544,   553,   557,   561,
     565,   569,   573,   581,   585,   593,   597,   601,   605,   613,
     617,   621,   629,   633,   637,   645,   649,   653,   657,   661,
     669,   673,   677,   685,   689,   697,   701,   709,   713,   721,
     725,   733,   737,   745,   749,   757,   761,   769,   773,   777,
     781,   785,   789,   793,   797,   801,   805,   809,   817,   821,
     829,   843,   858,   865,   873,   877,   881,   885,   889,   893,
     897,   901,   909,   913,   927,   944,   943,   966,   971,   975,
     979,   983,   991,   995,   999,  1003,  1007,  1011,  1015,  1019,
    1023,  1027,  1031,  1035,  1039,  1043,  1047,  1055,  1059,  1063,
    1078,  1085,  1092,  1103,  1107,  1115,  1119,  1127,  1131,  1139,
    1143,  1147,  1151,  1159,  1163,  1171,  1175,  1179,  1187,  1191,
    1203,  1207,  1219,  1231,  1235,  1243,  1247,  1255,  1259,  1263,
    1271,  1279,  1283,  1296,  1300,  1308,  1312,  1316,  1320,  1324,
    1328,  1332,  1336,  1340,  1344,  1348,  1352,  1360,  1364,  1368,
    1372,  1380,  1384,  1392,  1396,  1404,  1408,  1416,  1420,  1424,
    1432,  1436,  1444,  1448,  1456,  1460,  1464,  1472,  1476,  1480,
    1484,  1488,  1492,  1496,  1500,  1504,  1508,  1512,  1520,  1528,
    1532,  1536,  1544,  1548,  1552,  1556,  1564,  1572,  1576,  1584,
    1588,  1592,  1606,  1610,  1614,  1618,  1622,  1626,  1630,  1635,
    1644,  1648,  1652,  1660,  1664,  1664,  1675,  1679,  1688,  1692,
    1696,  1701,  1710,  1714,  1723,  1727,  1731,  1740,  1744,  1748,
    1752,  1756,  1760,  1764,  1768,  1772,  1776,  1780,  1784,  1788,
    1792,  1796,  1804,  1809,  1813,  1817,  1821,  1835,  1839,  1847,
    1851,  1858,  1870,  1871,  1876,  1875,  1892,  1891,  1913,  1912,
    1931,  1930,  1954,  1958,  1971,  1975,  1979,  1983,  1987,  1991,
    1995,  1999,  2003,  2007,  2012,  2028,  2032,  2037,  2042,  2049,
    2056,  2064,  2072,  2075,  2079,  2086,  2090,  2097,  2097,  2102,
    2102,  2110,  2117,  2125,  2128,  2132,  2139,  2143,  2147,  2154,
    2158,  2162,  2162,  2170,  2169,  2181,  2196,  2200,  2204,  2208,
    2212,  2216,  2221,  2228,  2236,  2239,  2243,  2250,  2254,  2261,
    2268,  2273,  2277,  2284,  2291,  2298,  2306,  2309,  2313,  2320,
    2324,  2331,  2338,  2346,  2349,  2353,  2360,  2364,  2368,  2375,
    2382,  2390,  2393,  2397,  2404,  2408,  2416,  2425,  2434,  2437,
    2441,  2449,  2453,  2461,  2461,  2466,  2470,  2470,  2475,  2481,
    2488,  2495,  2502,  2506,  2513,  2521,  2529,  2536,  2543,  2550,
    2554,  2558,  2562,  2569,  2573,  2580,  2580,  2588,  2595,  2602,
    2609,  2609,  2615,  2615,  2620,  2620,  2625,  2625,  2630,  2630,
    2635,  2639,  2643,  2643,  2648,  2648,  2656,  2660,  2664,  2668,
    2672,  2676,  2680,  2684,  2688,  2692,  2699,  2706,  2721,  2739,
    2765,  2769,  2777,  2785,  2784,  2797,  2800,  2804,  2811,  2815,
    2819,  2823,  2827,  2834,  2838,  2842,  2846,  2853,  2857,  2866,
    2865,  2882,  2891,  2890,  2903,  2906,  2910,  2917,  2921,  2925,
    2929,  2936,  2940,  2947,  2952,  2960,  2968,  2976,  2984,  2987,
    2991,  2998,  3002,  3006,  3010,  3014,  3018,  3025,  3031
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "START_SYMBOL_EXPRESSION",
  "START_SYMBOL_BLOCKLIST", "IDENTIFIER", "TYPE_NAME", "CONSTANT",
  "STRING_LITERAL", "PTR_OP", "INC_OP", "DEC_OP", "LEFT_OP", "RIGHT_OP",
  "LE_OP", "GE_OP", "EQ_OP", "NE_OP", "AND_OP", "OR_OP", "MUL_ASSIGN",
  "DIV_ASSIGN", "MOD_ASSIGN", "ADD_ASSIGN", "SUB_ASSIGN", "LEFT_ASSIGN",
  "RIGHT_ASSIGN", "AND_ASSIGN", "XOR_ASSIGN", "OR_ASSIGN", "SIZEOF",
  "TYPEDEF", "EXTERN", "STATIC", "AUTO", "REGISTER", "RESTRICT", "CHAR",
  "SHORT", "INT", "LONG", "SIGNED", "UNSIGNED", "FLOAT", "DOUBLE", "CONST",
  "VOLATILE", "VOID", "INLINE", "UBOOL", "UCOMPLEX", "UIMAGINARY",
  "STRUCT", "UNION", "ENUM", "ELLIPSIS", "CASE", "DEFAULT", "IF", "ELSE",
  "SWITCH", "WHILE", "DO", "FOR", "GOTO", "CONTINUE", "BREAK", "RETURN",
  "__BUILTIN_VA_ARG", "__BUILTIN_OFFSETOF", "__BUILTIN_TYPES_COMPATIBLE_P",
  "__ATTRIBUTE__", "PRAGMA_OMP", "PRAGMA_OMP_THREADPRIVATE",
  "OMP_PARALLEL", "OMP_SECTIONS", "OMP_NOWAIT", "OMP_ORDERED",
  "OMP_SCHEDULE", "OMP_STATIC", "OMP_DYNAMIC", "OMP_GUIDED", "OMP_RUNTIME",
  "OMP_AUTO", "OMP_SECTION", "OMP_AFFINITY", "OMP_SINGLE", "OMP_MASTER",
  "OMP_CRITICAL", "OMP_BARRIER", "OMP_ATOMIC", "OMP_FLUSH", "OMP_PRIVATE",
  "OMP_FIRSTPRIVATE", "OMP_LASTPRIVATE", "OMP_SHARED", "OMP_DEFAULT",
  "OMP_NONE", "OMP_REDUCTION", "OMP_COPYIN", "OMP_NUMTHREADS",
  "OMP_COPYPRIVATE", "OMP_FOR", "OMP_IF", "OMP_TASK", "OMP_UNTIED",
  "OMP_TASKWAIT", "OMP_COLLAPSE", "OMP_FINAL", "OMP_MERGEABLE",
  "OMP_TASKYIELD", "OMP_READ", "OMP_WRITE", "OMP_CAPTURE", "OMP_UPDATE",
  "OMP_MIN", "OMP_MAX", "PRAGMA_OMPIX", "OMPIX_TASKDEF", "OMPIX_IN",
  "OMPIX_OUT", "OMPIX_INOUT", "OMPIX_TASKSYNC", "OMPIX_UPONRETURN",
  "OMPIX_ATNODE", "OMPIX_DETACHED", "OMPIX_ATWORKER", "OMPIX_TASKSCHEDULE",
  "OMPIX_STRIDE", "OMPIX_START", "OMPIX_SCOPE", "OMPIX_NODES",
  "OMPIX_WORKERS", "OMPIX_LOCAL", "OMPIX_GLOBAL", "OMPIX_TIED", "'('",
  "')'", "'['", "']'", "'.'", "'{'", "'}'", "','", "'&'", "'*'", "'+'",
  "'-'", "'~'", "'!'", "'/'", "'%'", "'<'", "'>'", "'^'", "'|'", "'?'",
  "':'", "'='", "';'", "'\\n'", "$accept", "start_trick",
  "enumeration_constant", "string_literal", "primary_expression",
  "postfix_expression", "argument_expression_list", "unary_expression",
  "unary_operator", "cast_expression", "multiplicative_expression",
  "additive_expression", "shift_expression", "relational_expression",
  "equality_expression", "AND_expression", "exclusive_OR_expression",
  "inclusive_OR_expression", "logical_AND_expression",
  "logical_OR_expression", "conditional_expression",
  "assignment_expression", "assignment_operator", "expression",
  "constant_expression", "declaration", "declaration_specifiers",
  "init_declarator_list", "init_declarator", "$@1",
  "storage_class_specifier", "type_specifier", "struct_or_union_specifier",
  "struct_or_union", "struct_declaration_list", "struct_declaration",
  "specifier_qualifier_list", "struct_declarator_list",
  "struct_declarator", "enum_specifier", "enumerator_list", "enumerator",
  "type_qualifier", "function_specifier", "declarator",
  "direct_declarator", "pointer", "type_qualifier_list",
  "parameter_type_list", "parameter_list", "parameter_declaration",
  "identifier_list", "type_name", "abstract_declarator",
  "direct_abstract_declarator", "typedef_name", "initializer",
  "initializer_list", "designation", "designator_list", "designator",
  "statement", "labeled_statement", "compound_statement", "@2",
  "block_item_list", "block_item", "expression_statement",
  "selection_statement", "iteration_statement", "iteration_statement_for",
  "jump_statement", "translation_unit", "external_declaration",
  "function_definition", "normal_function_definition", "$@3", "$@4",
  "oldstyle_function_definition", "$@5", "$@6", "declaration_list",
  "openmp_construct", "openmp_directive", "structured_block",
  "parallel_construct", "parallel_directive", "parallel_clause_optseq",
  "parallel_clause", "unique_parallel_clause", "$@7", "$@8",
  "for_construct", "for_directive", "for_clause_optseq", "for_clause",
  "unique_for_clause", "$@9", "$@10", "schedule_kind",
  "sections_construct", "sections_directive", "sections_clause_optseq",
  "sections_clause", "section_scope", "section_sequence",
  "section_directive", "single_construct", "single_directive",
  "single_clause_optseq", "single_clause", "parallel_for_construct",
  "parallel_for_directive", "parallel_for_clause_optseq",
  "parallel_for_clause", "parallel_sections_construct",
  "parallel_sections_directive", "parallel_sections_clause_optseq",
  "parallel_sections_clause", "task_construct", "task_directive",
  "task_clause_optseq", "task_clause", "unique_task_clause", "$@11",
  "$@12", "master_construct", "master_directive", "critical_construct",
  "critical_directive", "region_phrase", "taskwait_directive",
  "taskyield_directive", "barrier_directive", "atomic_construct",
  "atomic_directive", "flush_directive", "flush_vars", "$@13",
  "ordered_construct", "ordered_directive", "threadprivate_directive",
  "data_clause", "$@14", "$@15", "$@16", "$@17", "$@18", "$@19", "$@20",
  "reduction_operator", "variable_list", "thrprv_variable_list",
  "ompix_directive", "ox_tasksync_directive", "ox_taskschedule_directive",
  "$@21", "ox_taskschedule_clause_optseq", "ox_taskschedule_clause",
  "ox_scope_spec", "ompix_construct", "ox_taskdef_construct", "$@22",
  "ox_taskdef_directive", "$@23", "ox_taskdef_clause_optseq",
  "ox_taskdef_clause", "ox_variable_size_list", "ox_variable_size_elem",
  "ox_task_construct", "ox_task_directive", "ox_task_clause_optseq",
  "ox_task_clause", "ox_funccall_expression", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,    40,    41,    91,    93,
      46,   123,   125,    44,    38,    42,    43,    45,   126,    33,
      47,    37,    60,    62,    94,   124,    63,    58,    61,    59,
      10
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   161,   162,   162,   162,   163,   164,   164,   165,   165,
     165,   165,   166,   166,   166,   166,   166,   166,   166,   166,
     166,   166,   166,   166,   166,   166,   167,   167,   168,   168,
     168,   168,   168,   168,   168,   168,   168,   169,   169,   169,
     169,   169,   169,   170,   170,   171,   171,   171,   171,   172,
     172,   172,   173,   173,   173,   174,   174,   174,   174,   174,
     175,   175,   175,   176,   176,   177,   177,   178,   178,   179,
     179,   180,   180,   181,   181,   182,   182,   183,   183,   183,
     183,   183,   183,   183,   183,   183,   183,   183,   184,   184,
     185,   186,   186,   186,   187,   187,   187,   187,   187,   187,
     187,   187,   188,   188,   189,   190,   189,   191,   191,   191,
     191,   191,   192,   192,   192,   192,   192,   192,   192,   192,
     192,   192,   192,   192,   192,   192,   192,   193,   193,   193,
     193,   193,   193,   194,   194,   195,   195,   196,   196,   197,
     197,   197,   197,   198,   198,   199,   199,   199,   200,   200,
     200,   200,   200,   201,   201,   202,   202,   203,   203,   203,
     204,   205,   205,   206,   206,   206,   206,   206,   206,   206,
     206,   206,   206,   206,   206,   206,   206,   207,   207,   207,
     207,   208,   208,   209,   209,   210,   210,   211,   211,   211,
     212,   212,   213,   213,   214,   214,   214,   215,   215,   215,
     215,   215,   215,   215,   215,   215,   215,   215,   216,   217,
     217,   217,   218,   218,   218,   218,   219,   220,   220,   221,
     221,   221,   222,   222,   222,   222,   222,   222,   222,   222,
     223,   223,   223,   224,   225,   224,   226,   226,   227,   227,
     227,   227,   228,   228,   229,   229,   229,   230,   230,   230,
     231,   231,   231,   231,   231,   231,   231,   231,   231,   231,
     231,   231,   232,   232,   232,   232,   232,   233,   233,   234,
     234,   234,   235,   235,   237,   236,   238,   236,   240,   239,
     241,   239,   242,   242,   243,   243,   243,   243,   243,   243,
     243,   243,   243,   243,   243,   244,   244,   244,   244,   245,
     246,   247,   248,   248,   248,   249,   249,   251,   250,   252,
     250,   253,   254,   255,   255,   255,   256,   256,   256,   257,
     257,   258,   257,   259,   257,   257,   260,   260,   260,   260,
     260,   260,   261,   262,   263,   263,   263,   264,   264,   265,
     266,   266,   266,   267,   268,   269,   270,   270,   270,   271,
     271,   272,   273,   274,   274,   274,   275,   275,   275,   276,
     277,   278,   278,   278,   279,   279,   280,   281,   282,   282,
     282,   283,   283,   285,   284,   284,   286,   284,   284,   287,
     288,   289,   290,   290,   291,   292,   293,   294,   295,   296,
     296,   296,   296,   297,   297,   299,   298,   300,   301,   302,
     304,   303,   305,   303,   306,   303,   307,   303,   308,   303,
     303,   303,   309,   303,   310,   303,   311,   311,   311,   311,
     311,   311,   311,   311,   311,   311,   312,   312,   313,   313,
     314,   314,   315,   317,   316,   318,   318,   318,   319,   319,
     319,   319,   319,   320,   320,   320,   320,   321,   321,   323,
     322,   322,   325,   324,   326,   326,   326,   327,   327,   327,
     327,   328,   328,   329,   329,   329,   330,   331,   332,   332,
     332,   333,   333,   333,   333,   333,   333,   334,   334
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     2,     1,     1,     2,     1,     1,
       1,     3,     1,     4,     3,     4,     3,     4,     3,     3,
       3,     3,     2,     2,     6,     7,     1,     3,     1,     2,
       2,     2,     2,     4,     6,     6,     6,     1,     1,     1,
       1,     1,     1,     1,     4,     1,     3,     3,     3,     1,
       3,     3,     1,     3,     3,     1,     3,     3,     3,     3,
       1,     3,     3,     1,     3,     1,     3,     1,     3,     1,
       3,     1,     3,     1,     5,     1,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     3,
       1,     2,     3,     1,     1,     2,     1,     2,     1,     2,
       1,     2,     1,     3,     1,     0,     4,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     4,     3,     5,
       5,     2,     2,     1,     1,     1,     2,     3,     2,     1,
       2,     1,     2,     1,     3,     1,     3,     2,     4,     5,
       5,     6,     2,     1,     3,     1,     3,     1,     1,     1,
       1,     1,     2,     1,     3,     3,     4,     4,     5,     5,
       6,     6,     4,     5,     4,     3,     4,     1,     2,     2,
       3,     1,     2,     1,     3,     1,     3,     2,     1,     2,
       1,     3,     1,     2,     1,     1,     2,     3,     2,     3,
       3,     4,     3,     4,     2,     3,     3,     4,     1,     1,
       3,     4,     1,     2,     3,     4,     2,     1,     2,     3,
       2,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     4,     3,     2,     0,     4,     1,     2,     1,     1,
       1,     1,     1,     2,     5,     7,     5,     5,     7,     1,
       6,     7,     7,     7,     8,     8,     8,     9,     6,     7,
       7,     8,     3,     2,     2,     2,     3,     1,     2,     1,
       1,     1,     1,     1,     0,     4,     0,     3,     0,     5,
       0,     4,     1,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       2,     4,     0,     2,     3,     1,     1,     0,     5,     0,
       5,     2,     4,     0,     2,     3,     1,     1,     1,     1,
       4,     0,     7,     0,     7,     4,     1,     1,     1,     1,
       1,     1,     2,     4,     0,     2,     3,     1,     1,     3,
       1,     2,     3,     3,     2,     4,     0,     2,     3,     1,
       1,     2,     5,     0,     2,     3,     1,     1,     1,     2,
       5,     0,     2,     3,     1,     1,     2,     4,     0,     2,
       3,     1,     1,     0,     5,     1,     0,     5,     1,     2,
       3,     2,     3,     4,     3,     3,     3,     3,     2,     3,
       4,     4,     4,     3,     4,     0,     4,     2,     3,     5,
       0,     5,     0,     5,     0,     5,     0,     5,     0,     5,
       4,     4,     0,     7,     0,     5,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     3,     1,     3,
       1,     1,     3,     0,     5,     0,     2,     3,     4,     4,
       4,     1,     1,     1,     1,     3,     3,     1,     1,     0,
       4,     2,     0,     5,     0,     2,     3,     4,     4,     4,
       6,     1,     3,     1,     5,     4,     3,     4,     0,     2,
       3,     4,     4,     4,     1,     1,     1,     3,     4
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       0,     0,     0,   163,   208,   107,   108,   109,   110,   111,
     158,   113,   114,   115,   116,   119,   120,   117,   118,   157,
     159,   112,   160,   121,   122,   123,   133,   134,     0,     0,
       0,     0,   177,     0,   270,     0,    94,    96,   124,     0,
     125,    98,   100,   280,   161,     0,   126,     2,   267,   269,
     272,   273,    93,   271,     0,     8,     9,     6,     0,     0,
       0,     0,     0,     0,     0,    37,    38,    39,    40,    41,
      42,    10,    12,    28,    43,     0,    45,    49,    52,    55,
      60,    63,    65,    67,    69,    71,    73,    75,    88,     3,
       8,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   234,   242,     0,   238,     0,   239,
     222,   223,     4,   236,   224,   225,   226,   249,   227,   228,
     240,   284,     0,   285,     0,   286,     0,   287,     0,   288,
       0,   289,     0,   294,     0,   290,     0,   291,     0,   297,
     298,   295,   292,     0,   296,   293,     0,   241,   430,   431,
     229,   447,   448,     0,   152,     0,     0,   452,     0,   181,
     179,   178,     1,    91,     0,   102,   278,    95,    97,   131,
       0,   132,    99,   101,     0,     0,     0,     0,   162,   268,
       0,   276,   451,     0,     0,    29,    30,     0,    32,     0,
       0,     0,     0,   139,   192,   141,     0,     7,     0,    22,
      23,     0,     0,     0,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    77,     0,    43,    31,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    90,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     263,   264,   265,     0,   302,   334,     0,   346,     0,     0,
       0,     0,     0,   313,   368,     0,     0,   468,     0,   433,
     233,     0,   243,   104,   237,   299,   300,   311,     0,   332,
     344,   351,   359,   366,   379,   381,   388,   397,     0,     0,
       0,     5,   155,     0,   153,   428,     0,   454,   164,   182,
     180,     0,    92,   105,     0,     0,     0,   128,     0,   135,
       0,     0,   277,   282,     0,   190,   175,   188,     0,   183,
     185,     0,     0,   165,    38,     0,     0,   274,     0,    14,
       0,    26,     0,     0,     0,     0,     0,    11,   140,     0,
       0,   194,   193,   195,   142,     0,    19,    21,    16,     0,
       0,    18,    20,    76,    46,    47,    48,    50,    51,    53,
      54,    58,    59,    56,    57,    61,    62,    64,    66,    68,
      70,    72,     0,    89,   230,     0,   232,     0,     0,     0,
       0,     0,     0,     0,   262,   266,   361,   353,     0,     0,
     398,     0,   380,     0,   382,     0,   387,     0,     0,     0,
     389,   395,   393,     0,     0,     0,   385,   386,     0,   432,
     435,     0,     0,   340,     0,     0,     0,   466,     0,     0,
     148,     0,     0,     0,     0,   103,     0,   275,     0,     0,
     127,   136,     0,   138,     0,   143,   145,     0,   283,   281,
       0,   187,   194,   189,   174,     0,   176,     0,     0,     0,
     172,   167,     0,   166,    38,     0,   450,    15,     0,     0,
      33,     0,     0,     0,   204,     0,     0,   198,    38,     0,
     196,     0,     0,     0,    44,    17,    13,     0,   231,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     400,   404,   406,   408,     0,     0,   414,     0,   402,     0,
       0,   301,   303,   305,   306,   338,     0,   333,   335,   337,
     350,     0,   345,   347,   349,     0,   383,   391,   390,   392,
       0,   394,   318,   319,     0,     0,     0,   312,   314,   316,
     317,     0,   375,     0,   378,     0,   367,   369,   371,   372,
     475,     0,   476,     0,   474,     0,   467,   469,     0,   235,
       0,     0,   339,     0,   341,   477,     0,   149,     0,   156,
     150,   154,   399,   429,     0,     0,     0,     0,     0,   453,
     455,     0,   209,   106,   279,   129,   147,     0,   137,     0,
     130,   184,   186,   191,   169,     0,     0,   173,   168,    27,
       0,     0,     0,   206,   197,   202,   200,   205,     0,   199,
      38,     0,     0,     0,   212,     0,     0,     0,   217,    74,
     244,   246,   247,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   360,   364,   362,   365,     0,   352,   356,
     357,   354,   358,     0,     0,     0,     0,     0,     0,     0,
     309,     0,   307,   304,   336,   348,   384,   426,     0,     0,
       0,   315,   373,   376,   370,     0,     0,   470,   442,     0,
       0,     0,   441,     0,   434,   436,   343,   342,   478,   151,
       0,     0,     0,     0,   456,     0,   144,   146,   170,   171,
      34,    35,    36,   207,   203,   201,     0,   220,   221,    24,
       0,   213,   216,   218,     0,     0,   250,     0,     0,     0,
       0,     0,     0,   258,     0,     0,     0,   363,   355,     0,
       0,     0,     0,     0,     0,   422,   423,   424,   425,   419,
     417,   416,   418,   420,   421,   412,     0,     0,     0,     0,
     396,     0,   331,   326,   327,   328,   329,   330,     0,     0,
       0,     0,     0,    38,     0,     0,     0,     0,     0,   437,
       0,   463,     0,   461,     0,     0,   210,     0,   219,    25,
     214,     0,   245,   248,   253,   252,     0,   251,     0,     0,
       0,   260,   259,     0,     0,     0,     0,     0,   410,   411,
       0,     0,     0,     0,     0,   427,   323,   320,   321,   325,
       0,     0,   471,   472,   473,     0,     0,   443,   444,     0,
       0,     0,   457,     0,   458,   459,   211,   215,   256,   255,
     254,     0,   261,   401,   405,   407,   409,     0,   415,   310,
     403,   308,     0,     0,   374,   377,   438,   439,     0,   440,
       0,     0,     0,   462,   257,     0,     0,     0,   446,   445,
     460,     0,   465,   413,   324,   322,   464
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    33,   292,    71,    72,    73,   330,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,   215,   106,   240,   107,   108,   164,   165,   426,
      36,    37,    38,    39,   308,   309,   194,   434,   435,    40,
     293,   294,    41,    42,    43,    44,    45,   161,   465,   319,
     320,   321,   196,   466,   343,    46,   604,   605,   606,   607,
     608,   275,   110,   111,   271,   112,   113,   114,   115,   116,
     117,   118,    47,    48,    49,    50,   304,   174,    51,   305,
     175,   314,   119,   120,   276,   121,   122,   388,   502,   503,
     729,   727,   123,   124,   404,   528,   529,   823,   822,   739,
     125,   126,   389,   508,   279,   414,   415,   127,   128,   391,
     513,   129,   130,   489,   631,   131,   132,   488,   625,   133,
     134,   405,   537,   538,   741,   742,   135,   136,   137,   138,
     395,   139,   140,   141,   142,   143,   144,   403,   520,   145,
     146,    52,   504,   633,   641,   634,   635,   636,   780,   639,
     725,   648,   296,   147,   148,   149,   410,   548,   665,   799,
     150,   151,   328,    54,   297,   424,   570,   752,   753,   152,
     153,   408,   547,   289
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -653
static const yytype_int16 yypact[] =
{
    1966,  1816,   938,  -653,  -653,  -653,  -653,  -653,  -653,  -653,
    -653,  -653,  -653,  -653,  -653,  -653,  -653,  -653,  -653,  -653,
    -653,  -653,  -653,  -653,  -653,  -653,  -653,  -653,    55,   -33,
     -42,    48,   216,    79,  -653,    18,  2707,  2707,  -653,    91,
    -653,  2707,  2707,    10,     5,    49,  -653,  2081,  -653,  -653,
    -653,  -653,  -653,  -653,  2196,    62,  -653,  -653,  1889,  1889,
    1913,    88,   103,   108,  1231,  -653,  -653,  -653,  -653,  -653,
    -653,   248,  -653,   261,   101,  1816,  -653,   -39,   -31,   268,
      78,   388,   148,   141,   150,   301,    14,  -653,  -653,   205,
     -91,  1816,   194,   222,   224,   258,   603,   271,   381,   266,
     293,   499,  2688,    -9,   312,  -653,   -60,  -653,    18,  -653,
    -653,  -653,   938,  -653,  -653,  -653,  -653,  -653,  -653,  -653,
    -653,  -653,   603,  -653,   394,  -653,   329,  -653,   603,  -653,
     394,  -653,   329,  -653,   603,  -653,   603,  -653,   603,  -653,
    -653,  -653,  -653,   748,  -653,  -653,   603,  -653,  -653,  -653,
    -653,  -653,  -653,   478,   351,   496,   503,  -653,   357,  -653,
    -653,   216,  -653,  -653,   125,  -653,   259,  -653,  -653,   371,
    2265,   374,  -653,  -653,   386,  2599,  2491,   492,     5,  -653,
      48,  -653,   390,  1394,  1231,  -653,  -653,  1231,  -653,  1816,
    2570,  2570,   -97,  2570,   -54,  2570,   399,  -653,   438,  -653,
    -653,  1447,  1816,   457,  -653,  -653,  -653,  -653,  -653,  -653,
    -653,  -653,  -653,  -653,  -653,  1816,  -653,  -653,  1816,  1816,
    1816,  1816,  1816,  1816,  1816,  1816,  1816,  1816,  1816,  1816,
    1816,  1816,  1816,  1816,  1816,  1816,  1816,  1816,   603,  -653,
     382,   603,  1816,  1816,  1816,  1310,    29,   483,   646,   414,
    -653,  -653,  -653,   142,   -34,  -653,   418,  -653,   420,   -62,
     423,   -24,   -59,  -653,  -653,   427,   435,  -653,   459,  -653,
    -653,   938,  -653,   440,  -653,  -653,  -653,  -653,  1038,  -653,
    -653,  -653,  -653,  -653,  -653,  -653,  -653,  -653,   485,   491,
     496,  -653,   484,   331,  -653,  -653,   -57,  -653,  -653,  -653,
    -653,    48,  -653,  -653,   386,  2599,  2570,  -653,  2307,  -653,
      31,  2570,  -653,  -653,  2439,  -653,  -653,    84,   410,   512,
    -653,     2,  1323,  -653,   523,   535,  1281,  -653,   386,  -653,
      90,  -653,   570,   585,   582,   591,   611,  -653,  -653,  2246,
    1476,    61,  -653,   178,  -653,  1502,  -653,  -653,  -653,   117,
      13,  -653,  -653,  -653,  -653,  -653,  -653,   -39,   -39,   -31,
     -31,   268,   268,   268,   268,    78,    78,   388,   148,   141,
     150,   301,     1,  -653,  -653,   603,  -653,   120,   234,   235,
     609,   893,   145,  1059,  -653,  -653,  -653,  -653,   787,   776,
    -653,  1243,  -653,   741,  -653,   597,  -653,   619,   620,   623,
    -653,  -653,  -653,   624,  1055,  2050,  -653,  -653,   315,  -653,
    -653,   793,  1142,  -653,   -40,   603,  1517,  -653,   348,  1816,
    -653,    32,   625,   758,   249,  -653,  1548,  -653,  2439,  2327,
    -653,  -653,  1816,  -653,   151,  -653,   612,  2379,  -653,  -653,
    2131,  -653,    19,  -653,  -653,  2657,  -653,   781,   648,  1323,
    -653,  -653,  1816,  -653,   649,   650,  -653,  -653,  1816,   655,
     655,  2570,   792,  2570,  -653,   665,   669,  -653,   668,   670,
     178,  2550,  1599,  1353,  -653,  -653,  -653,  1816,  -653,   603,
     603,   603,  1816,  1671,   159,  1107,  1695,   181,   920,  1996,
    -653,  -653,  -653,  -653,   674,   675,  -653,   676,  -653,   677,
     672,  -653,  -653,  -653,  -653,  -653,   508,  -653,  -653,  -653,
    -653,  1481,  -653,  -653,  -653,   671,  -653,  -653,  -653,  -653,
     809,  -653,  -653,  -653,   679,   684,   498,  -653,  -653,  -653,
    -653,   712,  -653,   728,  -653,   819,  -653,  -653,  -653,  -653,
    -653,   731,  -653,   737,  -653,   140,  -653,  -653,   429,  -653,
     716,   794,  -653,   603,  -653,  -653,   238,  -653,    50,  -653,
    -653,  -653,  -653,  -653,   753,   755,   763,   766,   400,  -653,
    -653,  1353,  -653,  -653,  -653,  -653,  -653,    24,  -653,  1816,
    -653,  -653,  -653,  -653,  -653,   767,   769,  -653,  -653,  -653,
     768,   772,   779,  -653,  -653,  -653,  -653,  -653,   784,  -653,
     786,   811,  1816,   546,  -653,   413,  1548,   129,  -653,  -653,
     867,  -653,  -653,   242,   603,   250,  1712,  1726,   183,   603,
     278,  1744,   672,  -653,  -653,  -653,  -653,  2113,  -653,  -653,
    -653,  -653,  -653,   795,   796,   797,   815,   241,   219,   817,
    -653,   818,  -653,  -653,  -653,  -653,  -653,  -653,   286,   366,
    1816,  -653,  -653,  -653,  -653,  1920,  1816,  -653,  -653,   820,
     821,   822,  -653,   227,  -653,  -653,  -653,  -653,  -653,  -653,
     219,   950,   950,   950,  -653,   588,  -653,  -653,  -653,  -653,
    -653,  -653,  -653,  -653,  -653,  -653,   825,  -653,  -653,  -653,
    1023,  -653,  -653,  -653,   603,   800,  -653,   603,   603,   289,
     603,   316,  1768,  -653,   603,   603,   334,  -653,  -653,   809,
     809,   809,   809,   823,   828,  -653,  -653,  -653,  -653,  -653,
    -653,  -653,  -653,  -653,  -653,  -653,   809,  1816,   809,  1816,
    -653,   961,  -653,  -653,  -653,  -653,  -653,  -653,   824,   335,
     368,  1816,  1816,   856,   860,   872,  1816,  1816,   601,  -653,
     865,   879,   380,  -653,   387,   389,  -653,  1302,  -653,  -653,
    -653,  1548,  -653,  -653,  -653,  -653,   603,  -653,   603,   603,
     398,  -653,  -653,   603,   403,   406,   411,   445,  -653,  -653,
     868,   475,   480,   487,   489,  -653,  -653,  -653,  -653,  -653,
     566,   567,  -653,  -653,  -653,   887,   889,  -653,   884,   895,
     950,   320,  -653,   950,  -653,  -653,  -653,  -653,  -653,  -653,
    -653,   603,  -653,  -653,  -653,  -653,  -653,   809,  -653,  -653,
    -653,  -653,  1816,  1816,  -653,  -653,  -653,  -653,   602,  -653,
     574,  1030,   897,  -653,  -653,   581,   584,   586,  -653,  -653,
    -653,   905,  -653,  -653,  -653,  -653,  -653
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -653,  -653,  -653,  -653,  -653,  -653,  -182,   -16,  -653,   -47,
     516,   519,   208,   531,   816,   826,   827,   831,   836,  -653,
     -86,  -176,  -653,    -1,  -354,     3,    15,  -653,   749,  -653,
    -653,   -53,  -653,  -653,  -192,  -201,   -85,  -653,   474,  -653,
     764,  -363,   116,  -653,   -27,   -36,   -26,  -143,  -159,  -653,
     614,  -653,  -120,  -168,  -310,   -37,  -412,   486,  -652,  -653,
     449,     8,  -653,  -139,  -653,   790,   -96,   919,  -653,  -653,
     229,  -653,  -653,  1025,  -653,  1019,  -653,  -653,  -653,  -653,
    -653,   770,  -653,  -653,  -116,  -653,  -653,  -653,   576,  -440,
    -653,  -653,  -653,  -653,  -653,   551,  -468,  -653,  -653,  -653,
    -653,  -653,  -653,   572,   949,  -653,   695,  -653,  -653,  -653,
     577,  -653,  -653,  -653,   463,  -653,  -653,  -653,   494,  -653,
    -653,  -653,   578,  -653,  -653,  -653,  -653,  -653,  -653,  -653,
    -653,  -653,  -653,  -653,  -653,  -653,  -653,  -653,  -653,  -653,
    -653,  -653,     7,  -653,  -653,  -653,  -653,  -653,  -653,  -653,
     441,  -389,  -653,  -653,  -653,  -653,  -653,  -653,   456,  -653,
    -653,    47,  -653,  -653,  -653,  -653,   552,  -600,   318,  -653,
    -653,  -653,   579,  -653
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -450
static const yytype_int16 yytable[] =
{
      89,   325,   171,    34,   158,   239,   160,   331,   166,   178,
     109,   193,   280,   334,   573,    35,   274,   318,   283,   349,
     284,   630,   285,     3,     3,   331,   342,   181,   217,     3,
     287,   470,   551,   235,   326,   312,     3,   291,   761,   353,
     337,   386,   185,   186,   188,   183,   237,    53,   624,   629,
      34,   167,   168,     3,     3,   291,   172,   173,   561,   216,
     154,   373,    35,   192,   332,   559,   238,   333,   387,   180,
     335,   336,   754,   755,   393,   216,   157,   401,   576,   162,
     422,   273,   339,   237,   340,   310,   423,   397,   398,     3,
     399,    32,   225,   226,    53,   267,   169,     4,   394,   272,
     253,   402,   552,   156,   247,   761,   218,   431,   338,   157,
     344,   219,   220,   268,   429,   221,   222,   193,   269,   437,
     109,   204,   205,   206,   207,   208,   209,   210,   211,   212,
     213,   193,   470,   267,   193,   300,   400,   193,   193,   446,
     193,   176,   193,   177,   237,   447,   448,   157,   159,   443,
     455,  -276,   476,   327,    31,   440,   237,   340,   477,   630,
      31,   347,   413,    32,   469,   427,   352,    31,   341,    32,
     236,   354,   355,   356,   560,   439,    32,   163,   313,   449,
     195,   432,   624,   192,    31,    31,   192,   629,   432,   456,
     433,   317,   669,    32,   691,   561,   155,   339,   183,   340,
     830,   350,   216,   216,   216,   216,   216,   216,   216,   216,
     216,   216,   216,   216,   216,   216,   216,   216,   216,   216,
     440,   310,   340,   310,   189,   677,   310,   457,   431,    32,
     227,   228,   170,   458,   556,   372,   431,   715,   716,   190,
     331,   377,   378,   379,   191,   540,   374,   382,   686,   376,
     572,   383,    10,   193,   475,   193,   197,   479,   193,   214,
     458,    19,    20,   237,   541,   542,   543,   602,   301,   603,
     198,   199,   200,   585,   273,   544,   586,   299,   760,   109,
     223,   224,   589,   436,   302,   237,   195,   692,   237,   574,
     441,   442,   231,   159,   577,   232,   601,   572,   474,   554,
     195,   385,   237,   195,   485,   233,   195,   195,   313,   195,
     578,   195,   598,   341,   471,   274,   472,   438,   616,   234,
     774,   775,   776,   777,   237,    55,   237,    56,    57,   216,
      58,    59,   658,   239,   717,   718,   713,   781,   714,   783,
     621,   590,   702,   592,   310,   760,   239,   564,   237,   807,
      60,   241,   310,   277,   317,   659,   660,   661,   242,   281,
     243,    32,   662,   719,   720,   721,   722,   732,   565,   566,
     567,   480,   481,   723,   724,   668,   193,   237,   237,   695,
     484,   458,   487,   478,   193,   237,   249,   697,    61,    62,
      63,   609,   568,   237,   244,   572,   509,   201,   514,   202,
    -274,   203,  -104,   216,   229,   230,   178,   248,   193,   569,
     193,   530,   539,   158,   442,   704,   216,   303,  -104,   109,
     540,   237,   195,   730,   195,   250,   766,   195,   835,   731,
     572,   438,   237,   361,   362,   363,   364,   667,   159,   541,
     542,   543,   299,   346,     4,   733,   734,   735,   736,   737,
     544,   738,   251,   768,   270,   317,    64,    97,   545,   237,
     317,   216,   351,     4,    65,    66,    67,    68,    69,    70,
     278,   773,   787,   420,   421,   546,   831,   237,   788,   744,
     745,   613,   615,   288,   618,   620,   317,   610,   611,   612,
     557,   558,   290,   239,   298,   626,   632,    55,   564,    56,
      57,   291,    58,    59,    55,   789,    56,    57,   295,    58,
      59,   237,   306,   509,   572,   311,   239,   802,   514,   565,
     566,   567,    60,   803,   804,   322,   805,   104,    10,    60,
     803,  -449,   803,   530,   658,   811,   345,    19,    20,   375,
     813,   237,   539,   814,   380,   195,   731,   444,   815,   731,
     436,   687,     4,   195,   731,   689,   690,   659,   660,   661,
      61,    62,    63,   216,   662,   299,   688,    61,    62,    63,
     795,   796,   663,   384,   522,   523,   524,   195,   390,   195,
     392,   572,   816,   396,   505,   572,   216,   406,   731,   664,
     490,   491,   492,   493,   494,   407,   495,   496,   303,   498,
     490,   491,   492,   493,   494,   525,   495,   496,    90,   498,
      56,    57,   818,    58,    59,   699,   701,   819,   731,   409,
     706,   416,   696,   237,   820,   832,   821,   703,    64,   626,
     731,   323,   237,    60,   632,    64,    65,   324,    67,    68,
      69,    70,   419,    65,    66,    67,    68,    69,    70,   740,
     417,    55,     4,    56,    57,   445,    58,    59,   252,    91,
      92,    93,   450,    94,    95,    96,    97,    98,    99,   100,
     101,    61,    62,    63,   451,   245,    60,     5,     6,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,   770,   762,   824,   825,   764,   765,   459,   767,   237,
     237,   840,   771,   772,    61,    62,    63,   803,   843,    29,
     246,   844,   460,   845,   731,   461,   782,   237,   784,   237,
     756,   757,   797,   798,   462,   838,   839,   357,   358,    64,
     790,   791,   359,   360,   104,   482,   515,    65,    66,    67,
      68,    69,    70,    55,   463,    56,    57,   516,    58,    59,
     365,   366,   105,   563,   490,   491,   492,   493,   494,   579,
     495,   496,   497,   498,   808,   499,   809,   810,    60,   517,
     518,   812,    64,   519,   521,   562,   583,   584,   587,   588,
      65,    66,    67,    68,    69,    70,   473,   591,    90,     4,
      56,    57,   593,    58,    59,   381,   594,   595,   646,   596,
     637,   638,   640,   642,   647,   649,    61,    62,    63,   834,
     650,   836,   837,    60,     5,     6,     7,     8,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,   652,    91,
      92,    93,   505,    94,    95,    96,    97,    98,    99,   100,
     101,    61,    62,    63,   653,   102,    29,   655,   490,   491,
     492,   493,   494,   656,   495,   496,   666,   498,   550,   490,
     491,   492,   493,   494,    64,   495,   496,   497,   498,   670,
     499,   671,    65,    66,    67,    68,    69,    70,    55,   672,
      56,    57,   673,    58,    59,   680,   678,   105,   679,   681,
     103,   490,   491,   492,   493,   494,   682,   495,   496,   506,
     498,   683,   531,    60,   532,   684,   694,   533,   534,    64,
     500,   709,   710,   711,   104,   549,   507,    65,    66,    67,
      68,    69,    70,    90,     4,    56,    57,   501,    58,    59,
     685,   712,   105,   726,   728,   751,   746,   747,   748,   763,
     778,    61,    62,    63,   758,   779,   785,   786,    60,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,   792,    91,    92,    93,   793,    94,    95,
      96,    97,    98,    99,   100,   101,    61,    62,    63,   794,
     102,    29,   490,   491,   492,   493,   494,   801,   495,   496,
     497,   498,   800,   499,   826,   817,   827,   828,    55,    64,
      56,    57,   829,    58,    59,   841,   842,    65,    66,    67,
      68,    69,    70,    90,   846,    56,    57,   367,    58,    59,
     425,   676,   483,    60,   418,   103,   693,   675,   368,   582,
     369,   411,   286,   622,    55,   370,    56,    57,    60,    58,
      59,   371,   179,   182,    64,   428,   643,   651,   644,   104,
     623,   282,    65,    66,    67,    68,    69,    70,   645,    60,
     708,    61,    62,    63,    91,    92,    93,   105,    94,    95,
      96,    97,    98,    99,   100,   101,    61,    62,    63,   553,
     412,   750,    55,   654,    56,    57,   707,    58,    59,   749,
     674,   833,     0,     0,   657,     0,     0,    61,    62,    63,
       0,   522,   523,   524,     0,     0,     0,    60,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   490,   491,   492,
     493,   494,     0,   495,   496,   246,   498,     0,     0,    64,
       0,   602,   525,   603,   571,   759,     0,    65,    66,    67,
      68,    69,    70,     0,    64,    61,    62,    63,     0,   104,
       0,     0,    65,    66,    67,    68,    69,    70,     0,     0,
       0,     0,     0,     0,     0,    64,     0,   105,   526,     0,
       0,     0,     0,    65,    66,    67,    68,    69,    70,     0,
       0,     0,     0,     0,     0,   527,   254,   255,   486,   256,
       0,     0,     0,     0,     0,     0,   550,     0,   257,   258,
     259,     0,   261,     0,     0,     0,    55,     4,    56,    57,
       0,    58,    59,    64,   263,     0,   264,     0,     0,     0,
       0,    65,    66,    67,    68,    69,    70,     0,     0,     0,
       0,    60,     0,     0,     0,     0,   617,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,     0,
      23,    24,    25,    26,    27,    28,    55,     0,    56,    57,
       0,    58,    59,     0,     0,     0,     0,     0,     0,    61,
      62,    63,     0,     0,     0,     0,     0,    55,     0,    56,
      57,    60,    58,    59,   452,     0,     0,    10,     0,   510,
       0,     0,     0,     0,     0,     0,    19,    20,    55,     0,
      56,    57,    60,    58,    59,   490,   491,   492,   493,   494,
       0,   495,   496,     0,   498,     0,     0,     0,     0,    61,
      62,    63,     0,    60,     0,     0,     0,     0,    55,    10,
      56,    57,     0,    58,    59,     0,     0,    64,    19,    20,
      61,    62,    63,     0,     0,    65,    66,    67,    68,    69,
      70,     0,     0,    60,   254,   255,   511,   256,     0,     0,
       0,    61,    62,    63,     0,     0,   257,   258,   259,    55,
     261,    56,    57,   512,    58,    59,     0,     0,     0,     0,
       0,     0,   263,     0,   264,     0,     0,    64,     0,     0,
     453,    61,    62,    63,    60,    65,   454,    67,    68,    69,
      70,     0,     0,     0,     0,     0,     0,     0,    64,     0,
     602,     0,   603,   571,   806,     0,    65,    66,    67,    68,
      69,    70,    55,     0,    56,    57,     0,    58,    59,    64,
       0,     0,    61,    62,    63,     0,     0,    65,    66,    67,
      68,    69,    70,     0,     0,     0,     0,    60,     0,     0,
       0,    55,     0,    56,    57,     0,    58,    59,     0,    64,
       0,   602,     0,   603,   571,     0,     0,    65,    66,    67,
      68,    69,    70,     0,     0,     0,    60,    55,     0,    56,
      57,     0,    58,    59,     0,    61,    62,    63,     0,     0,
       0,     0,    55,     0,    56,    57,     0,    58,    59,     0,
      64,   329,    60,     0,     0,     0,     0,     0,    65,    66,
      67,    68,    69,    70,    61,    62,    63,    60,     0,     0,
       0,     0,     0,    55,     0,    56,    57,   510,    58,    59,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      61,    62,    63,   490,   491,   492,   493,   494,    60,   495,
     496,     0,   498,    64,   348,    61,    62,    63,     0,     0,
       0,    65,    66,    67,    68,    69,    70,     0,     0,     0,
       0,     0,     0,     0,    55,     0,    56,    57,     0,    58,
      59,     0,    64,     0,     0,   467,    61,    62,    63,     0,
      65,   468,    67,    68,    69,    70,     0,     0,     0,    60,
       0,     0,     0,     0,     0,     0,     0,     0,    64,     0,
       0,     0,     0,   473,     0,     0,    65,    66,    67,    68,
      69,    70,     0,    64,   555,     0,     0,     0,     0,     0,
       0,    65,    66,    67,    68,    69,    70,    61,    62,    63,
       0,     0,     0,     0,     0,     0,    55,     0,    56,    57,
       0,    58,    59,     0,    64,     0,     0,     0,     0,   571,
       0,     0,    65,    66,    67,    68,    69,    70,     0,     0,
      55,    60,    56,    57,     0,    58,    59,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    55,     0,    56,
      57,     0,    58,    59,     0,    60,     0,     0,     0,     0,
       0,    55,     0,    56,    57,    64,    58,    59,   599,    61,
      62,    63,    60,    65,   600,    67,    68,    69,    70,    55,
       0,    56,    57,     0,    58,    59,    60,     0,     0,     0,
       0,     0,     0,    61,    62,    63,     0,     0,     0,     0,
       0,     0,     0,    55,    60,    56,    57,     0,    58,    59,
      61,    62,    63,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    61,    62,    63,     0,    60,     0,
       0,     0,     0,     0,     0,     0,     0,    64,   614,     0,
       0,     0,    61,    62,    63,    65,    66,    67,    68,    69,
      70,    55,     0,    56,    57,     0,    58,    59,     0,     0,
       0,    64,   619,     0,     0,     0,    61,    62,    63,    65,
      66,    67,    68,    69,    70,     0,    60,     0,    64,   698,
       0,     0,     0,     0,     0,     0,    65,    66,    67,    68,
      69,    70,    64,   700,     0,     0,     0,     0,     0,     0,
      65,    66,    67,    68,    69,    70,     0,     0,     0,     0,
      64,   705,     0,     0,    61,    62,    63,     0,    65,    66,
      67,    68,    69,    70,    55,     0,    56,    57,     0,    58,
      59,     0,     0,     0,    64,   769,     0,     0,     0,     0,
       0,     0,    65,    66,    67,    68,    69,    70,    55,    60,
      56,    57,     0,    58,    59,    55,     0,    56,    57,     0,
      58,    59,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    60,     0,     0,     0,     0,     0,     0,
      60,     0,    64,     0,     0,     0,     0,    61,    62,    63,
      65,    66,    67,    68,    69,    70,     0,     0,     0,     1,
       2,     3,     4,     0,     0,     0,     0,     0,     0,     0,
       0,    61,    62,    63,     0,     0,     0,     0,    61,    62,
      63,     0,     0,     0,     0,     0,     0,     5,     6,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,     0,     0,     0,     0,   184,     0,     0,     0,     0,
       0,     0,     0,    65,    66,    67,    68,    69,    70,    29,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   187,
       0,     0,     0,     0,     0,     0,    64,    65,    66,    67,
      68,    69,    70,     0,    65,   743,    67,    68,    69,    70,
       0,     0,     0,   523,   524,     0,     0,     0,     0,     0,
       0,     0,     0,    30,     0,     0,     3,     4,   490,   491,
     492,   493,   494,     0,   495,   496,   497,   498,     0,   499,
       0,     0,    31,   525,     0,     0,     0,     0,     0,     0,
       0,    32,     5,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,     3,     4,     0,   627,
       0,     0,   490,   491,   492,   493,   494,     0,   495,   496,
       0,   498,     0,   531,    29,   532,   628,     0,   533,   534,
       0,     0,     5,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,     0,     0,     0,     0,
     523,   524,     0,   535,     0,     0,     0,     0,    30,     0,
       0,     3,     4,     0,     0,   490,   491,   492,   493,   494,
     536,   495,   496,   497,   498,     0,   499,    31,     0,     0,
     525,     0,     0,     0,     0,     0,    32,     5,     6,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,     0,     4,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   440,   464,   340,
       0,     4,     0,     0,     0,     0,    32,     5,     6,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,     4,    23,    24,    25,    26,    27,    28,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    31,     4,     0,     0,     0,     0,     0,     0,
       0,    32,     0,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,     0,    23,    24,    25,    26,
      27,    28,     0,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,     0,    23,    24,    25,    26,
      27,    28,   339,   464,   340,     4,     0,     0,     0,     0,
       0,    32,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   307,     0,     0,
       0,     0,     0,     0,     0,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,     0,    23,    24,
      25,    26,    27,    28,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     4,     0,     0,     0,   430,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   575,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,     0,     0,   315,     4,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    29,     0,     0,     0,     0,     0,     0,     0,
       0,   580,     5,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     4,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     4,     0,     0,     0,
     104,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,     4,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,     0,    23,
      24,    25,    26,    27,    28,     0,     0,     0,   316,     0,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     4,     0,     0,     0,     0,     0,     0,
       0,     0,    29,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   597,     5,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,   581,     4,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     5,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,   254,   255,     0,   256,     0,     0,     0,     0,
       0,     0,     0,     0,   257,   258,   259,   260,   261,   262,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     263,     0,   264,     0,   265,     0,     0,     0,   266
};

static const yytype_int16 yycheck[] =
{
       1,   177,    39,     0,    31,    91,    32,   183,    35,    45,
       2,    64,   128,   189,   426,     0,   112,   176,   134,   201,
     136,   489,   138,     5,     5,   201,   194,    54,    75,     5,
     146,   341,    72,    19,   177,   174,     5,     5,   690,   215,
     137,    75,    58,    59,    60,   136,   143,     0,   488,   489,
      47,    36,    37,     5,     5,     5,    41,    42,   421,    75,
       5,   237,    47,    64,   184,   419,   157,   187,   102,    54,
     190,   191,   672,   673,   136,    91,   118,   136,   432,     0,
     137,   108,   136,   143,   138,   170,   143,   111,   112,     5,
     114,   145,    14,    15,    47,   104,     5,     6,   160,   159,
     101,   160,   142,   136,    96,   757,   145,   308,   193,   118,
     195,   150,   151,   122,   306,   146,   147,   170,   127,   311,
     112,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,   184,   442,   104,   187,   161,   160,   190,   191,   137,
     193,   136,   195,   138,   143,   143,   322,   118,    32,   317,
     326,   141,   139,   180,   136,   136,   143,   138,   157,   627,
     136,   198,   278,   145,   340,   304,   203,   136,   194,   145,
     156,   218,   219,   220,   142,   314,   145,   159,   175,   322,
      64,   157,   622,   184,   136,   136,   187,   627,   157,   328,
     159,   176,   142,   145,   606,   558,   141,   136,   136,   138,
     800,   202,   218,   219,   220,   221,   222,   223,   224,   225,
     226,   227,   228,   229,   230,   231,   232,   233,   234,   235,
     136,   306,   138,   308,   136,   579,   311,   137,   429,   145,
     152,   153,   141,   143,   416,   236,   437,    18,    19,   136,
     416,   242,   243,   244,   136,   105,   238,   248,   602,   241,
     426,   248,    36,   306,   137,   308,     8,   137,   311,   158,
     143,    45,    46,   143,   124,   125,   126,   138,   143,   140,
       9,    10,    11,   449,   301,   135,   452,   161,   690,   271,
      12,    13,   458,   310,   159,   143,   170,   158,   143,   428,
     317,   317,   144,   177,   143,   154,   472,   473,   345,   415,
     184,   159,   143,   187,   159,   155,   190,   191,   305,   193,
     159,   195,   471,   339,   136,   411,   138,   314,   159,    18,
     709,   710,   711,   712,   143,     5,   143,     7,     8,   345,
      10,    11,   105,   419,   115,   116,    95,   726,    97,   728,
     159,   461,   159,   463,   429,   757,   432,    98,   143,   761,
      30,   157,   437,   124,   339,   128,   129,   130,   136,   130,
     136,   145,   135,   144,   145,   146,   147,     1,   119,   120,
     121,   137,   137,   154,   155,   137,   429,   143,   143,   137,
     381,   143,   383,   375,   437,   143,     5,   137,    68,    69,
      70,   477,   143,   143,   136,   571,   389,   136,   391,   138,
     141,   140,   143,   419,    16,    17,   442,   136,   461,   160,
     463,   404,   405,   440,   440,   137,   432,   158,   159,   411,
     105,   143,   306,   137,   308,   159,   137,   311,   817,   143,
     606,   428,   143,   225,   226,   227,   228,   553,   322,   124,
     125,   126,   326,     5,     6,    79,    80,    81,    82,    83,
     135,    85,   159,   137,   142,   440,   136,    63,   143,   143,
     445,   477,     5,     6,   144,   145,   146,   147,   148,   149,
     141,   137,   137,   142,   143,   160,   156,   143,   143,   655,
     656,   482,   483,     5,   485,   486,   471,   479,   480,   481,
     142,   143,   141,   579,   137,   488,   489,     5,    98,     7,
       8,     5,    10,    11,     5,   137,     7,     8,     5,    10,
      11,   143,   141,   506,   690,   141,   602,   137,   511,   119,
     120,   121,    30,   143,   137,    33,   137,   141,    36,    30,
     143,   141,   143,   526,   105,   137,   137,    45,    46,   157,
     137,   143,   535,   137,    61,   429,   143,   137,   137,   143,
     577,     5,     6,   437,   143,   142,   143,   128,   129,   130,
      68,    69,    70,   579,   135,   449,   603,    68,    69,    70,
     746,   747,   143,   159,    76,    77,    78,   461,   160,   463,
     160,   757,   137,   160,    76,   761,   602,   160,   143,   160,
      92,    93,    94,    95,    96,   160,    98,    99,   158,   101,
      92,    93,    94,    95,    96,   107,    98,    99,     5,   101,
       7,     8,   137,    10,    11,   616,   617,   137,   143,   160,
     621,   136,   614,   143,   137,   801,   137,   619,   136,   622,
     143,   139,   143,    30,   627,   136,   144,   145,   146,   147,
     148,   149,   158,   144,   145,   146,   147,   148,   149,   650,
     159,     5,     6,     7,     8,   143,    10,    11,   159,    56,
      57,    58,   139,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,   139,    72,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,   702,   694,   137,   137,   697,   698,   137,   700,   143,
     143,   137,   704,   705,    68,    69,    70,   143,   137,    73,
     117,   137,   137,   137,   143,   143,   727,   143,   729,   143,
     142,   143,   131,   132,   143,   133,   134,   221,   222,   136,
     741,   742,   223,   224,   141,   136,     5,   144,   145,   146,
     147,   148,   149,     5,   143,     7,     8,   160,    10,    11,
     229,   230,   159,     5,    92,    93,    94,    95,    96,   157,
      98,    99,   100,   101,   766,   103,   768,   769,    30,   160,
     160,   773,   136,   160,   160,   160,     5,   139,   139,   139,
     144,   145,   146,   147,   148,   149,   141,     5,     5,     6,
       7,     8,   137,    10,    11,   159,   137,   139,   137,   139,
     136,   136,   136,   136,     5,   136,    68,    69,    70,   811,
     136,   822,   823,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,   136,    56,
      57,    58,    76,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,   136,    72,    73,   136,    92,    93,
      94,    95,    96,   136,    98,    99,   160,   101,    84,    92,
      93,    94,    95,    96,   136,    98,    99,   100,   101,   136,
     103,   136,   144,   145,   146,   147,   148,   149,     5,   136,
       7,     8,   136,    10,    11,   137,   139,   159,   139,   137,
     117,    92,    93,    94,    95,    96,   137,    98,    99,   143,
     101,   137,   103,    30,   105,   139,    59,   108,   109,   136,
     143,   136,   136,   136,   141,   142,   160,   144,   145,   146,
     147,   148,   149,     5,     6,     7,     8,   160,    10,    11,
     139,   136,   159,   136,   136,     5,   136,   136,   136,   159,
     137,    68,    69,    70,   139,   137,     5,   143,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,   137,    56,    57,    58,   137,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,   137,
      72,    73,    92,    93,    94,    95,    96,   138,    98,    99,
     100,   101,   157,   103,   137,   157,   137,   143,     5,   136,
       7,     8,   137,    10,    11,     5,   139,   144,   145,   146,
     147,   148,   149,     5,   139,     7,     8,   231,    10,    11,
     301,   577,   159,    30,   290,   117,   607,   571,   232,   445,
     233,   271,   143,   143,     5,   234,     7,     8,    30,    10,
      11,   235,    47,    54,   136,   305,   500,   526,   506,   141,
     160,   132,   144,   145,   146,   147,   148,   149,   511,    30,
     627,    68,    69,    70,    56,    57,    58,   159,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,   414,
      72,   670,     5,   535,     7,     8,   622,    10,    11,   663,
     568,   803,    -1,    -1,   545,    -1,    -1,    68,    69,    70,
      -1,    76,    77,    78,    -1,    -1,    -1,    30,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    92,    93,    94,
      95,    96,    -1,    98,    99,   117,   101,    -1,    -1,   136,
      -1,   138,   107,   140,   141,   142,    -1,   144,   145,   146,
     147,   148,   149,    -1,   136,    68,    69,    70,    -1,   141,
      -1,    -1,   144,   145,   146,   147,   148,   149,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   136,    -1,   159,   143,    -1,
      -1,    -1,    -1,   144,   145,   146,   147,   148,   149,    -1,
      -1,    -1,    -1,    -1,    -1,   160,    74,    75,   159,    77,
      -1,    -1,    -1,    -1,    -1,    -1,    84,    -1,    86,    87,
      88,    -1,    90,    -1,    -1,    -1,     5,     6,     7,     8,
      -1,    10,    11,   136,   102,    -1,   104,    -1,    -1,    -1,
      -1,   144,   145,   146,   147,   148,   149,    -1,    -1,    -1,
      -1,    30,    -1,    -1,    -1,    -1,   159,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    -1,
      49,    50,    51,    52,    53,    54,     5,    -1,     7,     8,
      -1,    10,    11,    -1,    -1,    -1,    -1,    -1,    -1,    68,
      69,    70,    -1,    -1,    -1,    -1,    -1,     5,    -1,     7,
       8,    30,    10,    11,    33,    -1,    -1,    36,    -1,    76,
      -1,    -1,    -1,    -1,    -1,    -1,    45,    46,     5,    -1,
       7,     8,    30,    10,    11,    92,    93,    94,    95,    96,
      -1,    98,    99,    -1,   101,    -1,    -1,    -1,    -1,    68,
      69,    70,    -1,    30,    -1,    -1,    -1,    -1,     5,    36,
       7,     8,    -1,    10,    11,    -1,    -1,   136,    45,    46,
      68,    69,    70,    -1,    -1,   144,   145,   146,   147,   148,
     149,    -1,    -1,    30,    74,    75,   143,    77,    -1,    -1,
      -1,    68,    69,    70,    -1,    -1,    86,    87,    88,     5,
      90,     7,     8,   160,    10,    11,    -1,    -1,    -1,    -1,
      -1,    -1,   102,    -1,   104,    -1,    -1,   136,    -1,    -1,
     139,    68,    69,    70,    30,   144,   145,   146,   147,   148,
     149,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   136,    -1,
     138,    -1,   140,   141,   142,    -1,   144,   145,   146,   147,
     148,   149,     5,    -1,     7,     8,    -1,    10,    11,   136,
      -1,    -1,    68,    69,    70,    -1,    -1,   144,   145,   146,
     147,   148,   149,    -1,    -1,    -1,    -1,    30,    -1,    -1,
      -1,     5,    -1,     7,     8,    -1,    10,    11,    -1,   136,
      -1,   138,    -1,   140,   141,    -1,    -1,   144,   145,   146,
     147,   148,   149,    -1,    -1,    -1,    30,     5,    -1,     7,
       8,    -1,    10,    11,    -1,    68,    69,    70,    -1,    -1,
      -1,    -1,     5,    -1,     7,     8,    -1,    10,    11,    -1,
     136,   137,    30,    -1,    -1,    -1,    -1,    -1,   144,   145,
     146,   147,   148,   149,    68,    69,    70,    30,    -1,    -1,
      -1,    -1,    -1,     5,    -1,     7,     8,    76,    10,    11,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      68,    69,    70,    92,    93,    94,    95,    96,    30,    98,
      99,    -1,   101,   136,   137,    68,    69,    70,    -1,    -1,
      -1,   144,   145,   146,   147,   148,   149,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     5,    -1,     7,     8,    -1,    10,
      11,    -1,   136,    -1,    -1,   139,    68,    69,    70,    -1,
     144,   145,   146,   147,   148,   149,    -1,    -1,    -1,    30,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   136,    -1,
      -1,    -1,    -1,   141,    -1,    -1,   144,   145,   146,   147,
     148,   149,    -1,   136,   137,    -1,    -1,    -1,    -1,    -1,
      -1,   144,   145,   146,   147,   148,   149,    68,    69,    70,
      -1,    -1,    -1,    -1,    -1,    -1,     5,    -1,     7,     8,
      -1,    10,    11,    -1,   136,    -1,    -1,    -1,    -1,   141,
      -1,    -1,   144,   145,   146,   147,   148,   149,    -1,    -1,
       5,    30,     7,     8,    -1,    10,    11,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     5,    -1,     7,
       8,    -1,    10,    11,    -1,    30,    -1,    -1,    -1,    -1,
      -1,     5,    -1,     7,     8,   136,    10,    11,   139,    68,
      69,    70,    30,   144,   145,   146,   147,   148,   149,     5,
      -1,     7,     8,    -1,    10,    11,    30,    -1,    -1,    -1,
      -1,    -1,    -1,    68,    69,    70,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,     5,    30,     7,     8,    -1,    10,    11,
      68,    69,    70,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    68,    69,    70,    -1,    30,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   136,   137,    -1,
      -1,    -1,    68,    69,    70,   144,   145,   146,   147,   148,
     149,     5,    -1,     7,     8,    -1,    10,    11,    -1,    -1,
      -1,   136,   137,    -1,    -1,    -1,    68,    69,    70,   144,
     145,   146,   147,   148,   149,    -1,    30,    -1,   136,   137,
      -1,    -1,    -1,    -1,    -1,    -1,   144,   145,   146,   147,
     148,   149,   136,   137,    -1,    -1,    -1,    -1,    -1,    -1,
     144,   145,   146,   147,   148,   149,    -1,    -1,    -1,    -1,
     136,   137,    -1,    -1,    68,    69,    70,    -1,   144,   145,
     146,   147,   148,   149,     5,    -1,     7,     8,    -1,    10,
      11,    -1,    -1,    -1,   136,   137,    -1,    -1,    -1,    -1,
      -1,    -1,   144,   145,   146,   147,   148,   149,     5,    30,
       7,     8,    -1,    10,    11,     5,    -1,     7,     8,    -1,
      10,    11,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    30,    -1,    -1,    -1,    -1,    -1,    -1,
      30,    -1,   136,    -1,    -1,    -1,    -1,    68,    69,    70,
     144,   145,   146,   147,   148,   149,    -1,    -1,    -1,     3,
       4,     5,     6,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    68,    69,    70,    -1,    -1,    -1,    -1,    68,    69,
      70,    -1,    -1,    -1,    -1,    -1,    -1,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    -1,    -1,    -1,    -1,   136,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   144,   145,   146,   147,   148,   149,    73,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   136,
      -1,    -1,    -1,    -1,    -1,    -1,   136,   144,   145,   146,
     147,   148,   149,    -1,   144,   145,   146,   147,   148,   149,
      -1,    -1,    -1,    77,    78,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   117,    -1,    -1,     5,     6,    92,    93,
      94,    95,    96,    -1,    98,    99,   100,   101,    -1,   103,
      -1,    -1,   136,   107,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   145,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,     5,     6,    -1,   143,
      -1,    -1,    92,    93,    94,    95,    96,    -1,    98,    99,
      -1,   101,    -1,   103,    73,   105,   160,    -1,   108,   109,
      -1,    -1,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    -1,    -1,    -1,    -1,
      77,    78,    -1,   143,    -1,    -1,    -1,    -1,   117,    -1,
      -1,     5,     6,    -1,    -1,    92,    93,    94,    95,    96,
     160,    98,    99,   100,   101,    -1,   103,   136,    -1,    -1,
     107,    -1,    -1,    -1,    -1,    -1,   145,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    -1,     6,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   136,   137,   138,
      -1,     6,    -1,    -1,    -1,    -1,   145,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,     6,    49,    50,    51,    52,    53,    54,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   136,     6,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   145,    -1,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    -1,    49,    50,    51,    52,
      53,    54,    -1,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    -1,    49,    50,    51,    52,
      53,    54,   136,   137,   138,     6,    -1,    -1,    -1,    -1,
      -1,   145,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   142,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    -1,    49,    50,
      51,    52,    53,    54,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,     6,    -1,    -1,    -1,   142,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   142,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    -1,    -1,     5,     6,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    73,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   142,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     6,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     6,    -1,    -1,    -1,
     141,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,     6,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    -1,    49,
      50,    51,    52,    53,    54,    -1,    -1,    -1,   137,    -1,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,     6,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    73,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   137,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,     6,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    74,    75,    -1,    77,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    86,    87,    88,    89,    90,    91,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     102,    -1,   104,    -1,   106,    -1,    -1,    -1,   110
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,     3,     4,     5,     6,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    73,
     117,   136,   145,   162,   186,   187,   191,   192,   193,   194,
     200,   203,   204,   205,   206,   207,   216,   233,   234,   235,
     236,   239,   302,   322,   324,     5,     7,     8,    10,    11,
      30,    68,    69,    70,   136,   144,   145,   146,   147,   148,
     149,   164,   165,   166,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   184,
       5,    56,    57,    58,    60,    61,    62,    63,    64,    65,
      66,    67,    72,   117,   141,   159,   184,   186,   187,   222,
     223,   224,   226,   227,   228,   229,   230,   231,   232,   243,
     244,   246,   247,   253,   254,   261,   262,   268,   269,   272,
     273,   276,   277,   280,   281,   287,   288,   289,   290,   292,
     293,   294,   295,   296,   297,   300,   301,   314,   315,   316,
     321,   322,   330,   331,     5,   141,   136,   118,   205,   203,
     207,   208,     0,   159,   188,   189,   205,   187,   187,     5,
     141,   216,   187,   187,   238,   241,   136,   138,   206,   234,
     187,   205,   236,   136,   136,   168,   168,   136,   168,   136,
     136,   136,   184,   192,   197,   203,   213,     8,     9,    10,
      11,   136,   138,   140,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,   158,   183,   168,   170,   145,   150,
     151,   146,   147,    12,    13,    14,    15,   152,   153,    16,
      17,   144,   154,   155,    18,    19,   156,   143,   157,   181,
     185,   157,   136,   136,   136,    72,   117,   222,   136,     5,
     159,   159,   159,   184,    74,    75,    77,    86,    87,    88,
      89,    90,    91,   102,   104,   106,   110,   104,   122,   127,
     142,   225,   159,   205,   227,   222,   245,   231,   141,   265,
     245,   231,   265,   245,   245,   245,   228,   245,     5,   334,
     141,     5,   163,   201,   202,     5,   313,   325,   137,   203,
     207,   143,   159,   158,   237,   240,   141,   142,   195,   196,
     197,   141,   224,   186,   242,     5,   137,   187,   209,   210,
     211,   212,    33,   139,   145,   182,   208,   205,   323,   137,
     167,   182,   213,   213,   182,   213,   213,   137,   197,   136,
     138,   207,   214,   215,   197,   137,     5,   216,   137,   167,
     184,     5,   216,   182,   170,   170,   170,   171,   171,   172,
     172,   173,   173,   173,   173,   174,   174,   175,   176,   177,
     178,   179,   184,   182,   222,   157,   222,   184,   184,   184,
      61,   159,   184,   186,   159,   159,    75,   102,   248,   263,
     160,   270,   160,   136,   160,   291,   160,   111,   112,   114,
     160,   136,   160,   298,   255,   282,   160,   160,   332,   160,
     317,   226,    72,   245,   266,   267,   136,   159,   201,   158,
     142,   143,   137,   143,   326,   189,   190,   224,   242,   195,
     142,   196,   157,   159,   198,   199,   205,   195,   186,   224,
     136,   205,   207,   214,   137,   143,   137,   143,   182,   208,
     139,   139,    33,   139,   145,   182,   224,   137,   143,   137,
     137,   143,   143,   143,   137,   209,   214,   139,   145,   182,
     215,   136,   138,   141,   170,   137,   139,   157,   222,   137,
     137,   137,   136,   159,   184,   159,   159,   184,   278,   274,
      92,    93,    94,    95,    96,    98,    99,   100,   101,   103,
     143,   160,   249,   250,   303,    76,   143,   160,   264,   303,
      76,   143,   160,   271,   303,     5,   160,   160,   160,   160,
     299,   160,    76,    77,    78,   107,   143,   160,   256,   257,
     303,   103,   105,   108,   109,   143,   160,   283,   284,   303,
     105,   124,   125,   126,   135,   143,   160,   333,   318,   142,
      84,    72,   142,   267,   245,   137,   167,   142,   143,   185,
     142,   202,   160,     5,    98,   119,   120,   121,   143,   160,
     327,   141,   182,   217,   224,   142,   185,   143,   159,   157,
     142,    55,   211,     5,   139,   182,   182,   139,   139,   182,
     213,     5,   213,   137,   137,   139,   139,   137,   209,   139,
     145,   182,   138,   140,   217,   218,   219,   220,   221,   181,
     222,   222,   222,   184,   137,   184,   159,   159,   184,   137,
     184,   159,   143,   160,   250,   279,   303,   143,   160,   250,
     257,   275,   303,   304,   306,   307,   308,   136,   136,   310,
     136,   305,   136,   249,   264,   271,   137,     5,   312,   136,
     136,   256,   136,   136,   283,   136,   136,   333,   105,   128,
     129,   130,   135,   143,   160,   319,   160,   245,   137,   142,
     136,   136,   136,   136,   327,   218,   199,   185,   139,   139,
     137,   137,   137,   137,   139,   139,   185,     5,   216,   142,
     143,   217,   158,   221,    59,   137,   222,   137,   137,   184,
     137,   184,   159,   222,   137,   137,   184,   279,   275,   136,
     136,   136,   136,    95,    97,    18,    19,   115,   116,   144,
     145,   146,   147,   154,   155,   311,   136,   252,   136,   251,
     137,   143,     1,    79,    80,    81,    82,    83,    85,   260,
     184,   285,   286,   145,   182,   182,   136,   136,   136,   319,
     311,     5,   328,   329,   328,   328,   142,   143,   139,   142,
     217,   219,   222,   159,   222,   222,   137,   222,   137,   137,
     184,   222,   222,   137,   312,   312,   312,   312,   137,   137,
     309,   312,   184,   312,   184,     5,   143,   137,   143,   137,
     184,   184,   137,   137,   137,   182,   182,   131,   132,   320,
     157,   138,   137,   143,   137,   137,   142,   217,   222,   222,
     222,   137,   222,   137,   137,   137,   137,   157,   137,   137,
     137,   137,   259,   258,   137,   137,   137,   137,   143,   137,
     328,   156,   182,   329,   222,   312,   184,   184,   133,   134,
     137,     5,   139,   137,   137,   137,   139
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1455 of yacc.c  */
#line 307 "parser.y"
    { /* to avoid warnings */ }
    break;

  case 3:

/* Line 1455 of yacc.c  */
#line 308 "parser.y"
    { pastree_expr = (yyvsp[(2) - (2)].expr); }
    break;

  case 4:

/* Line 1455 of yacc.c  */
#line 309 "parser.y"
    { pastree_stmt = (yyvsp[(2) - (2)].stmt); }
    break;

  case 5:

/* Line 1455 of yacc.c  */
#line 327 "parser.y"
    {
      symbol s = Symbol((yyvsp[(1) - (1)].name));
      if (checkDecls) 
      {
        if ( symtab_get(stab, s, LABELNAME) )  /* NOT a type name */
          parse_error(-1, "enum symbol '%s' is already in use.", (yyvsp[(1) - (1)].name));
        symtab_put(stab, s, LABELNAME);
      }
      (yyval.symb) = s;
    }
    break;

  case 6:

/* Line 1455 of yacc.c  */
#line 348 "parser.y"
    {
      (yyval.string) = strdup((yyvsp[(1) - (1)].name));
    }
    break;

  case 7:

/* Line 1455 of yacc.c  */
#line 352 "parser.y"
    {
      /* Or we could leave it as is (as a SpaceList) */
      if (((yyvsp[(1) - (2)].string) = realloc((yyvsp[(1) - (2)].string), strlen((yyvsp[(1) - (2)].string)) + strlen((yyvsp[(2) - (2)].name)))) == NULL)
        parse_error(-1, "string out of memory\n");
      strcpy(((yyvsp[(1) - (2)].string))+(strlen((yyvsp[(1) - (2)].string))-1),((yyvsp[(2) - (2)].name))+1);  /* Catenate on the '"' */
      (yyval.string) = (yyvsp[(1) - (2)].string);
    }
    break;

  case 8:

/* Line 1455 of yacc.c  */
#line 375 "parser.y"
    {
      symbol  id = Symbol((yyvsp[(1) - (1)].name));
      stentry e;
      int     chflag = 0;
    
      if (checkDecls)
      {
        check_uknown_var((yyvsp[(1) - (1)].name));
        /* The parser constructs the original AST; this is the only
         * place it doesn't (actually there is one more place, when replacing
         * the "main" function): threadprivate variables are replaced on the
         * fly by pointers. This makes the job of later stages much smoother,
         * but the produced AST is semantically incorrect.
         */
        if ((e = symtab_get(stab, id, IDNAME)) != NULL) /* could be enum name */
          if (istp(e) && threadmode)
            chflag = 1;
      }
      (yyval.expr) = chflag ? UnaryOperator(UOP_paren,
                             UnaryOperator(UOP_star, Identifier(id)))
                  : Identifier(id);
    }
    break;

  case 9:

/* Line 1455 of yacc.c  */
#line 398 "parser.y"
    {
      (yyval.expr) = Constant( strdup((yyvsp[(1) - (1)].name)) );
    }
    break;

  case 10:

/* Line 1455 of yacc.c  */
#line 402 "parser.y"
    {
      (yyval.expr) = String((yyvsp[(1) - (1)].string));
    }
    break;

  case 11:

/* Line 1455 of yacc.c  */
#line 406 "parser.y"
    {
      (yyval.expr) = UnaryOperator(UOP_paren, (yyvsp[(2) - (3)].expr));
    }
    break;

  case 12:

/* Line 1455 of yacc.c  */
#line 414 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 13:

/* Line 1455 of yacc.c  */
#line 418 "parser.y"
    {
      (yyval.expr) = ArrayIndex((yyvsp[(1) - (4)].expr), (yyvsp[(3) - (4)].expr));
    }
    break;

  case 14:

/* Line 1455 of yacc.c  */
#line 428 "parser.y"
    {
      /* Catch calls to "main()" (unlikely but possible) */
      (yyval.expr) = strcmp((yyvsp[(1) - (3)].name), "main") ?
             FunctionCall(Identifier(Symbol((yyvsp[(1) - (3)].name))), NULL) :
             FunctionCall(Identifier(Symbol(MAIN_NEWNAME)), NULL);
    }
    break;

  case 15:

/* Line 1455 of yacc.c  */
#line 435 "parser.y"
    {
      /* Catch calls to "main()" (unlikely but possible) */
      (yyval.expr) = strcmp((yyvsp[(1) - (4)].name), "main") ?
             FunctionCall(Identifier(Symbol((yyvsp[(1) - (4)].name))), (yyvsp[(3) - (4)].expr)) :
             FunctionCall(Identifier(Symbol(MAIN_NEWNAME)), (yyvsp[(3) - (4)].expr));
    }
    break;

  case 16:

/* Line 1455 of yacc.c  */
#line 442 "parser.y"
    {
      (yyval.expr) = FunctionCall((yyvsp[(1) - (3)].expr), NULL);
    }
    break;

  case 17:

/* Line 1455 of yacc.c  */
#line 446 "parser.y"
    {
      (yyval.expr) = FunctionCall((yyvsp[(1) - (4)].expr), (yyvsp[(3) - (4)].expr));
    }
    break;

  case 18:

/* Line 1455 of yacc.c  */
#line 450 "parser.y"
    {
      (yyval.expr) = DotField((yyvsp[(1) - (3)].expr), Symbol((yyvsp[(3) - (3)].name)));
    }
    break;

  case 19:

/* Line 1455 of yacc.c  */
#line 454 "parser.y"
    {
      (yyval.expr) = PtrField((yyvsp[(1) - (3)].expr), Symbol((yyvsp[(3) - (3)].name)));
    }
    break;

  case 20:

/* Line 1455 of yacc.c  */
#line 463 "parser.y"
    {
      (yyval.expr) = DotField((yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].symb));
    }
    break;

  case 21:

/* Line 1455 of yacc.c  */
#line 467 "parser.y"
    {
      (yyval.expr) = PtrField((yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].symb));
    }
    break;

  case 22:

/* Line 1455 of yacc.c  */
#line 471 "parser.y"
    {
      (yyval.expr) = PostOperator((yyvsp[(1) - (2)].expr), UOP_inc);
    }
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 475 "parser.y"
    {
      (yyval.expr) = PostOperator((yyvsp[(1) - (2)].expr), UOP_dec);
    }
    break;

  case 24:

/* Line 1455 of yacc.c  */
#line 479 "parser.y"
    {
      (yyval.expr) = CastedExpr((yyvsp[(2) - (6)].decl), BracedInitializer((yyvsp[(5) - (6)].expr)));
    }
    break;

  case 25:

/* Line 1455 of yacc.c  */
#line 483 "parser.y"
    {
      (yyval.expr) = CastedExpr((yyvsp[(2) - (7)].decl), BracedInitializer((yyvsp[(5) - (7)].expr)));
    }
    break;

  case 26:

/* Line 1455 of yacc.c  */
#line 491 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr); 
    }
    break;

  case 27:

/* Line 1455 of yacc.c  */
#line 495 "parser.y"
    {
      (yyval.expr) = CommaList((yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 28:

/* Line 1455 of yacc.c  */
#line 503 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 29:

/* Line 1455 of yacc.c  */
#line 507 "parser.y"
    {
      (yyval.expr) = PreOperator((yyvsp[(2) - (2)].expr), UOP_inc);
    }
    break;

  case 30:

/* Line 1455 of yacc.c  */
#line 511 "parser.y"
    {
      (yyval.expr) = PreOperator((yyvsp[(2) - (2)].expr), UOP_dec);
    }
    break;

  case 31:

/* Line 1455 of yacc.c  */
#line 515 "parser.y"
    {
      if ((yyvsp[(1) - (2)].type) == -1)
        (yyval.expr) = (yyvsp[(2) - (2)].expr);                    /* simplify */
      else
        (yyval.expr) = UnaryOperator((yyvsp[(1) - (2)].type), (yyvsp[(2) - (2)].expr));
    }
    break;

  case 32:

/* Line 1455 of yacc.c  */
#line 522 "parser.y"
    {
      (yyval.expr) = Sizeof((yyvsp[(2) - (2)].expr));
    }
    break;

  case 33:

/* Line 1455 of yacc.c  */
#line 526 "parser.y"
    {
      (yyval.expr) = Sizeoftype((yyvsp[(3) - (4)].decl));
    }
    break;

  case 34:

/* Line 1455 of yacc.c  */
#line 535 "parser.y"
    {
      (yyval.expr) = FunctionCall(Identifier(Symbol("__builtin_va_arg")),
                        CommaList((yyvsp[(3) - (6)].expr), TypeTrick((yyvsp[(5) - (6)].decl))));
    }
    break;

  case 35:

/* Line 1455 of yacc.c  */
#line 540 "parser.y"
    {
      (yyval.expr) = FunctionCall(Identifier(Symbol("__builtin_offsetof")),
                        CommaList(TypeTrick((yyvsp[(3) - (6)].decl)), Identifier(Symbol((yyvsp[(5) - (6)].name)))));
    }
    break;

  case 36:

/* Line 1455 of yacc.c  */
#line 545 "parser.y"
    {
      (yyval.expr) = FunctionCall(Identifier(Symbol("__builtin_types_compatible_p")),
                        CommaList(TypeTrick((yyvsp[(3) - (6)].decl)), TypeTrick((yyvsp[(5) - (6)].decl))));
    }
    break;

  case 37:

/* Line 1455 of yacc.c  */
#line 554 "parser.y"
    {
      (yyval.type) = UOP_addr;
    }
    break;

  case 38:

/* Line 1455 of yacc.c  */
#line 558 "parser.y"
    {
      (yyval.type) = UOP_star;
    }
    break;

  case 39:

/* Line 1455 of yacc.c  */
#line 562 "parser.y"
    {
      (yyval.type) = -1;         /* Ingore this one */
    }
    break;

  case 40:

/* Line 1455 of yacc.c  */
#line 566 "parser.y"
    {
      (yyval.type) = UOP_neg;
    }
    break;

  case 41:

/* Line 1455 of yacc.c  */
#line 570 "parser.y"
    {
      (yyval.type) = UOP_bnot;
    }
    break;

  case 42:

/* Line 1455 of yacc.c  */
#line 574 "parser.y"
    {
      (yyval.type) = UOP_lnot;
    }
    break;

  case 43:

/* Line 1455 of yacc.c  */
#line 582 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 44:

/* Line 1455 of yacc.c  */
#line 586 "parser.y"
    {
      (yyval.expr) = CastedExpr((yyvsp[(2) - (4)].decl), (yyvsp[(4) - (4)].expr));
    }
    break;

  case 45:

/* Line 1455 of yacc.c  */
#line 594 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 46:

/* Line 1455 of yacc.c  */
#line 598 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_mul, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 47:

/* Line 1455 of yacc.c  */
#line 602 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_div, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 48:

/* Line 1455 of yacc.c  */
#line 606 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_mod, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 49:

/* Line 1455 of yacc.c  */
#line 614 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 50:

/* Line 1455 of yacc.c  */
#line 618 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_add, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 51:

/* Line 1455 of yacc.c  */
#line 622 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_sub, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 52:

/* Line 1455 of yacc.c  */
#line 630 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 53:

/* Line 1455 of yacc.c  */
#line 634 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_shl, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 638 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_shr, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 55:

/* Line 1455 of yacc.c  */
#line 646 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 56:

/* Line 1455 of yacc.c  */
#line 650 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_lt, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 57:

/* Line 1455 of yacc.c  */
#line 654 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_gt, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 58:

/* Line 1455 of yacc.c  */
#line 658 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_leq, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
     }
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 662 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_geq, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 60:

/* Line 1455 of yacc.c  */
#line 670 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 674 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_eqeq, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 678 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_neq, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 63:

/* Line 1455 of yacc.c  */
#line 686 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 690 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_band, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 65:

/* Line 1455 of yacc.c  */
#line 698 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 702 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_xor, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 710 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 714 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_bor, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 69:

/* Line 1455 of yacc.c  */
#line 722 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 70:

/* Line 1455 of yacc.c  */
#line 726 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_land, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 71:

/* Line 1455 of yacc.c  */
#line 734 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 72:

/* Line 1455 of yacc.c  */
#line 738 "parser.y"
    {
      (yyval.expr) = BinaryOperator(BOP_lor, (yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 73:

/* Line 1455 of yacc.c  */
#line 746 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 74:

/* Line 1455 of yacc.c  */
#line 750 "parser.y"
    {
      (yyval.expr) = ConditionalExpr((yyvsp[(1) - (5)].expr), (yyvsp[(3) - (5)].expr), (yyvsp[(5) - (5)].expr));
    }
    break;

  case 75:

/* Line 1455 of yacc.c  */
#line 758 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 76:

/* Line 1455 of yacc.c  */
#line 762 "parser.y"
    {
      (yyval.expr) = Assignment((yyvsp[(1) - (3)].expr), (yyvsp[(2) - (3)].type), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 77:

/* Line 1455 of yacc.c  */
#line 770 "parser.y"
    {
      (yyval.type) = ASS_eq;  /* Need fix here! */
    }
    break;

  case 78:

/* Line 1455 of yacc.c  */
#line 774 "parser.y"
    {
      (yyval.type) = ASS_mul;
    }
    break;

  case 79:

/* Line 1455 of yacc.c  */
#line 778 "parser.y"
    {
      (yyval.type) = ASS_div;
    }
    break;

  case 80:

/* Line 1455 of yacc.c  */
#line 782 "parser.y"
    {
      (yyval.type) = ASS_mod;
    }
    break;

  case 81:

/* Line 1455 of yacc.c  */
#line 786 "parser.y"
    {
      (yyval.type) = ASS_add;
    }
    break;

  case 82:

/* Line 1455 of yacc.c  */
#line 790 "parser.y"
    {
      (yyval.type) = ASS_sub;
    }
    break;

  case 83:

/* Line 1455 of yacc.c  */
#line 794 "parser.y"
    {
      (yyval.type) = ASS_shl;
    }
    break;

  case 84:

/* Line 1455 of yacc.c  */
#line 798 "parser.y"
    {
      (yyval.type) = ASS_shr;
    }
    break;

  case 85:

/* Line 1455 of yacc.c  */
#line 802 "parser.y"
    {
      (yyval.type) = ASS_and;
    }
    break;

  case 86:

/* Line 1455 of yacc.c  */
#line 806 "parser.y"
    {
      (yyval.type) = ASS_xor;
    }
    break;

  case 87:

/* Line 1455 of yacc.c  */
#line 810 "parser.y"
    {
      (yyval.type) = ASS_or;
    }
    break;

  case 88:

/* Line 1455 of yacc.c  */
#line 818 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 89:

/* Line 1455 of yacc.c  */
#line 822 "parser.y"
    {
      (yyval.expr) = CommaList((yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 90:

/* Line 1455 of yacc.c  */
#line 830 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 91:

/* Line 1455 of yacc.c  */
#line 844 "parser.y"
    {
      /* There is a special case which wrongly uses this rule:
       *   typedef xxx alread_known_user_type.
       * In this case the already_known_user_type (T) is re-defined,
       * and because T is known, it is not considered as a declarator,
       * but a "typedef_name", and is part of the specifier.
       * We fix it here.
       */
      if (isTypedef && (yyvsp[(1) - (2)].spec)->type == SPECLIST)
        (yyval.stmt) = Declaration((yyvsp[(1) - (2)].spec), fix_known_typename((yyvsp[(1) - (2)].spec)));
      else
        (yyval.stmt) = Declaration((yyvsp[(1) - (2)].spec), NULL);
      isTypedef = 0;
    }
    break;

  case 92:

/* Line 1455 of yacc.c  */
#line 859 "parser.y"
    {
      (yyval.stmt) = Declaration((yyvsp[(1) - (3)].spec), (yyvsp[(2) - (3)].decl));
      if (checkDecls) add_declaration_links((yyvsp[(1) - (3)].spec), (yyvsp[(2) - (3)].decl));
      isTypedef = 0;
      
    }
    break;

  case 93:

/* Line 1455 of yacc.c  */
#line 866 "parser.y"
    {
      (yyval.stmt) = OmpStmt(OmpConstruct(DCTHREADPRIVATE, (yyvsp[(1) - (1)].odir), NULL));
    }
    break;

  case 94:

/* Line 1455 of yacc.c  */
#line 874 "parser.y"
    {
      (yyval.spec) = (yyvsp[(1) - (1)].spec);
    }
    break;

  case 95:

/* Line 1455 of yacc.c  */
#line 878 "parser.y"
    {
      (yyval.spec) = Speclist_right((yyvsp[(1) - (2)].spec), (yyvsp[(2) - (2)].spec));
    }
    break;

  case 96:

/* Line 1455 of yacc.c  */
#line 882 "parser.y"
    {
      (yyval.spec) = (yyvsp[(1) - (1)].spec);
    }
    break;

  case 97:

/* Line 1455 of yacc.c  */
#line 886 "parser.y"
    {
      (yyval.spec) = Speclist_right((yyvsp[(1) - (2)].spec), (yyvsp[(2) - (2)].spec));
    }
    break;

  case 98:

/* Line 1455 of yacc.c  */
#line 890 "parser.y"
    {
      (yyval.spec) = (yyvsp[(1) - (1)].spec);
    }
    break;

  case 99:

/* Line 1455 of yacc.c  */
#line 894 "parser.y"
    {
      (yyval.spec) = Speclist_right((yyvsp[(1) - (2)].spec), (yyvsp[(2) - (2)].spec));
    }
    break;

  case 100:

/* Line 1455 of yacc.c  */
#line 898 "parser.y"
    {
      (yyval.spec) = (yyvsp[(1) - (1)].spec);
    }
    break;

  case 101:

/* Line 1455 of yacc.c  */
#line 902 "parser.y"
    {
      (yyval.spec) = Speclist_right((yyvsp[(1) - (2)].spec), (yyvsp[(2) - (2)].spec));
    }
    break;

  case 102:

/* Line 1455 of yacc.c  */
#line 910 "parser.y"
    {
      (yyval.decl) = (yyvsp[(1) - (1)].decl);
    }
    break;

  case 103:

/* Line 1455 of yacc.c  */
#line 914 "parser.y"
    {
      (yyval.decl) = DeclList((yyvsp[(1) - (3)].decl), (yyvsp[(3) - (3)].decl));
    }
    break;

  case 104:

/* Line 1455 of yacc.c  */
#line 928 "parser.y"
    {
      astdecl s = decl_getidentifier((yyvsp[(1) - (1)].decl));
      int     declkind = decl_getkind((yyvsp[(1) - (1)].decl));
      stentry e;
      
      if (!isTypedef && declkind == DFUNC && strcmp(s->u.id->name, "main") == 0)
        s->u.id = Symbol(MAIN_NEWNAME);       /* Catch main()'s declaration */
      if (checkDecls) 
      {
        e = symtab_put(stab, s->u.id, (isTypedef) ? TYPENAME :
                                       (declkind == DFUNC) ? FUNCNAME : IDNAME);
        e->isarray = (declkind == DARRAY);
      }
      (yyval.decl) = (yyvsp[(1) - (1)].decl);
    }
    break;

  case 105:

/* Line 1455 of yacc.c  */
#line 944 "parser.y"
    {
      astdecl s = decl_getidentifier((yyvsp[(1) - (2)].decl));
      int     declkind = decl_getkind((yyvsp[(1) - (2)].decl));
      stentry e;
      
      if (!isTypedef && declkind == DFUNC && strcmp(s->u.id->name, "main") == 0)
        s->u.id = Symbol(MAIN_NEWNAME);         /* Catch main()'s declaration */
      if (checkDecls) 
      {
        e = symtab_put(stab, s->u.id, (isTypedef) ? TYPENAME :
                                       (declkind == DFUNC) ? FUNCNAME : IDNAME);
        e->isarray = (declkind == DARRAY);
      }
    }
    break;

  case 106:

/* Line 1455 of yacc.c  */
#line 959 "parser.y"
    {
      (yyval.decl) = InitDecl((yyvsp[(1) - (4)].decl), (yyvsp[(4) - (4)].expr));
    }
    break;

  case 107:

/* Line 1455 of yacc.c  */
#line 967 "parser.y"
    {
      (yyval.spec) = StClassSpec(SPEC_typedef);    /* Just a string */
      isTypedef = 1;
    }
    break;

  case 108:

/* Line 1455 of yacc.c  */
#line 972 "parser.y"
    {
      (yyval.spec) = StClassSpec(SPEC_extern);
    }
    break;

  case 109:

/* Line 1455 of yacc.c  */
#line 976 "parser.y"
    {
      (yyval.spec) = StClassSpec(SPEC_static);
    }
    break;

  case 110:

/* Line 1455 of yacc.c  */
#line 980 "parser.y"
    {
      (yyval.spec) = StClassSpec(SPEC_auto);
    }
    break;

  case 111:

/* Line 1455 of yacc.c  */
#line 984 "parser.y"
    {
      (yyval.spec) = StClassSpec(SPEC_register);
    }
    break;

  case 112:

/* Line 1455 of yacc.c  */
#line 992 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_void);
    }
    break;

  case 113:

/* Line 1455 of yacc.c  */
#line 996 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_char);
    }
    break;

  case 114:

/* Line 1455 of yacc.c  */
#line 1000 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_short);
    }
    break;

  case 115:

/* Line 1455 of yacc.c  */
#line 1004 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_int);
    }
    break;

  case 116:

/* Line 1455 of yacc.c  */
#line 1008 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_long);
    }
    break;

  case 117:

/* Line 1455 of yacc.c  */
#line 1012 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_float);
    }
    break;

  case 118:

/* Line 1455 of yacc.c  */
#line 1016 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_double);
    }
    break;

  case 119:

/* Line 1455 of yacc.c  */
#line 1020 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_signed);
    }
    break;

  case 120:

/* Line 1455 of yacc.c  */
#line 1024 "parser.y"
    { 
      (yyval.spec) = Declspec(SPEC_unsigned);
    }
    break;

  case 121:

/* Line 1455 of yacc.c  */
#line 1028 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_ubool);
    }
    break;

  case 122:

/* Line 1455 of yacc.c  */
#line 1032 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_ucomplex);
    }
    break;

  case 123:

/* Line 1455 of yacc.c  */
#line 1036 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_uimaginary);
    }
    break;

  case 124:

/* Line 1455 of yacc.c  */
#line 1040 "parser.y"
    {
      (yyval.spec) = (yyvsp[(1) - (1)].spec);
    }
    break;

  case 125:

/* Line 1455 of yacc.c  */
#line 1044 "parser.y"
    {
      (yyval.spec) = (yyvsp[(1) - (1)].spec);
    }
    break;

  case 126:

/* Line 1455 of yacc.c  */
#line 1048 "parser.y"
    {
      (yyval.spec) = Usertype((yyvsp[(1) - (1)].symb));
    }
    break;

  case 127:

/* Line 1455 of yacc.c  */
#line 1056 "parser.y"
    {
      (yyval.spec) = SUdecl((yyvsp[(1) - (4)].type), NULL, (yyvsp[(3) - (4)].decl));
    }
    break;

  case 128:

/* Line 1455 of yacc.c  */
#line 1060 "parser.y"
    {
      (yyval.spec) = SUdecl((yyvsp[(1) - (3)].type), NULL, NULL);
    }
    break;

  case 129:

/* Line 1455 of yacc.c  */
#line 1064 "parser.y"
    {
      symbol s = Symbol((yyvsp[(2) - (5)].name));
      /* Well, struct & union names have their own name space, and
       * their own scopes. I.e. they can be re-declare in nested
       * scopes. We don't do any kind of duplicate checks.
       */
      if (checkDecls) 
        symtab_put(stab, s, SUNAME);
      (yyval.spec) = SUdecl((yyvsp[(1) - (5)].type), s, (yyvsp[(4) - (5)].decl));
    }
    break;

  case 130:

/* Line 1455 of yacc.c  */
#line 1079 "parser.y"
    {
      symbol s = (yyvsp[(2) - (5)].symb);
      if (checkDecls) 
        symtab_put(stab, s, SUNAME);
      (yyval.spec) = SUdecl((yyvsp[(1) - (5)].type), s, (yyvsp[(4) - (5)].decl));
    }
    break;

  case 131:

/* Line 1455 of yacc.c  */
#line 1086 "parser.y"
    {
      symbol s = Symbol((yyvsp[(2) - (2)].name));
      if (checkDecls) 
        symtab_put(stab, s, SUNAME);
      (yyval.spec) = SUdecl((yyvsp[(1) - (2)].type), s, NULL);
    }
    break;

  case 132:

/* Line 1455 of yacc.c  */
#line 1093 "parser.y"
    {
      symbol s = (yyvsp[(2) - (2)].symb);
      if (checkDecls) 
        symtab_put(stab, s, SUNAME);
      (yyval.spec) = SUdecl((yyvsp[(1) - (2)].type), s, NULL);
    }
    break;

  case 133:

/* Line 1455 of yacc.c  */
#line 1104 "parser.y"
    {
      (yyval.type) = SPEC_struct;
    }
    break;

  case 134:

/* Line 1455 of yacc.c  */
#line 1108 "parser.y"
    {
      (yyval.type) = SPEC_union;
    }
    break;

  case 135:

/* Line 1455 of yacc.c  */
#line 1116 "parser.y"
    {
      (yyval.decl) = (yyvsp[(1) - (1)].decl);
    }
    break;

  case 136:

/* Line 1455 of yacc.c  */
#line 1120 "parser.y"
    {
      (yyval.decl) = StructfieldList((yyvsp[(1) - (2)].decl), (yyvsp[(2) - (2)].decl));
    }
    break;

  case 137:

/* Line 1455 of yacc.c  */
#line 1128 "parser.y"
    {
      (yyval.decl) = StructfieldDecl((yyvsp[(1) - (3)].spec), (yyvsp[(2) - (3)].decl));
    }
    break;

  case 138:

/* Line 1455 of yacc.c  */
#line 1132 "parser.y"
    {
      (yyval.decl) = StructfieldDecl((yyvsp[(1) - (2)].spec), NULL);
    }
    break;

  case 139:

/* Line 1455 of yacc.c  */
#line 1140 "parser.y"
    {
      (yyval.spec) = (yyvsp[(1) - (1)].spec);
    }
    break;

  case 140:

/* Line 1455 of yacc.c  */
#line 1144 "parser.y"
    {
      (yyval.spec) = Speclist_right((yyvsp[(1) - (2)].spec), (yyvsp[(2) - (2)].spec));
    }
    break;

  case 141:

/* Line 1455 of yacc.c  */
#line 1148 "parser.y"
    {
      (yyval.spec) = (yyvsp[(1) - (1)].spec);
    }
    break;

  case 142:

/* Line 1455 of yacc.c  */
#line 1152 "parser.y"
    {
      (yyval.spec) = Speclist_right((yyvsp[(1) - (2)].spec), (yyvsp[(2) - (2)].spec));
    }
    break;

  case 143:

/* Line 1455 of yacc.c  */
#line 1160 "parser.y"
    {
      (yyval.decl) = (yyvsp[(1) - (1)].decl);
    }
    break;

  case 144:

/* Line 1455 of yacc.c  */
#line 1164 "parser.y"
    {
      (yyval.decl) = DeclList((yyvsp[(1) - (3)].decl), (yyvsp[(3) - (3)].decl));
    }
    break;

  case 145:

/* Line 1455 of yacc.c  */
#line 1172 "parser.y"
    {
      (yyval.decl) = (yyvsp[(1) - (1)].decl);
    }
    break;

  case 146:

/* Line 1455 of yacc.c  */
#line 1176 "parser.y"
    {
      (yyval.decl) = BitDecl((yyvsp[(1) - (3)].decl), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 147:

/* Line 1455 of yacc.c  */
#line 1180 "parser.y"
    {
      (yyval.decl) = BitDecl(NULL, (yyvsp[(2) - (2)].expr));
    }
    break;

  case 148:

/* Line 1455 of yacc.c  */
#line 1188 "parser.y"
    {
      (yyval.spec) = Enumdecl(NULL, (yyvsp[(3) - (4)].spec));
    }
    break;

  case 149:

/* Line 1455 of yacc.c  */
#line 1192 "parser.y"
    {
      symbol s = Symbol((yyvsp[(2) - (5)].name));

      if (checkDecls) 
      {
        if (symtab_get(stab, s, ENUMNAME))
          parse_error(-1, "enum name '%s' is already in use.", (yyvsp[(2) - (5)].name));
        symtab_put(stab, s, ENUMNAME);
      }
      (yyval.spec) = Enumdecl(s, (yyvsp[(4) - (5)].spec));
    }
    break;

  case 150:

/* Line 1455 of yacc.c  */
#line 1204 "parser.y"
    {
      (yyval.spec) = Enumdecl(NULL, (yyvsp[(3) - (5)].spec));
    }
    break;

  case 151:

/* Line 1455 of yacc.c  */
#line 1208 "parser.y"
    {
      symbol s = Symbol((yyvsp[(2) - (6)].name));

      if (checkDecls) 
      {
        if (symtab_get(stab, s, ENUMNAME))
          parse_error(-1, "enum name '%s' is already in use.", (yyvsp[(2) - (6)].name));
        symtab_put(stab, s, ENUMNAME);
      }
      (yyval.spec) = Enumdecl(s, (yyvsp[(4) - (6)].spec));
    }
    break;

  case 152:

/* Line 1455 of yacc.c  */
#line 1220 "parser.y"
    {
      /*
      if (symtab_get(stab, s, ENUMNAME))
        parse_error(-1, "enum name '%s' is unknown.", $2);
      */
      (yyval.spec) = Enumdecl(Symbol((yyvsp[(2) - (2)].name)), NULL);
    }
    break;

  case 153:

/* Line 1455 of yacc.c  */
#line 1232 "parser.y"
    {
      (yyval.spec) = (yyvsp[(1) - (1)].spec);
    }
    break;

  case 154:

/* Line 1455 of yacc.c  */
#line 1236 "parser.y"
    {
      (yyval.spec) = Enumbodylist((yyvsp[(1) - (3)].spec), (yyvsp[(3) - (3)].spec));
    }
    break;

  case 155:

/* Line 1455 of yacc.c  */
#line 1244 "parser.y"
    {
      (yyval.spec) = Enumerator((yyvsp[(1) - (1)].symb), NULL);
    }
    break;

  case 156:

/* Line 1455 of yacc.c  */
#line 1248 "parser.y"
    {
      (yyval.spec) = Enumerator((yyvsp[(1) - (3)].symb), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 157:

/* Line 1455 of yacc.c  */
#line 1256 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_const);
    }
    break;

  case 158:

/* Line 1455 of yacc.c  */
#line 1260 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_restrict);
    }
    break;

  case 159:

/* Line 1455 of yacc.c  */
#line 1264 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_volatile);
    }
    break;

  case 160:

/* Line 1455 of yacc.c  */
#line 1272 "parser.y"
    {
      (yyval.spec) = Declspec(SPEC_inline);
    }
    break;

  case 161:

/* Line 1455 of yacc.c  */
#line 1280 "parser.y"
    {
      (yyval.decl) = Declarator(NULL, (yyvsp[(1) - (1)].decl));
    }
    break;

  case 162:

/* Line 1455 of yacc.c  */
#line 1284 "parser.y"
    {
      (yyval.decl) = Declarator((yyvsp[(1) - (2)].spec), (yyvsp[(2) - (2)].decl));
    }
    break;

  case 163:

/* Line 1455 of yacc.c  */
#line 1297 "parser.y"
    {
      (yyval.decl) = IdentifierDecl( Symbol((yyvsp[(1) - (1)].name)) );
    }
    break;

  case 164:

/* Line 1455 of yacc.c  */
#line 1301 "parser.y"
    {
      /* Try to simplify a bit: (ident) -> ident */
      if ((yyvsp[(2) - (3)].decl)->spec == NULL && (yyvsp[(2) - (3)].decl)->decl->type == DIDENT)
        (yyval.decl) = (yyvsp[(2) - (3)].decl)->decl;
      else
        (yyval.decl) = ParenDecl((yyvsp[(2) - (3)].decl));
    }
    break;

  case 165:

/* Line 1455 of yacc.c  */
#line 1309 "parser.y"
    {
      (yyval.decl) = ArrayDecl((yyvsp[(1) - (3)].decl), NULL, NULL);
    }
    break;

  case 166:

/* Line 1455 of yacc.c  */
#line 1313 "parser.y"
    {
      (yyval.decl) = ArrayDecl((yyvsp[(1) - (4)].decl), (yyvsp[(3) - (4)].spec), NULL);
    }
    break;

  case 167:

/* Line 1455 of yacc.c  */
#line 1317 "parser.y"
    {
      (yyval.decl) = ArrayDecl((yyvsp[(1) - (4)].decl), NULL, (yyvsp[(3) - (4)].expr));
    }
    break;

  case 168:

/* Line 1455 of yacc.c  */
#line 1321 "parser.y"
    {
      (yyval.decl) = ArrayDecl((yyvsp[(1) - (5)].decl), (yyvsp[(3) - (5)].spec), (yyvsp[(4) - (5)].expr));
    }
    break;

  case 169:

/* Line 1455 of yacc.c  */
#line 1325 "parser.y"
    {
      (yyval.decl) = ArrayDecl((yyvsp[(1) - (5)].decl), StClassSpec(SPEC_static), (yyvsp[(4) - (5)].expr));
    }
    break;

  case 170:

/* Line 1455 of yacc.c  */
#line 1329 "parser.y"
    {
      (yyval.decl) = ArrayDecl((yyvsp[(1) - (6)].decl), Speclist_right( StClassSpec(SPEC_static), (yyvsp[(4) - (6)].spec) ), (yyvsp[(5) - (6)].expr));
    }
    break;

  case 171:

/* Line 1455 of yacc.c  */
#line 1333 "parser.y"
    {
      (yyval.decl) = ArrayDecl((yyvsp[(1) - (6)].decl), Speclist_left( (yyvsp[(3) - (6)].spec), StClassSpec(SPEC_static) ), (yyvsp[(5) - (6)].expr));
    }
    break;

  case 172:

/* Line 1455 of yacc.c  */
#line 1337 "parser.y"
    {
      (yyval.decl) = ArrayDecl((yyvsp[(1) - (4)].decl), Declspec(SPEC_star), NULL);
    }
    break;

  case 173:

/* Line 1455 of yacc.c  */
#line 1341 "parser.y"
    {
      (yyval.decl) = ArrayDecl((yyvsp[(1) - (5)].decl), Speclist_left( (yyvsp[(3) - (5)].spec), Declspec(SPEC_star) ), NULL);
    }
    break;

  case 174:

/* Line 1455 of yacc.c  */
#line 1345 "parser.y"
    {
      (yyval.decl) = FuncDecl((yyvsp[(1) - (4)].decl), (yyvsp[(3) - (4)].decl));
    }
    break;

  case 175:

/* Line 1455 of yacc.c  */
#line 1349 "parser.y"
    {
      (yyval.decl) = FuncDecl((yyvsp[(1) - (3)].decl), NULL);
    }
    break;

  case 176:

/* Line 1455 of yacc.c  */
#line 1353 "parser.y"
    {
      (yyval.decl) = FuncDecl((yyvsp[(1) - (4)].decl), (yyvsp[(3) - (4)].decl));
    }
    break;

  case 177:

/* Line 1455 of yacc.c  */
#line 1361 "parser.y"
    {
      (yyval.spec) = Pointer();
    }
    break;

  case 178:

/* Line 1455 of yacc.c  */
#line 1365 "parser.y"
    {
      (yyval.spec) = Speclist_right(Pointer(), (yyvsp[(2) - (2)].spec));
    }
    break;

  case 179:

/* Line 1455 of yacc.c  */
#line 1369 "parser.y"
    {
      (yyval.spec) = Speclist_right(Pointer(), (yyvsp[(2) - (2)].spec));
    }
    break;

  case 180:

/* Line 1455 of yacc.c  */
#line 1373 "parser.y"
    {
      (yyval.spec) = Speclist_right( Pointer(), Speclist_left((yyvsp[(2) - (3)].spec), (yyvsp[(3) - (3)].spec)) );
    }
    break;

  case 181:

/* Line 1455 of yacc.c  */
#line 1381 "parser.y"
    {
      (yyval.spec) = (yyvsp[(1) - (1)].spec);
    }
    break;

  case 182:

/* Line 1455 of yacc.c  */
#line 1385 "parser.y"
    {
      (yyval.spec) = Speclist_left((yyvsp[(1) - (2)].spec), (yyvsp[(2) - (2)].spec));
    }
    break;

  case 183:

/* Line 1455 of yacc.c  */
#line 1393 "parser.y"
    {
      (yyval.decl) = (yyvsp[(1) - (1)].decl);
    }
    break;

  case 184:

/* Line 1455 of yacc.c  */
#line 1397 "parser.y"
    {
      (yyval.decl) = ParamList((yyvsp[(1) - (3)].decl), Ellipsis());
    }
    break;

  case 185:

/* Line 1455 of yacc.c  */
#line 1405 "parser.y"
    {
      (yyval.decl) = (yyvsp[(1) - (1)].decl);
    }
    break;

  case 186:

/* Line 1455 of yacc.c  */
#line 1409 "parser.y"
    {
      (yyval.decl) = ParamList((yyvsp[(1) - (3)].decl), (yyvsp[(3) - (3)].decl));
    }
    break;

  case 187:

/* Line 1455 of yacc.c  */
#line 1417 "parser.y"
    {
      (yyval.decl) = ParamDecl((yyvsp[(1) - (2)].spec), (yyvsp[(2) - (2)].decl));
    }
    break;

  case 188:

/* Line 1455 of yacc.c  */
#line 1421 "parser.y"
    {
      (yyval.decl) = ParamDecl((yyvsp[(1) - (1)].spec), NULL);
    }
    break;

  case 189:

/* Line 1455 of yacc.c  */
#line 1425 "parser.y"
    {
      (yyval.decl) = ParamDecl((yyvsp[(1) - (2)].spec), (yyvsp[(2) - (2)].decl));
    }
    break;

  case 190:

/* Line 1455 of yacc.c  */
#line 1433 "parser.y"
    {
      (yyval.decl) = IdentifierDecl( Symbol((yyvsp[(1) - (1)].name)) );
    }
    break;

  case 191:

/* Line 1455 of yacc.c  */
#line 1437 "parser.y"
    {
      (yyval.decl) = IdList((yyvsp[(1) - (3)].decl), IdentifierDecl( Symbol((yyvsp[(3) - (3)].name)) ));
    }
    break;

  case 192:

/* Line 1455 of yacc.c  */
#line 1445 "parser.y"
    {
      (yyval.decl) = Casttypename((yyvsp[(1) - (1)].spec), NULL);
    }
    break;

  case 193:

/* Line 1455 of yacc.c  */
#line 1449 "parser.y"
    {
      (yyval.decl) = Casttypename((yyvsp[(1) - (2)].spec), (yyvsp[(2) - (2)].decl));
    }
    break;

  case 194:

/* Line 1455 of yacc.c  */
#line 1457 "parser.y"
    {
      (yyval.decl) = AbstractDeclarator((yyvsp[(1) - (1)].spec), NULL);
    }
    break;

  case 195:

/* Line 1455 of yacc.c  */
#line 1461 "parser.y"
    {
      (yyval.decl) = AbstractDeclarator(NULL, (yyvsp[(1) - (1)].decl));
    }
    break;

  case 196:

/* Line 1455 of yacc.c  */
#line 1465 "parser.y"
    {
      (yyval.decl) = AbstractDeclarator((yyvsp[(1) - (2)].spec), (yyvsp[(2) - (2)].decl));
    }
    break;

  case 197:

/* Line 1455 of yacc.c  */
#line 1473 "parser.y"
    {
      (yyval.decl) = ParenDecl((yyvsp[(2) - (3)].decl));
    }
    break;

  case 198:

/* Line 1455 of yacc.c  */
#line 1477 "parser.y"
    {
      (yyval.decl) = ArrayDecl(NULL, NULL, NULL);
    }
    break;

  case 199:

/* Line 1455 of yacc.c  */
#line 1481 "parser.y"
    {
      (yyval.decl) = ArrayDecl((yyvsp[(1) - (3)].decl), NULL, NULL);
    }
    break;

  case 200:

/* Line 1455 of yacc.c  */
#line 1485 "parser.y"
    {
      (yyval.decl) = ArrayDecl(NULL, NULL, (yyvsp[(2) - (3)].expr));
    }
    break;

  case 201:

/* Line 1455 of yacc.c  */
#line 1489 "parser.y"
    {
      (yyval.decl) = ArrayDecl((yyvsp[(1) - (4)].decl), NULL, (yyvsp[(3) - (4)].expr));
    }
    break;

  case 202:

/* Line 1455 of yacc.c  */
#line 1493 "parser.y"
    {
      (yyval.decl) = ArrayDecl(NULL, Declspec(SPEC_star), NULL);
    }
    break;

  case 203:

/* Line 1455 of yacc.c  */
#line 1497 "parser.y"
    {
      (yyval.decl) = ArrayDecl((yyvsp[(1) - (4)].decl), Declspec(SPEC_star), NULL);
    }
    break;

  case 204:

/* Line 1455 of yacc.c  */
#line 1501 "parser.y"
    {
      (yyval.decl) = FuncDecl(NULL, NULL);
    }
    break;

  case 205:

/* Line 1455 of yacc.c  */
#line 1505 "parser.y"
    {
      (yyval.decl) = FuncDecl((yyvsp[(1) - (3)].decl), NULL);
    }
    break;

  case 206:

/* Line 1455 of yacc.c  */
#line 1509 "parser.y"
    {
      (yyval.decl) = FuncDecl(NULL, (yyvsp[(2) - (3)].decl));
    }
    break;

  case 207:

/* Line 1455 of yacc.c  */
#line 1513 "parser.y"
    {
      (yyval.decl) = FuncDecl((yyvsp[(1) - (4)].decl), (yyvsp[(3) - (4)].decl));
    }
    break;

  case 208:

/* Line 1455 of yacc.c  */
#line 1521 "parser.y"
    {
      (yyval.symb) = Symbol((yyvsp[(1) - (1)].name));
    }
    break;

  case 209:

/* Line 1455 of yacc.c  */
#line 1529 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 210:

/* Line 1455 of yacc.c  */
#line 1533 "parser.y"
    {
      (yyval.expr) = BracedInitializer((yyvsp[(2) - (3)].expr));
    }
    break;

  case 211:

/* Line 1455 of yacc.c  */
#line 1537 "parser.y"
    {
      (yyval.expr) = BracedInitializer((yyvsp[(2) - (4)].expr));
    }
    break;

  case 212:

/* Line 1455 of yacc.c  */
#line 1545 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 213:

/* Line 1455 of yacc.c  */
#line 1549 "parser.y"
    {
      (yyval.expr) = Designated((yyvsp[(1) - (2)].expr), (yyvsp[(2) - (2)].expr));
    }
    break;

  case 214:

/* Line 1455 of yacc.c  */
#line 1553 "parser.y"
    {
      (yyval.expr) = CommaList((yyvsp[(1) - (3)].expr), (yyvsp[(3) - (3)].expr));
    }
    break;

  case 215:

/* Line 1455 of yacc.c  */
#line 1557 "parser.y"
    {
      (yyval.expr) = CommaList((yyvsp[(1) - (4)].expr), Designated((yyvsp[(3) - (4)].expr), (yyvsp[(4) - (4)].expr)));
    }
    break;

  case 216:

/* Line 1455 of yacc.c  */
#line 1565 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (2)].expr); 
    }
    break;

  case 217:

/* Line 1455 of yacc.c  */
#line 1573 "parser.y"
    {
      (yyval.expr) = (yyvsp[(1) - (1)].expr);
    }
    break;

  case 218:

/* Line 1455 of yacc.c  */
#line 1577 "parser.y"
    {
      (yyval.expr) = SpaceList((yyvsp[(1) - (2)].expr), (yyvsp[(2) - (2)].expr));
    }
    break;

  case 219:

/* Line 1455 of yacc.c  */
#line 1585 "parser.y"
    {
      (yyval.expr) = IdxDesignator((yyvsp[(2) - (3)].expr));
    }
    break;

  case 220:

/* Line 1455 of yacc.c  */
#line 1589 "parser.y"
    {
      (yyval.expr) = DotDesignator( Symbol((yyvsp[(2) - (2)].name)) );
    }
    break;

  case 221:

/* Line 1455 of yacc.c  */
#line 1593 "parser.y"
    {
      (yyval.expr) = DotDesignator((yyvsp[(2) - (2)].symb));
    }
    break;

  case 222:

/* Line 1455 of yacc.c  */
#line 1607 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 223:

/* Line 1455 of yacc.c  */
#line 1611 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 224:

/* Line 1455 of yacc.c  */
#line 1615 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 225:

/* Line 1455 of yacc.c  */
#line 1619 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 226:

/* Line 1455 of yacc.c  */
#line 1623 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 227:

/* Line 1455 of yacc.c  */
#line 1627 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 228:

/* Line 1455 of yacc.c  */
#line 1631 "parser.y"
    {
      (yyval.stmt) = OmpStmt((yyvsp[(1) - (1)].ocon));
      (yyval.stmt)->l = (yyvsp[(1) - (1)].ocon)->l;
    }
    break;

  case 229:

/* Line 1455 of yacc.c  */
#line 1636 "parser.y"
    {
      (yyval.stmt) = OmpixStmt((yyvsp[(1) - (1)].xcon));
      (yyval.stmt)->l = (yyvsp[(1) - (1)].xcon)->l;
    }
    break;

  case 230:

/* Line 1455 of yacc.c  */
#line 1645 "parser.y"
    {
      (yyval.stmt) = Labeled( Symbol((yyvsp[(1) - (3)].name)), (yyvsp[(3) - (3)].stmt) );
    }
    break;

  case 231:

/* Line 1455 of yacc.c  */
#line 1649 "parser.y"
    {
      (yyval.stmt) = Case((yyvsp[(2) - (4)].expr), (yyvsp[(4) - (4)].stmt));
    }
    break;

  case 232:

/* Line 1455 of yacc.c  */
#line 1653 "parser.y"
    {
      (yyval.stmt) = Default((yyvsp[(3) - (3)].stmt));
    }
    break;

  case 233:

/* Line 1455 of yacc.c  */
#line 1661 "parser.y"
    {
      (yyval.stmt) = Compound(NULL);
    }
    break;

  case 234:

/* Line 1455 of yacc.c  */
#line 1664 "parser.y"
    { (yyval.type) = sc_original_line()-1; scope_start(stab); }
    break;

  case 235:

/* Line 1455 of yacc.c  */
#line 1666 "parser.y"
    {
      (yyval.stmt) = Compound((yyvsp[(3) - (4)].stmt));
      scope_end(stab);
      (yyval.stmt)->l = (yyvsp[(2) - (4)].type);     /* Remember 1st line */
    }
    break;

  case 236:

/* Line 1455 of yacc.c  */
#line 1676 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 237:

/* Line 1455 of yacc.c  */
#line 1680 "parser.y"
    {
      (yyval.stmt) = BlockList((yyvsp[(1) - (2)].stmt), (yyvsp[(2) - (2)].stmt));
      (yyval.stmt)->l = (yyvsp[(1) - (2)].stmt)->l;
    }
    break;

  case 238:

/* Line 1455 of yacc.c  */
#line 1689 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 239:

/* Line 1455 of yacc.c  */
#line 1693 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 240:

/* Line 1455 of yacc.c  */
#line 1697 "parser.y"
    {
      (yyval.stmt) = OmpStmt((yyvsp[(1) - (1)].ocon));
      (yyval.stmt)->l = (yyvsp[(1) - (1)].ocon)->l;
    }
    break;

  case 241:

/* Line 1455 of yacc.c  */
#line 1702 "parser.y"
    {
      (yyval.stmt) = OmpixStmt((yyvsp[(1) - (1)].xcon));
      (yyval.stmt)->l = (yyvsp[(1) - (1)].xcon)->l;
    }
    break;

  case 242:

/* Line 1455 of yacc.c  */
#line 1711 "parser.y"
    {
      (yyval.stmt) = Expression(NULL);
    }
    break;

  case 243:

/* Line 1455 of yacc.c  */
#line 1715 "parser.y"
    {
      (yyval.stmt) = Expression((yyvsp[(1) - (2)].expr));
      (yyval.stmt)->l = (yyvsp[(1) - (2)].expr)->l;
    }
    break;

  case 244:

/* Line 1455 of yacc.c  */
#line 1724 "parser.y"
    {
      (yyval.stmt) = If((yyvsp[(3) - (5)].expr), (yyvsp[(5) - (5)].stmt), NULL);
    }
    break;

  case 245:

/* Line 1455 of yacc.c  */
#line 1728 "parser.y"
    {
      (yyval.stmt) = If((yyvsp[(3) - (7)].expr), (yyvsp[(5) - (7)].stmt), (yyvsp[(7) - (7)].stmt));
    }
    break;

  case 246:

/* Line 1455 of yacc.c  */
#line 1732 "parser.y"
    {
      (yyval.stmt) = Switch((yyvsp[(3) - (5)].expr), (yyvsp[(5) - (5)].stmt));
    }
    break;

  case 247:

/* Line 1455 of yacc.c  */
#line 1741 "parser.y"
    {
      (yyval.stmt) = While((yyvsp[(3) - (5)].expr), (yyvsp[(5) - (5)].stmt));
    }
    break;

  case 248:

/* Line 1455 of yacc.c  */
#line 1745 "parser.y"
    {
      (yyval.stmt) = Do((yyvsp[(2) - (7)].stmt), (yyvsp[(5) - (7)].expr));
    }
    break;

  case 250:

/* Line 1455 of yacc.c  */
#line 1753 "parser.y"
    {
      (yyval.stmt) = For(NULL, NULL, NULL, (yyvsp[(6) - (6)].stmt));
    }
    break;

  case 251:

/* Line 1455 of yacc.c  */
#line 1757 "parser.y"
    {
      (yyval.stmt) = For(Expression((yyvsp[(3) - (7)].expr)), NULL, NULL, (yyvsp[(7) - (7)].stmt));
    }
    break;

  case 252:

/* Line 1455 of yacc.c  */
#line 1761 "parser.y"
    {
      (yyval.stmt) = For(NULL, (yyvsp[(4) - (7)].expr), NULL, (yyvsp[(7) - (7)].stmt));
    }
    break;

  case 253:

/* Line 1455 of yacc.c  */
#line 1765 "parser.y"
    {
      (yyval.stmt) = For(NULL, NULL, (yyvsp[(5) - (7)].expr), (yyvsp[(7) - (7)].stmt));
    }
    break;

  case 254:

/* Line 1455 of yacc.c  */
#line 1769 "parser.y"
    {
      (yyval.stmt) = For(Expression((yyvsp[(3) - (8)].expr)), (yyvsp[(5) - (8)].expr), NULL, (yyvsp[(8) - (8)].stmt));
    }
    break;

  case 255:

/* Line 1455 of yacc.c  */
#line 1773 "parser.y"
    {
      (yyval.stmt) = For(Expression((yyvsp[(3) - (8)].expr)), NULL, (yyvsp[(6) - (8)].expr), (yyvsp[(8) - (8)].stmt));
    }
    break;

  case 256:

/* Line 1455 of yacc.c  */
#line 1777 "parser.y"
    {
      (yyval.stmt) = For(NULL, (yyvsp[(4) - (8)].expr), (yyvsp[(6) - (8)].expr), (yyvsp[(8) - (8)].stmt));
    }
    break;

  case 257:

/* Line 1455 of yacc.c  */
#line 1781 "parser.y"
    {
      (yyval.stmt) = For(Expression((yyvsp[(3) - (9)].expr)), (yyvsp[(5) - (9)].expr), (yyvsp[(7) - (9)].expr), (yyvsp[(9) - (9)].stmt));
    }
    break;

  case 258:

/* Line 1455 of yacc.c  */
#line 1785 "parser.y"
    {
      (yyval.stmt) = For((yyvsp[(3) - (6)].stmt), NULL, NULL, (yyvsp[(6) - (6)].stmt));
    }
    break;

  case 259:

/* Line 1455 of yacc.c  */
#line 1789 "parser.y"
    {
      (yyval.stmt) = For((yyvsp[(3) - (7)].stmt), (yyvsp[(4) - (7)].expr), NULL, (yyvsp[(7) - (7)].stmt));
    }
    break;

  case 260:

/* Line 1455 of yacc.c  */
#line 1793 "parser.y"
    {
      (yyval.stmt) = For((yyvsp[(3) - (7)].stmt), NULL, (yyvsp[(5) - (7)].expr), (yyvsp[(7) - (7)].stmt));
    }
    break;

  case 261:

/* Line 1455 of yacc.c  */
#line 1797 "parser.y"
    {
      (yyval.stmt) = For((yyvsp[(3) - (8)].stmt), (yyvsp[(4) - (8)].expr), (yyvsp[(6) - (8)].expr), (yyvsp[(8) - (8)].stmt));
    }
    break;

  case 262:

/* Line 1455 of yacc.c  */
#line 1805 "parser.y"
    {
      /* We don't keep track of labels -- we leave it to the native compiler */
      (yyval.stmt) = Goto( Symbol((yyvsp[(2) - (3)].name)) );
    }
    break;

  case 263:

/* Line 1455 of yacc.c  */
#line 1810 "parser.y"
    {
      (yyval.stmt) = Continue();
    }
    break;

  case 264:

/* Line 1455 of yacc.c  */
#line 1814 "parser.y"
    {
      (yyval.stmt) = Break();
    }
    break;

  case 265:

/* Line 1455 of yacc.c  */
#line 1818 "parser.y"
    {
      (yyval.stmt) = Return(NULL);
    }
    break;

  case 266:

/* Line 1455 of yacc.c  */
#line 1822 "parser.y"
    {
      (yyval.stmt) = Return((yyvsp[(2) - (3)].expr));
    }
    break;

  case 267:

/* Line 1455 of yacc.c  */
#line 1836 "parser.y"
    {
      (yyval.stmt) = pastree = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 268:

/* Line 1455 of yacc.c  */
#line 1840 "parser.y"
    {
      (yyval.stmt) = pastree = BlockList((yyvsp[(1) - (2)].stmt), (yyvsp[(2) - (2)].stmt));
    }
    break;

  case 269:

/* Line 1455 of yacc.c  */
#line 1848 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 270:

/* Line 1455 of yacc.c  */
#line 1852 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 271:

/* Line 1455 of yacc.c  */
#line 1859 "parser.y"
    {
      (yyval.stmt) = OmpixStmt((yyvsp[(1) - (1)].xcon));
    }
    break;

  case 272:

/* Line 1455 of yacc.c  */
#line 1870 "parser.y"
    { (yyval.stmt) = (yyvsp[(1) - (1)].stmt); }
    break;

  case 273:

/* Line 1455 of yacc.c  */
#line 1871 "parser.y"
    { (yyval.stmt) = (yyvsp[(1) - (1)].stmt); }
    break;

  case 274:

/* Line 1455 of yacc.c  */
#line 1876 "parser.y"
    {
      if (isTypedef || (yyvsp[(2) - (2)].decl)->decl->type != DFUNC)
        parse_error(1, "function definition cannot be parsed.\n");
      if (symtab_get(stab, decl_getidentifier_symbol((yyvsp[(2) - (2)].decl)), FUNCNAME) == NULL)
        symtab_put(stab, decl_getidentifier_symbol((yyvsp[(2) - (2)].decl)), FUNCNAME);
      
      scope_start(stab);
      ast_declare_function_params((yyvsp[(2) - (2)].decl));
    }
    break;

  case 275:

/* Line 1455 of yacc.c  */
#line 1886 "parser.y"
    {
      scope_end(stab);
      check_for_main_and_declare((yyvsp[(1) - (4)].spec), (yyvsp[(2) - (4)].decl));
      (yyval.stmt) = FuncDef((yyvsp[(1) - (4)].spec), (yyvsp[(2) - (4)].decl), NULL, (yyvsp[(4) - (4)].stmt));
    }
    break;

  case 276:

/* Line 1455 of yacc.c  */
#line 1892 "parser.y"
    {
      if (isTypedef || (yyvsp[(1) - (1)].decl)->decl->type != DFUNC)
        parse_error(1, "function definition cannot be parsed.\n");
      if (symtab_get(stab, decl_getidentifier_symbol((yyvsp[(1) - (1)].decl)), FUNCNAME) == NULL)
        symtab_put(stab, decl_getidentifier_symbol((yyvsp[(1) - (1)].decl)), FUNCNAME);
      
      scope_start(stab);
      ast_declare_function_params((yyvsp[(1) - (1)].decl));
    }
    break;

  case 277:

/* Line 1455 of yacc.c  */
#line 1902 "parser.y"
    {
      astspec s = Declspec(SPEC_int);  /* return type defaults to "int" */

      scope_end(stab);
      check_for_main_and_declare(s, (yyvsp[(1) - (3)].decl));
      (yyval.stmt) = FuncDef(s, (yyvsp[(1) - (3)].decl), NULL, (yyvsp[(3) - (3)].stmt));
    }
    break;

  case 278:

/* Line 1455 of yacc.c  */
#line 1913 "parser.y"
    {
      if (isTypedef || (yyvsp[(2) - (2)].decl)->decl->type != DFUNC)
        parse_error(1, "function definition cannot be parsed.\n");
      if (symtab_get(stab, decl_getidentifier_symbol((yyvsp[(2) - (2)].decl)), FUNCNAME) == NULL)
        symtab_put(stab, decl_getidentifier_symbol((yyvsp[(2) - (2)].decl)), FUNCNAME);
      
      scope_start(stab);
      /* Notice here that the function parameters are declared through
       * the declaration_list and we need to do nothing else!
       */
    }
    break;

  case 279:

/* Line 1455 of yacc.c  */
#line 1925 "parser.y"
    {
      scope_end(stab);
      check_for_main_and_declare((yyvsp[(1) - (5)].spec), (yyvsp[(2) - (5)].decl));
      (yyval.stmt) = FuncDef((yyvsp[(1) - (5)].spec), (yyvsp[(2) - (5)].decl), (yyvsp[(4) - (5)].stmt), (yyvsp[(5) - (5)].stmt));
    }
    break;

  case 280:

/* Line 1455 of yacc.c  */
#line 1931 "parser.y"
    {
      if (isTypedef || (yyvsp[(1) - (1)].decl)->decl->type != DFUNC)
        parse_error(1, "function definition cannot be parsed.\n");
      if (symtab_get(stab, decl_getidentifier_symbol((yyvsp[(1) - (1)].decl)), FUNCNAME) == NULL)
        symtab_put(stab, decl_getidentifier_symbol((yyvsp[(1) - (1)].decl)), FUNCNAME);

      scope_start(stab);
      /* Notice here that the function parameters are declared through
       * the declaration_list and we need to do nothing else!
       */
    }
    break;

  case 281:

/* Line 1455 of yacc.c  */
#line 1943 "parser.y"
    {
      astspec s = Declspec(SPEC_int);  /* return type defaults to "int" */

      scope_end(stab);
      check_for_main_and_declare(s, (yyvsp[(1) - (4)].decl));
      (yyval.stmt) = FuncDef(s, (yyvsp[(1) - (4)].decl), (yyvsp[(3) - (4)].stmt), (yyvsp[(4) - (4)].stmt));
    }
    break;

  case 282:

/* Line 1455 of yacc.c  */
#line 1955 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 283:

/* Line 1455 of yacc.c  */
#line 1959 "parser.y"
    {
      (yyval.stmt) = BlockList((yyvsp[(1) - (2)].stmt), (yyvsp[(2) - (2)].stmt));         /* Same as block list */
    }
    break;

  case 284:

/* Line 1455 of yacc.c  */
#line 1972 "parser.y"
    {
      (yyval.ocon) = (yyvsp[(1) - (1)].ocon);
    }
    break;

  case 285:

/* Line 1455 of yacc.c  */
#line 1976 "parser.y"
    {
      (yyval.ocon) = (yyvsp[(1) - (1)].ocon);
    }
    break;

  case 286:

/* Line 1455 of yacc.c  */
#line 1980 "parser.y"
    {
      (yyval.ocon) = (yyvsp[(1) - (1)].ocon);
    }
    break;

  case 287:

/* Line 1455 of yacc.c  */
#line 1984 "parser.y"
    {
      (yyval.ocon) = (yyvsp[(1) - (1)].ocon);
    }
    break;

  case 288:

/* Line 1455 of yacc.c  */
#line 1988 "parser.y"
    {
      (yyval.ocon) = (yyvsp[(1) - (1)].ocon);
    }
    break;

  case 289:

/* Line 1455 of yacc.c  */
#line 1992 "parser.y"
    {
      (yyval.ocon) = (yyvsp[(1) - (1)].ocon);
    }
    break;

  case 290:

/* Line 1455 of yacc.c  */
#line 1996 "parser.y"
    {
      (yyval.ocon) = (yyvsp[(1) - (1)].ocon);
    }
    break;

  case 291:

/* Line 1455 of yacc.c  */
#line 2000 "parser.y"
    {
      (yyval.ocon) = (yyvsp[(1) - (1)].ocon);
    }
    break;

  case 292:

/* Line 1455 of yacc.c  */
#line 2004 "parser.y"
    {
      (yyval.ocon) = (yyvsp[(1) - (1)].ocon);
    }
    break;

  case 293:

/* Line 1455 of yacc.c  */
#line 2008 "parser.y"
    {
      (yyval.ocon) = (yyvsp[(1) - (1)].ocon);
    }
    break;

  case 294:

/* Line 1455 of yacc.c  */
#line 2013 "parser.y"
    {
      (yyval.ocon) = (yyvsp[(1) - (1)].ocon);
    }
    break;

  case 295:

/* Line 1455 of yacc.c  */
#line 2029 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCBARRIER, (yyvsp[(1) - (1)].odir), NULL);
    }
    break;

  case 296:

/* Line 1455 of yacc.c  */
#line 2033 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCFLUSH, (yyvsp[(1) - (1)].odir), NULL);
    }
    break;

  case 297:

/* Line 1455 of yacc.c  */
#line 2038 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCTASKWAIT, (yyvsp[(1) - (1)].odir), NULL);
    }
    break;

  case 298:

/* Line 1455 of yacc.c  */
#line 2043 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCTASKYIELD, (yyvsp[(1) - (1)].odir), NULL);
    }
    break;

  case 299:

/* Line 1455 of yacc.c  */
#line 2050 "parser.y"
    {
      (yyval.stmt) = (yyvsp[(1) - (1)].stmt);
    }
    break;

  case 300:

/* Line 1455 of yacc.c  */
#line 2057 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCPARALLEL, (yyvsp[(1) - (2)].odir), (yyvsp[(2) - (2)].stmt));
      (yyval.ocon)->l = (yyvsp[(1) - (2)].odir)->l;
    }
    break;

  case 301:

/* Line 1455 of yacc.c  */
#line 2065 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCPARALLEL, (yyvsp[(3) - (4)].ocla));
    }
    break;

  case 302:

/* Line 1455 of yacc.c  */
#line 2072 "parser.y"
    {
      (yyval.ocla) = NULL;
    }
    break;

  case 303:

/* Line 1455 of yacc.c  */
#line 2076 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (2)].ocla), (yyvsp[(2) - (2)].ocla));
    }
    break;

  case 304:

/* Line 1455 of yacc.c  */
#line 2080 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (3)].ocla), (yyvsp[(3) - (3)].ocla));
    }
    break;

  case 305:

/* Line 1455 of yacc.c  */
#line 2087 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 306:

/* Line 1455 of yacc.c  */
#line 2091 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 307:

/* Line 1455 of yacc.c  */
#line 2097 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 308:

/* Line 1455 of yacc.c  */
#line 2098 "parser.y"
    {
      sc_start_openmp();
      (yyval.ocla) = IfClause((yyvsp[(4) - (5)].expr));
    }
    break;

  case 309:

/* Line 1455 of yacc.c  */
#line 2102 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 310:

/* Line 1455 of yacc.c  */
#line 2103 "parser.y"
    {
      sc_start_openmp();
      (yyval.ocla) = NumthreadsClause((yyvsp[(4) - (5)].expr));
    }
    break;

  case 311:

/* Line 1455 of yacc.c  */
#line 2111 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCFOR, (yyvsp[(1) - (2)].odir), (yyvsp[(2) - (2)].stmt));
    }
    break;

  case 312:

/* Line 1455 of yacc.c  */
#line 2118 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCFOR, (yyvsp[(3) - (4)].ocla));
    }
    break;

  case 313:

/* Line 1455 of yacc.c  */
#line 2125 "parser.y"
    {
      (yyval.ocla) = NULL;
    }
    break;

  case 314:

/* Line 1455 of yacc.c  */
#line 2129 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (2)].ocla), (yyvsp[(2) - (2)].ocla));
    }
    break;

  case 315:

/* Line 1455 of yacc.c  */
#line 2133 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (3)].ocla), (yyvsp[(3) - (3)].ocla));
    }
    break;

  case 316:

/* Line 1455 of yacc.c  */
#line 2140 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 317:

/* Line 1455 of yacc.c  */
#line 2144 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 318:

/* Line 1455 of yacc.c  */
#line 2148 "parser.y"
    {
      (yyval.ocla) = PlainClause(OCNOWAIT);
    }
    break;

  case 319:

/* Line 1455 of yacc.c  */
#line 2155 "parser.y"
    {
      (yyval.ocla) = PlainClause(OCORDERED);
    }
    break;

  case 320:

/* Line 1455 of yacc.c  */
#line 2159 "parser.y"
    {
      (yyval.ocla) = ScheduleClause((yyvsp[(3) - (4)].type), NULL);
    }
    break;

  case 321:

/* Line 1455 of yacc.c  */
#line 2162 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 322:

/* Line 1455 of yacc.c  */
#line 2163 "parser.y"
    {
      sc_start_openmp();
      if ((yyvsp[(3) - (7)].type) == OC_runtime)
        parse_error(1, "\"runtime\" schedules may not have a chunksize.\n");
      (yyval.ocla) = ScheduleClause((yyvsp[(3) - (7)].type), (yyvsp[(6) - (7)].expr));
    }
    break;

  case 323:

/* Line 1455 of yacc.c  */
#line 2170 "parser.y"
    {  /* non-OpenMP schedule */
      tempsave = checkDecls;
      checkDecls = 0;   /* Because the index of the loop is usualy involved */
      sc_pause_openmp();
    }
    break;

  case 324:

/* Line 1455 of yacc.c  */
#line 2176 "parser.y"
    {
      sc_start_openmp();
      checkDecls = tempsave;
      (yyval.ocla) = ScheduleClause(OC_affinity, (yyvsp[(6) - (7)].expr));
    }
    break;

  case 325:

/* Line 1455 of yacc.c  */
#line 2182 "parser.y"
    {
      int n = 0, er = 0;
      if (xar_expr_is_constant((yyvsp[(3) - (4)].expr)))
      {
        n = xar_calc_int_expr((yyvsp[(3) - (4)].expr), &er);
        if (er) n = 0;
      }
      if (n <= 0) 
        parse_error(1, "invalid number in collapse() clause.\n");
      (yyval.ocla) = CollapseClause(n);
    }
    break;

  case 326:

/* Line 1455 of yacc.c  */
#line 2197 "parser.y"
    {
      (yyval.type) = OC_static;
    }
    break;

  case 327:

/* Line 1455 of yacc.c  */
#line 2201 "parser.y"
    {
      (yyval.type) = OC_dynamic;
    }
    break;

  case 328:

/* Line 1455 of yacc.c  */
#line 2205 "parser.y"
    {
      (yyval.type) = OC_guided;
    }
    break;

  case 329:

/* Line 1455 of yacc.c  */
#line 2209 "parser.y"
    {
      (yyval.type) = OC_runtime;
    }
    break;

  case 330:

/* Line 1455 of yacc.c  */
#line 2213 "parser.y"
    {
      (yyval.type) = OC_auto;
    }
    break;

  case 331:

/* Line 1455 of yacc.c  */
#line 2216 "parser.y"
    { parse_error(1, "invalid openmp schedule type.\n"); }
    break;

  case 332:

/* Line 1455 of yacc.c  */
#line 2222 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCSECTIONS, (yyvsp[(1) - (2)].odir), (yyvsp[(2) - (2)].stmt));
    }
    break;

  case 333:

/* Line 1455 of yacc.c  */
#line 2229 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCSECTIONS, (yyvsp[(3) - (4)].ocla));
    }
    break;

  case 334:

/* Line 1455 of yacc.c  */
#line 2236 "parser.y"
    {
      (yyval.ocla) = NULL;
    }
    break;

  case 335:

/* Line 1455 of yacc.c  */
#line 2240 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (2)].ocla), (yyvsp[(2) - (2)].ocla));
    }
    break;

  case 336:

/* Line 1455 of yacc.c  */
#line 2244 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (3)].ocla), (yyvsp[(3) - (3)].ocla));
    }
    break;

  case 337:

/* Line 1455 of yacc.c  */
#line 2251 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 338:

/* Line 1455 of yacc.c  */
#line 2255 "parser.y"
    {
      (yyval.ocla) = PlainClause(OCNOWAIT);
    }
    break;

  case 339:

/* Line 1455 of yacc.c  */
#line 2262 "parser.y"
    {
      (yyval.stmt) = Compound((yyvsp[(2) - (3)].stmt));
    }
    break;

  case 340:

/* Line 1455 of yacc.c  */
#line 2269 "parser.y"
    {
      /* Make it look like it had a section pragma */
      (yyval.stmt) = OmpStmt( OmpConstruct(DCSECTION, OmpDirective(DCSECTION,NULL), (yyvsp[(1) - (1)].stmt)) );
    }
    break;

  case 341:

/* Line 1455 of yacc.c  */
#line 2274 "parser.y"
    {
      (yyval.stmt) = OmpStmt( OmpConstruct(DCSECTION, (yyvsp[(1) - (2)].odir), (yyvsp[(2) - (2)].stmt)) );
    }
    break;

  case 342:

/* Line 1455 of yacc.c  */
#line 2278 "parser.y"
    {
      (yyval.stmt) = BlockList((yyvsp[(1) - (3)].stmt), OmpStmt( OmpConstruct(DCSECTION, (yyvsp[(2) - (3)].odir), (yyvsp[(3) - (3)].stmt)) ));
    }
    break;

  case 343:

/* Line 1455 of yacc.c  */
#line 2285 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCSECTION, NULL);
    }
    break;

  case 344:

/* Line 1455 of yacc.c  */
#line 2292 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCSINGLE, (yyvsp[(1) - (2)].odir), (yyvsp[(2) - (2)].stmt));
    }
    break;

  case 345:

/* Line 1455 of yacc.c  */
#line 2299 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCSINGLE, (yyvsp[(3) - (4)].ocla));
    }
    break;

  case 346:

/* Line 1455 of yacc.c  */
#line 2306 "parser.y"
    {
      (yyval.ocla) = NULL;
    }
    break;

  case 347:

/* Line 1455 of yacc.c  */
#line 2310 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (2)].ocla), (yyvsp[(2) - (2)].ocla));
    }
    break;

  case 348:

/* Line 1455 of yacc.c  */
#line 2314 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (3)].ocla), (yyvsp[(3) - (3)].ocla));
    }
    break;

  case 349:

/* Line 1455 of yacc.c  */
#line 2321 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 350:

/* Line 1455 of yacc.c  */
#line 2325 "parser.y"
    {
      (yyval.ocla) = PlainClause(OCNOWAIT);
    }
    break;

  case 351:

/* Line 1455 of yacc.c  */
#line 2332 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCPARFOR, (yyvsp[(1) - (2)].odir), (yyvsp[(2) - (2)].stmt));
    }
    break;

  case 352:

/* Line 1455 of yacc.c  */
#line 2339 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCPARFOR, (yyvsp[(4) - (5)].ocla));
    }
    break;

  case 353:

/* Line 1455 of yacc.c  */
#line 2346 "parser.y"
    {
      (yyval.ocla) = NULL;
    }
    break;

  case 354:

/* Line 1455 of yacc.c  */
#line 2350 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (2)].ocla), (yyvsp[(2) - (2)].ocla));
    }
    break;

  case 355:

/* Line 1455 of yacc.c  */
#line 2354 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (3)].ocla), (yyvsp[(3) - (3)].ocla));
    }
    break;

  case 356:

/* Line 1455 of yacc.c  */
#line 2361 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 357:

/* Line 1455 of yacc.c  */
#line 2365 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 358:

/* Line 1455 of yacc.c  */
#line 2369 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 359:

/* Line 1455 of yacc.c  */
#line 2376 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCPARSECTIONS, (yyvsp[(1) - (2)].odir), (yyvsp[(2) - (2)].stmt));
    }
    break;

  case 360:

/* Line 1455 of yacc.c  */
#line 2383 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCPARSECTIONS, (yyvsp[(4) - (5)].ocla));
    }
    break;

  case 361:

/* Line 1455 of yacc.c  */
#line 2390 "parser.y"
    {
      (yyval.ocla) = NULL;
    }
    break;

  case 362:

/* Line 1455 of yacc.c  */
#line 2394 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (2)].ocla), (yyvsp[(2) - (2)].ocla));
    }
    break;

  case 363:

/* Line 1455 of yacc.c  */
#line 2398 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (3)].ocla), (yyvsp[(3) - (3)].ocla));
    }
    break;

  case 364:

/* Line 1455 of yacc.c  */
#line 2405 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 365:

/* Line 1455 of yacc.c  */
#line 2409 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 366:

/* Line 1455 of yacc.c  */
#line 2417 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCTASK, (yyvsp[(1) - (2)].odir), (yyvsp[(2) - (2)].stmt));
      (yyval.ocon)->l = (yyvsp[(1) - (2)].odir)->l;
    }
    break;

  case 367:

/* Line 1455 of yacc.c  */
#line 2426 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCTASK, (yyvsp[(3) - (4)].ocla));
    }
    break;

  case 368:

/* Line 1455 of yacc.c  */
#line 2434 "parser.y"
    {
      (yyval.ocla) = NULL;
    }
    break;

  case 369:

/* Line 1455 of yacc.c  */
#line 2438 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (2)].ocla), (yyvsp[(2) - (2)].ocla));
    }
    break;

  case 370:

/* Line 1455 of yacc.c  */
#line 2442 "parser.y"
    {
      (yyval.ocla) = OmpClauseList((yyvsp[(1) - (3)].ocla), (yyvsp[(3) - (3)].ocla));
    }
    break;

  case 371:

/* Line 1455 of yacc.c  */
#line 2450 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 372:

/* Line 1455 of yacc.c  */
#line 2454 "parser.y"
    {
      (yyval.ocla) = (yyvsp[(1) - (1)].ocla);
    }
    break;

  case 373:

/* Line 1455 of yacc.c  */
#line 2461 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 374:

/* Line 1455 of yacc.c  */
#line 2462 "parser.y"
    {
      sc_start_openmp();
      (yyval.ocla) = IfClause((yyvsp[(4) - (5)].expr));
    }
    break;

  case 375:

/* Line 1455 of yacc.c  */
#line 2467 "parser.y"
    {
      (yyval.ocla) = PlainClause(OCUNTIED);
    }
    break;

  case 376:

/* Line 1455 of yacc.c  */
#line 2470 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 377:

/* Line 1455 of yacc.c  */
#line 2471 "parser.y"
    {
      sc_start_openmp();
      (yyval.ocla) = FinalClause((yyvsp[(4) - (5)].expr));
    }
    break;

  case 378:

/* Line 1455 of yacc.c  */
#line 2476 "parser.y"
    {
      (yyval.ocla) = PlainClause(OCMERGEABLE);
    }
    break;

  case 379:

/* Line 1455 of yacc.c  */
#line 2482 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCMASTER, (yyvsp[(1) - (2)].odir), (yyvsp[(2) - (2)].stmt));
    }
    break;

  case 380:

/* Line 1455 of yacc.c  */
#line 2489 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCMASTER, NULL);
    }
    break;

  case 381:

/* Line 1455 of yacc.c  */
#line 2496 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCCRITICAL, (yyvsp[(1) - (2)].odir), (yyvsp[(2) - (2)].stmt));
    }
    break;

  case 382:

/* Line 1455 of yacc.c  */
#line 2503 "parser.y"
    {
      (yyval.odir) = OmpCriticalDirective(NULL);
    }
    break;

  case 383:

/* Line 1455 of yacc.c  */
#line 2507 "parser.y"
    {
      (yyval.odir) = OmpCriticalDirective((yyvsp[(3) - (4)].symb));
    }
    break;

  case 384:

/* Line 1455 of yacc.c  */
#line 2514 "parser.y"
    {
      (yyval.symb) = Symbol((yyvsp[(2) - (3)].name));
    }
    break;

  case 385:

/* Line 1455 of yacc.c  */
#line 2522 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCTASKWAIT, NULL);
    }
    break;

  case 386:

/* Line 1455 of yacc.c  */
#line 2530 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCTASKYIELD, NULL);
    }
    break;

  case 387:

/* Line 1455 of yacc.c  */
#line 2537 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCBARRIER, NULL);
    }
    break;

  case 388:

/* Line 1455 of yacc.c  */
#line 2544 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCATOMIC, (yyvsp[(1) - (2)].odir), (yyvsp[(2) - (2)].stmt));
    }
    break;

  case 389:

/* Line 1455 of yacc.c  */
#line 2551 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCATOMIC, NULL);
    }
    break;

  case 390:

/* Line 1455 of yacc.c  */
#line 2555 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCATOMIC, NULL);
    }
    break;

  case 391:

/* Line 1455 of yacc.c  */
#line 2559 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCATOMIC, NULL);
    }
    break;

  case 392:

/* Line 1455 of yacc.c  */
#line 2563 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCATOMIC, NULL);
    }
    break;

  case 393:

/* Line 1455 of yacc.c  */
#line 2570 "parser.y"
    {
      (yyval.odir) = OmpFlushDirective(NULL);
    }
    break;

  case 394:

/* Line 1455 of yacc.c  */
#line 2574 "parser.y"
    {
      (yyval.odir) = OmpFlushDirective((yyvsp[(3) - (4)].decl));
    }
    break;

  case 395:

/* Line 1455 of yacc.c  */
#line 2580 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 396:

/* Line 1455 of yacc.c  */
#line 2581 "parser.y"
    {
      sc_start_openmp();
      (yyval.decl) = (yyvsp[(3) - (4)].decl);
    }
    break;

  case 397:

/* Line 1455 of yacc.c  */
#line 2589 "parser.y"
    {
      (yyval.ocon) = OmpConstruct(DCORDERED, (yyvsp[(1) - (2)].odir), (yyvsp[(2) - (2)].stmt));
    }
    break;

  case 398:

/* Line 1455 of yacc.c  */
#line 2596 "parser.y"
    {
      (yyval.odir) = OmpDirective(DCORDERED, NULL);
    }
    break;

  case 399:

/* Line 1455 of yacc.c  */
#line 2603 "parser.y"
    {
      (yyval.odir) = OmpThreadprivateDirective((yyvsp[(3) - (5)].decl));
    }
    break;

  case 400:

/* Line 1455 of yacc.c  */
#line 2609 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 401:

/* Line 1455 of yacc.c  */
#line 2610 "parser.y"
    {
      sc_start_openmp();
      (yyval.ocla) = VarlistClause(OCPRIVATE, (yyvsp[(4) - (5)].decl));
    }
    break;

  case 402:

/* Line 1455 of yacc.c  */
#line 2615 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 403:

/* Line 1455 of yacc.c  */
#line 2616 "parser.y"
    {
      sc_start_openmp();
      (yyval.ocla) = VarlistClause(OCCOPYPRIVATE, (yyvsp[(4) - (5)].decl));
    }
    break;

  case 404:

/* Line 1455 of yacc.c  */
#line 2620 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 405:

/* Line 1455 of yacc.c  */
#line 2621 "parser.y"
    {
      sc_start_openmp();
      (yyval.ocla) = VarlistClause(OCFIRSTPRIVATE, (yyvsp[(4) - (5)].decl));
    }
    break;

  case 406:

/* Line 1455 of yacc.c  */
#line 2625 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 407:

/* Line 1455 of yacc.c  */
#line 2626 "parser.y"
    {
      sc_start_openmp();
      (yyval.ocla) = VarlistClause(OCLASTPRIVATE, (yyvsp[(4) - (5)].decl));
    }
    break;

  case 408:

/* Line 1455 of yacc.c  */
#line 2630 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 409:

/* Line 1455 of yacc.c  */
#line 2631 "parser.y"
    {
      sc_start_openmp();
      (yyval.ocla) = VarlistClause(OCSHARED, (yyvsp[(4) - (5)].decl));
    }
    break;

  case 410:

/* Line 1455 of yacc.c  */
#line 2636 "parser.y"
    {
      (yyval.ocla) = DefaultClause(OC_defshared);
    }
    break;

  case 411:

/* Line 1455 of yacc.c  */
#line 2640 "parser.y"
    {
      (yyval.ocla) = DefaultClause(OC_defnone);
    }
    break;

  case 412:

/* Line 1455 of yacc.c  */
#line 2643 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 413:

/* Line 1455 of yacc.c  */
#line 2644 "parser.y"
    {
      sc_start_openmp();
      (yyval.ocla) = ReductionClause((yyvsp[(3) - (7)].type), (yyvsp[(6) - (7)].decl));
    }
    break;

  case 414:

/* Line 1455 of yacc.c  */
#line 2648 "parser.y"
    { sc_pause_openmp(); }
    break;

  case 415:

/* Line 1455 of yacc.c  */
#line 2649 "parser.y"
    {
      sc_start_openmp();
      (yyval.ocla) = VarlistClause(OCCOPYIN, (yyvsp[(4) - (5)].decl));
    }
    break;

  case 416:

/* Line 1455 of yacc.c  */
#line 2657 "parser.y"
    {
      (yyval.type) = OC_plus;
    }
    break;

  case 417:

/* Line 1455 of yacc.c  */
#line 2661 "parser.y"
    {
      (yyval.type) = OC_times;
    }
    break;

  case 418:

/* Line 1455 of yacc.c  */
#line 2665 "parser.y"
    {
      (yyval.type) = OC_minus;
    }
    break;

  case 419:

/* Line 1455 of yacc.c  */
#line 2669 "parser.y"
    {
      (yyval.type) = OC_band;
    }
    break;

  case 420:

/* Line 1455 of yacc.c  */
#line 2673 "parser.y"
    {
      (yyval.type) = OC_xor;
    }
    break;

  case 421:

/* Line 1455 of yacc.c  */
#line 2677 "parser.y"
    {
      (yyval.type) = OC_bor;
    }
    break;

  case 422:

/* Line 1455 of yacc.c  */
#line 2681 "parser.y"
    {
      (yyval.type) = OC_land;
    }
    break;

  case 423:

/* Line 1455 of yacc.c  */
#line 2685 "parser.y"
    {
      (yyval.type) = OC_lor;
    }
    break;

  case 424:

/* Line 1455 of yacc.c  */
#line 2689 "parser.y"
    {
      (yyval.type) = OC_min;
    }
    break;

  case 425:

/* Line 1455 of yacc.c  */
#line 2693 "parser.y"
    {
      (yyval.type) = OC_max;
    }
    break;

  case 426:

/* Line 1455 of yacc.c  */
#line 2700 "parser.y"
    {
      if (checkDecls) 
        if (symtab_get(stab, Symbol((yyvsp[(1) - (1)].name)), IDNAME) == NULL)
          parse_error(-1, "unknown identifier `%s'.\n", (yyvsp[(1) - (1)].name));
      (yyval.decl) = IdentifierDecl( Symbol((yyvsp[(1) - (1)].name)) );
    }
    break;

  case 427:

/* Line 1455 of yacc.c  */
#line 2707 "parser.y"
    {
      if (checkDecls) 
        if (symtab_get(stab, Symbol((yyvsp[(3) - (3)].name)), IDNAME) == NULL)
          parse_error(-1, "unknown identifier `%s'.\n", (yyvsp[(3) - (3)].name));
      (yyval.decl) = IdList((yyvsp[(1) - (3)].decl), IdentifierDecl( Symbol((yyvsp[(3) - (3)].name)) ));
    }
    break;

  case 428:

/* Line 1455 of yacc.c  */
#line 2722 "parser.y"
    {
      if (checkDecls)
      {
        stentry e = symtab_get(stab, Symbol((yyvsp[(1) - (1)].name)), IDNAME);
        if (e == NULL)
          parse_error(-1, "unknown identifier `%s'.\n", (yyvsp[(1) - (1)].name));
        if (e->scopelevel != stab->scopelevel)
          parse_error(-1, "threadprivate directive appears at different "
                          "scope level\nfrom the one `%s' was declared.\n", (yyvsp[(1) - (1)].name));
        if (stab->scopelevel > 0)    /* Don't care for globals */
          if (speclist_getspec(e->spec, STCLASSSPEC, SPEC_static) == NULL)
            parse_error(-1, "threadprivate variable `%s' does not have static "
                            "storage type.\n", (yyvsp[(1) - (1)].name));
        e->isthrpriv = 1;   /* Mark */
      }
      (yyval.decl) = IdentifierDecl( Symbol((yyvsp[(1) - (1)].name)) );
    }
    break;

  case 429:

/* Line 1455 of yacc.c  */
#line 2740 "parser.y"
    {
      if (checkDecls) 
      {
        stentry e = symtab_get(stab, Symbol((yyvsp[(3) - (3)].name)), IDNAME);
        if (e == NULL)
          parse_error(-1, "unknown identifier `%s'.\n", (yyvsp[(3) - (3)].name));
        if (e->scopelevel != stab->scopelevel)
          parse_error(-1, "threadprivate directive appears at different "
                          "scope level\nfrom the one `%s' was declared.\n", (yyvsp[(3) - (3)].name));
        if (stab->scopelevel > 0)    /* Don't care for globals */
          if (speclist_getspec(e->spec, STCLASSSPEC, SPEC_static) == NULL)
            parse_error(-1, "threadprivate variable `%s' does not have static "
                            "storage type.\n", (yyvsp[(3) - (3)].name));
        e->isthrpriv = 1;   /* Mark */
      }
      (yyval.decl) = IdList((yyvsp[(1) - (3)].decl), IdentifierDecl( Symbol((yyvsp[(3) - (3)].name)) ));
    }
    break;

  case 430:

/* Line 1455 of yacc.c  */
#line 2766 "parser.y"
    {
      (yyval.xcon) = OmpixConstruct(OX_DCTASKSYNC, (yyvsp[(1) - (1)].xdir), NULL);
    }
    break;

  case 431:

/* Line 1455 of yacc.c  */
#line 2770 "parser.y"
    {
      (yyval.xcon) = OmpixConstruct(OX_DCTASKSCHEDULE, (yyvsp[(1) - (1)].xdir), NULL);
    }
    break;

  case 432:

/* Line 1455 of yacc.c  */
#line 2778 "parser.y"
    {
      (yyval.xdir) = OmpixDirective(OX_DCTASKSYNC, NULL);
    }
    break;

  case 433:

/* Line 1455 of yacc.c  */
#line 2785 "parser.y"
    { 
      scope_start(stab); 
    }
    break;

  case 434:

/* Line 1455 of yacc.c  */
#line 2789 "parser.y"
    {
      scope_end(stab);
      (yyval.xdir) = OmpixDirective(OX_DCTASKSCHEDULE, (yyvsp[(4) - (5)].xcla));
    }
    break;

  case 435:

/* Line 1455 of yacc.c  */
#line 2797 "parser.y"
    {
      (yyval.xcla) = NULL;
    }
    break;

  case 436:

/* Line 1455 of yacc.c  */
#line 2801 "parser.y"
    {
      (yyval.xcla) = OmpixClauseList((yyvsp[(1) - (2)].xcla), (yyvsp[(2) - (2)].xcla));
    }
    break;

  case 437:

/* Line 1455 of yacc.c  */
#line 2805 "parser.y"
    {
      (yyval.xcla) = OmpixClauseList((yyvsp[(1) - (3)].xcla), (yyvsp[(3) - (3)].xcla));
    }
    break;

  case 438:

/* Line 1455 of yacc.c  */
#line 2812 "parser.y"
    {
      (yyval.xcla) = OmpixStrideClause((yyvsp[(3) - (4)].expr));
    }
    break;

  case 439:

/* Line 1455 of yacc.c  */
#line 2816 "parser.y"
    {
      (yyval.xcla) = OmpixStartClause((yyvsp[(3) - (4)].expr));
    }
    break;

  case 440:

/* Line 1455 of yacc.c  */
#line 2820 "parser.y"
    {
      (yyval.xcla) = OmpixScopeClause((yyvsp[(3) - (4)].type));
    }
    break;

  case 441:

/* Line 1455 of yacc.c  */
#line 2824 "parser.y"
    {
      (yyval.xcla) = OmpixPlainClause(OX_OCTIED);
    }
    break;

  case 442:

/* Line 1455 of yacc.c  */
#line 2828 "parser.y"
    {
      (yyval.xcla) = OmpixPlainClause(OX_OCUNTIED);
    }
    break;

  case 443:

/* Line 1455 of yacc.c  */
#line 2835 "parser.y"
    {
      (yyval.type) = OX_SCOPE_NODES;
    }
    break;

  case 444:

/* Line 1455 of yacc.c  */
#line 2839 "parser.y"
    {
      (yyval.type) = OX_SCOPE_WGLOBAL;
    }
    break;

  case 445:

/* Line 1455 of yacc.c  */
#line 2843 "parser.y"
    {
      (yyval.type) = OX_SCOPE_WGLOBAL;
    }
    break;

  case 446:

/* Line 1455 of yacc.c  */
#line 2847 "parser.y"
    {
      (yyval.type) = OX_SCOPE_WLOCAL;
    }
    break;

  case 447:

/* Line 1455 of yacc.c  */
#line 2854 "parser.y"
    {
      (yyval.xcon) = (yyvsp[(1) - (1)].xcon);
    }
    break;

  case 448:

/* Line 1455 of yacc.c  */
#line 2858 "parser.y"
    {
      (yyval.xcon) = (yyvsp[(1) - (1)].xcon);
    }
    break;

  case 449:

/* Line 1455 of yacc.c  */
#line 2866 "parser.y"
    {
      /* Should put the name of the callback function in the stab, too
      if (symtab_get(stab, decl_getidentifier_symbol($2->u.declaration.decl),
            FUNCNAME) == NULL)
        symtab_put(stab, decl_getidentifier_symbol($2->u.declaration.spec),
            FUNCNAME);
      */
      scope_start(stab);   /* re-declare the arguments of the task function */
      ast_declare_function_params((yyvsp[(2) - (2)].stmt)->u.declaration.decl);
    }
    break;

  case 450:

/* Line 1455 of yacc.c  */
#line 2877 "parser.y"
    {
      scope_end(stab);
      (yyval.xcon) = OmpixTaskdef((yyvsp[(1) - (4)].xdir), (yyvsp[(2) - (4)].stmt), (yyvsp[(4) - (4)].stmt));
      (yyval.xcon)->l = (yyvsp[(1) - (4)].xdir)->l;
    }
    break;

  case 451:

/* Line 1455 of yacc.c  */
#line 2883 "parser.y"
    {
      (yyval.xcon) = OmpixTaskdef((yyvsp[(1) - (2)].xdir), (yyvsp[(2) - (2)].stmt), NULL);
      (yyval.xcon)->l = (yyvsp[(1) - (2)].xdir)->l;
    }
    break;

  case 452:

/* Line 1455 of yacc.c  */
#line 2891 "parser.y"
    { 
      scope_start(stab); 
    }
    break;

  case 453:

/* Line 1455 of yacc.c  */
#line 2895 "parser.y"
    {
      scope_end(stab);
      (yyval.xdir) = OmpixDirective(OX_DCTASKDEF, (yyvsp[(4) - (5)].xcla));
    }
    break;

  case 454:

/* Line 1455 of yacc.c  */
#line 2903 "parser.y"
    {
      (yyval.xcla) = NULL;
    }
    break;

  case 455:

/* Line 1455 of yacc.c  */
#line 2907 "parser.y"
    {
      (yyval.xcla) = OmpixClauseList((yyvsp[(1) - (2)].xcla), (yyvsp[(2) - (2)].xcla));
    }
    break;

  case 456:

/* Line 1455 of yacc.c  */
#line 2911 "parser.y"
    {
      (yyval.xcla) = OmpixClauseList((yyvsp[(1) - (3)].xcla), (yyvsp[(3) - (3)].xcla));
    }
    break;

  case 457:

/* Line 1455 of yacc.c  */
#line 2918 "parser.y"
    {
      (yyval.xcla) = OmpixVarlistClause(OX_OCIN, (yyvsp[(3) - (4)].decl));
    }
    break;

  case 458:

/* Line 1455 of yacc.c  */
#line 2922 "parser.y"
    {
      (yyval.xcla) = OmpixVarlistClause(OX_OCOUT, (yyvsp[(3) - (4)].decl));
    }
    break;

  case 459:

/* Line 1455 of yacc.c  */
#line 2926 "parser.y"
    {
      (yyval.xcla) = OmpixVarlistClause(OX_OCINOUT, (yyvsp[(3) - (4)].decl));
    }
    break;

  case 460:

/* Line 1455 of yacc.c  */
#line 2930 "parser.y"
    {
      (yyval.xcla) = OmpixReductionClause((yyvsp[(3) - (6)].type), (yyvsp[(5) - (6)].decl));
    }
    break;

  case 461:

/* Line 1455 of yacc.c  */
#line 2937 "parser.y"
    {
      (yyval.decl) = (yyvsp[(1) - (1)].decl);
    }
    break;

  case 462:

/* Line 1455 of yacc.c  */
#line 2941 "parser.y"
    {
      (yyval.decl) = IdList((yyvsp[(1) - (3)].decl), (yyvsp[(3) - (3)].decl));
    }
    break;

  case 463:

/* Line 1455 of yacc.c  */
#line 2948 "parser.y"
    {
      (yyval.decl) = IdentifierDecl( Symbol((yyvsp[(1) - (1)].name)) );
      symtab_put(stab, Symbol((yyvsp[(1) - (1)].name)), IDNAME);
    }
    break;

  case 464:

/* Line 1455 of yacc.c  */
#line 2953 "parser.y"
    {
      if (checkDecls) check_uknown_var((yyvsp[(4) - (5)].name));
      /* Use extern to differentiate */
      (yyval.decl) = ArrayDecl(IdentifierDecl( Symbol((yyvsp[(1) - (5)].name)) ), StClassSpec(SPEC_extern), 
                     Identifier(Symbol((yyvsp[(4) - (5)].name))));
      symtab_put(stab, Symbol((yyvsp[(1) - (5)].name)), IDNAME);
    }
    break;

  case 465:

/* Line 1455 of yacc.c  */
#line 2961 "parser.y"
    {
      (yyval.decl) = ArrayDecl(IdentifierDecl( Symbol((yyvsp[(1) - (4)].name)) ), NULL, (yyvsp[(3) - (4)].expr));
      symtab_put(stab, Symbol((yyvsp[(1) - (4)].name)), IDNAME);
    }
    break;

  case 466:

/* Line 1455 of yacc.c  */
#line 2969 "parser.y"
    {
      (yyval.xcon) = OmpixConstruct(OX_DCTASK, (yyvsp[(1) - (3)].xdir), Expression((yyvsp[(2) - (3)].expr)));
      (yyval.xcon)->l = (yyvsp[(1) - (3)].xdir)->l;
    }
    break;

  case 467:

/* Line 1455 of yacc.c  */
#line 2977 "parser.y"
    {
      (yyval.xdir) = OmpixDirective(OX_DCTASK, (yyvsp[(3) - (4)].xcla));
    }
    break;

  case 468:

/* Line 1455 of yacc.c  */
#line 2984 "parser.y"
    {
      (yyval.xcla) = NULL;
    }
    break;

  case 469:

/* Line 1455 of yacc.c  */
#line 2988 "parser.y"
    {
      (yyval.xcla) = OmpixClauseList((yyvsp[(1) - (2)].xcla), (yyvsp[(2) - (2)].xcla));
    }
    break;

  case 470:

/* Line 1455 of yacc.c  */
#line 2992 "parser.y"
    {
      (yyval.xcla) = OmpixClauseList((yyvsp[(1) - (3)].xcla), (yyvsp[(3) - (3)].xcla));
    }
    break;

  case 471:

/* Line 1455 of yacc.c  */
#line 2999 "parser.y"
    {
      (yyval.xcla) = OmpixPlainClause(OX_OCATALL);
    }
    break;

  case 472:

/* Line 1455 of yacc.c  */
#line 3003 "parser.y"
    {
      (yyval.xcla) = OmpixAtnodeClause((yyvsp[(3) - (4)].expr));
    }
    break;

  case 473:

/* Line 1455 of yacc.c  */
#line 3007 "parser.y"
    {
      (yyval.xcla) = OmpixAtworkerClause((yyvsp[(3) - (4)].expr));
    }
    break;

  case 474:

/* Line 1455 of yacc.c  */
#line 3011 "parser.y"
    {
      (yyval.xcla) = OmpixPlainClause(OX_OCTIED);
    }
    break;

  case 475:

/* Line 1455 of yacc.c  */
#line 3015 "parser.y"
    {
      (yyval.xcla) = OmpixPlainClause(OX_OCUNTIED);
    }
    break;

  case 476:

/* Line 1455 of yacc.c  */
#line 3019 "parser.y"
    {
      (yyval.xcla) = OmpixPlainClause(OX_OCDETACHED);
    }
    break;

  case 477:

/* Line 1455 of yacc.c  */
#line 3026 "parser.y"
    {
      (yyval.expr) = strcmp((yyvsp[(1) - (3)].name), "main") ?
             FunctionCall(Identifier(Symbol((yyvsp[(1) - (3)].name))), NULL) :
             FunctionCall(Identifier(Symbol(MAIN_NEWNAME)), NULL);
    }
    break;

  case 478:

/* Line 1455 of yacc.c  */
#line 3032 "parser.y"
    {
      (yyval.expr) = strcmp((yyvsp[(1) - (4)].name), "main") ?
             FunctionCall(Identifier(Symbol((yyvsp[(1) - (4)].name))), (yyvsp[(3) - (4)].expr)) :
             FunctionCall(Identifier(Symbol(MAIN_NEWNAME)), (yyvsp[(3) - (4)].expr));
    }
    break;



/* Line 1455 of yacc.c  */
#line 7459 "parser.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 3040 "parser.y"



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                               *
 *     CODE                                                      *
 *                                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


void yyerror(char *s)
{
  fprintf(stderr, "(file %s, line %d, column %d):\n\t%s\n",
                  sc_original_file(), sc_original_line(), sc_column(), s);
}


/* Check whether the identifier is known or not
 */
void check_uknown_var(char *name)
{
  symbol s = Symbol(name);
  if (symtab_get(stab, s, IDNAME) == NULL &&
      symtab_get(stab, s, LABELNAME) == NULL &&
      symtab_get(stab, s, FUNCNAME) == NULL)
    parse_error(-1, "unknown identifier `%s'.\n", name);
}



/* See the "declaration" rule: if the last element of the list
 * is a user typename, we remove it, and we return it as an
 * identifier declarator.
 * The list should have 3 elements (typedef xxx type).
 */
astdecl fix_known_typename(astspec s)
{
  astspec prev;
  astdecl d;
  
  if (s->type != SPECLIST || s->u.next->type != SPECLIST) return (NULL);
  
  for (; s->u.next->type == SPECLIST; prev = s, s = s->u.next)  
    ;   /* goto last list node */
  if (s->u.next->type != USERTYPE)         /* nope */
    return (NULL);
    
  prev->u.next = s->body;
  
  d = Declarator(NULL, IdentifierDecl(s->u.next->name));
  if (checkDecls) 
    symtab_put(stab, s->u.next->name, TYPENAME);
  free(s);
  return (d);
}


void check_for_main_and_declare(astspec s, astdecl d)
{
  astdecl n = decl_getidentifier(d);

  assert(d->type == DECLARATOR);
  assert(d->decl->type == DFUNC);
  
  if (strcmp(n->u.id->name, "main") == 0)
  {
    n->u.id = Symbol(MAIN_NEWNAME);         /* Catch main()'s definition */
    hasMainfunc = 1;

    /* Now check for return type and # parameters */
    /* It d != NULL then its parameters is either (id or idlist) or
     * (paramtype or parmatypelist). If it is a list, assume the
     * standard 2 params, otherwise, we guess the single argument
     * must be the type "void" which means no params.
     * In any case, we always force main have (argc, argv[]).
     */
    if (d->decl->u.params == NULL || d->decl->u.params->type != DLIST)
      d->decl->u.params =
          ParamList(
            ParamDecl(
              Declspec(SPEC_int),
              Declarator( NULL, IdentifierDecl( Symbol("_argc_ignored") ) )
            ),
            ParamDecl(
              Declspec(SPEC_char),
              Declarator(Speclist_right( Pointer(), Pointer() ),
                         IdentifierDecl( Symbol("_argv_ignored") ))
            )
          );
   
    mainfuncRettype = 0; /* int */
    if (s != NULL)
    {
      for (; s->type == SPECLIST && s->subtype == SPEC_Rlist; s = s->u.next)
        if (s->body->type == SPEC && s->body->subtype == SPEC_void)
        {
          s = s->body;
          break;
        };
      if (s->type == SPEC && s->subtype == SPEC_void)
        mainfuncRettype = 1; /* void */
    }
  }
  if (symtab_get(stab, n->u.id, FUNCNAME) == NULL)/* From earlier declaration */
    symtab_put(stab, n->u.id, FUNCNAME);
}


/* For each variable/typename in the given declaration, add pointers in the
 * symbol table entries back to the declaration nodes.
 */
void add_declaration_links(astspec s, astdecl d)
{
  astdecl ini = NULL;
  
  if (d->type == DLIST && d->subtype == DECL_decllist)
  {
    add_declaration_links(s, d->u.next);
    d = d->decl;
  }
  if (d->type == DINIT) d = (ini = d)->decl;   /* Skip the initializer */
  assert(d->type == DECLARATOR);
  if (d->decl != NULL && d->decl->type != ABSDECLARATOR)
  {
    symbol  t = decl_getidentifier_symbol(d->decl);
    stentry e = isTypedef ?
                symtab_get(stab,t,TYPENAME) :
                symtab_get(stab,t,(decl_getkind(d)==DFUNC) ? FUNCNAME : IDNAME);
    e->spec  = s;
    e->decl  = d;
    e->idecl = ini;
  }
}


void parse_error(int exitvalue, char *format, ...)
{
  va_list ap;

  va_start(ap, format);
  fprintf(stderr, "(%s, line %d)\n\t", sc_original_file(), sc_original_line());
  vfprintf(stderr, format, ap);
  va_end(ap);
  if (strcmp(sc_original_file(), "injected_code") == 0)
    fprintf(stderr, "\n>>>>>>>\n%s\n>>>>>>>\n", parsingstring);
  _exit(exitvalue);
}


void parse_warning(char *format, ...)
{
  va_list ap;

  va_start(ap, format);
  fprintf(stderr, "[warning] ");
  vfprintf(stderr, format, ap);
  va_end(ap);
}


aststmt parse_file(char *fname, int *error)
{
  *error = 0;
  if ( (yyin = fopen(fname, "r")) == NULL )
    return (NULL);
  sc_set_filename(fname);      /* Inform the scanner */
  *error = yyparse();
  fclose(yyin);                /* No longer needed */
  return (pastree);
}


#define PARSE_STRING_SIZE 8192


astexpr parse_expression_string(char *format, ...)
{
  static char s[PARSE_STRING_SIZE];
  int    savecD;
  
  va_list ap;
  va_start(ap, format);
  vsnprintf(s, PARSE_STRING_SIZE-1, format, ap);
  va_end(ap);
  parsingstring = s;
  sc_scan_string(s);
  sc_set_start_token(START_SYMBOL_EXPRESSION);

  savecD = checkDecls;
  checkDecls = 0;         /* Don't check identifiers & don't declare them */
  yyparse();
  checkDecls = savecD;    /* Reset it */
  return ( pastree_expr );
}


aststmt parse_blocklist_string(char *format, ...)
{
  static char s[PARSE_STRING_SIZE];
  int    savecD;

  va_list ap;
  va_start(ap, format);
  vsnprintf(s, PARSE_STRING_SIZE-1, format, ap);
  va_end(ap);
  parsingstring = s;
  sc_scan_string(s);
  sc_set_start_token(START_SYMBOL_BLOCKLIST);

  savecD = checkDecls;
  checkDecls = 0;         /* Don't check identifiers & don't declare them */
  yyparse();
  checkDecls = savecD;    /* Reset it */
  return ( pastree_stmt );
}


aststmt parse_and_declare_blocklist_string(char *format, ...)
{
  static char s[PARSE_STRING_SIZE];
  int    savecD;
  
  va_list ap;
  va_start(ap, format);
  vsnprintf(s, PARSE_STRING_SIZE-1, format, ap);
  va_end(ap);
  parsingstring = s;
  sc_scan_string(s);
  sc_set_start_token(START_SYMBOL_BLOCKLIST);

  savecD = checkDecls;
  checkDecls = 1;         /* Do check identifiers & do declare them */
  yyparse();
  checkDecls = savecD;    /* Reset it */
  return ( pastree_stmt );
}


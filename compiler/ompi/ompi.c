/*
  OMPi OpenMP Compiler
  == Copyright since 2001 the OMPi Team
  == Department of Computer Science, University of Ioannina

  This file is part of OMPi.

  OMPi is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  OMPi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OMPi; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * 2009/05/10
 *   fixed '#include <string.h>' bug 
 */
 
/* ompi.c -- the starting point */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include "ompi.h"
#include "ast.h"
#include "ast_free.h"
#include "ast_xform.h"
#include "ast_show.h"
#include "x_types.h"
#include "str.h"
#include "config.h"

static aststmt ast;          /* The AST we use as our original */
symtab         stab;         /* Our symbol table */
int            testingmode;  /* For internal testing only */
char           *filename;    /* The file we parse */

/* This is for taking care of main() in the parsed code;
 * OMPi generates its own main() and replaces the original one.
 */
char *MAIN_NEWNAME   = "__original_main";  /* Rename the original main */
int  hasMainfunc     = 0;    /* 1 if main() function is defined in the file */
int  needMemcpy      = 0;    /* 1 if generated code includes memcpy()s */
int  needMalloc      = 0;    /* 1 if generated code includes malloc()s */
int  mainfuncRettype = 0;    /* 0 = int, 1 = void */
int  nonewmain       = 0;    /* If 1, we won't output a new "main()" */
int  processmode     = 0;    /* If 1, turn on process mode */
int  threadmode      = 0;    /* Will become 1 by default */
int  enableOpenMP    = 1;    /* If 0, ignore OpenMP constructs */
int  enableOmpix     = 1;    /* Enable OMPi-extensions */
int  enableCodeDup   = 1;    /* Duplicate code where appropriate for speed */
int  needLimits      = 0;    /* 1 if need limits.h constants (min/max) */
int  needFloat       = 0;    /* 1 if need float.h constants (min/max) */


void append_new_main()
{
  if (!hasMainfunc) return;
  
  A_str_truncate();
  str_printf(strA(),
         "/* OMPi-generated main() */\n"
         "int %s(int argc, char **argv)\n{\n",
         nonewmain ? "__ompi_main" : "main");

  if (mainfuncRettype == 0)
  {
    if (enableOpenMP || testingmode || processmode)
      str_printf(strA(),
           "  int _xval = 0;\n\n"
           "  ort_initialize(&argc, &argv);\n"
           "  _xval = (int) %s(argc, argv);\n"
           "  ort_finalize(_xval);\n"
           "  return (_xval);\n", MAIN_NEWNAME);
    else
      str_printf(strA(),
           "  int _xval = 0;\n\n"
           "  _xval = (int) %s(argc, argv);\n"
           "  return (_xval);\n", MAIN_NEWNAME);
  }
  else
  {
    if (enableOpenMP || testingmode || processmode)
      str_printf(strA(),
           "  ort_initialize(&argc, &argv);\n"
           "  %s(argc, argv);\n"
           "  ort_finalize(0);\n"
           "  return (0);\n", MAIN_NEWNAME);
    else
      str_printf(strA(),
           "  %s(argc, argv);\n"
           "  return (0);\n", MAIN_NEWNAME);
  }
  str_printf(strA(), "}\n");

  ast = BlockList(ast, Verbatim( strdup(A_str_string()) ));
  ast->u.next->parent = ast;   /* Parentize correctly */
  ast->body->parent = ast;
}


int getopts(int argc, char *argv[])
{
  int i;
  
  for (i = 0; i < argc; i++)
  {
    if (strcmp(argv[i], "--nomain") == 0) 
      nonewmain = 1;
    else if (strcmp(argv[i], "--procs") == 0) 
      processmode = 1;
    else if (strcmp(argv[i], "--threads") == 0) 
      threadmode = 1;
    else if (strcmp(argv[i], "--nomp") == 0)
      enableOpenMP = 0;
    else if (strcmp(argv[i], "--nox") == 0)
      enableOmpix = 0;
    else if (strcmp(argv[i], "--nocodedup") == 0)
      enableCodeDup = 0;
    else
      return (1);
  }
  return (0);
}


#include "ort.defs"

             
int main(int argc, char *argv[])
{
  time_t  now;
  char    tmp[256];
  int     r, includes_omph;
  int     knowMemcpy, knowSize_t, knowMalloc; /* flag if def'ed in user code */
  aststmt p;

  /*
   * 1. Preparations
   */
  
  if (argc < 3)
  {
    OMPI_FAILURE:
      fprintf(stderr, "** %s should not be run directly; use %scc instead\n",
                      argv[0], argv[0]);
      return (20);
  }
  if (strcmp(argv[2], "__ompi__") != 0)
  {
    if (strcmp(argv[2], "__intest__") == 0)
      testingmode = 1;
    else
      goto OMPI_FAILURE;
  }
  if (argc > 3 && getopts(argc-3, argv+3))
    goto OMPI_FAILURE;
  filename = argv[1];
  if (!processmode) threadmode = 1;  /* By default */

  stab = Symtab();                            /* Create the symbol table */

  /* Take care of GCC */
  symtab_put(stab, Symbol("__builtin_va_list"), TYPENAME);
  symtab_put(stab, Symbol("__extension__"), TYPENAME);
  
  /* This is a reserved identifier in C99; it is actually supposed to be
   * "declared" at the top of each function; we simply insert it
   * @ global scope so it is visible everywhere.
   */
  symtab_put(stab, Symbol("__func__"), IDNAME);
  
  time(&now);  /* Advertise us */
  sprintf(tmp, "/* File generated from [%s] by OMPi %s, %s */",
               filename, VERSION, ctime(&now));
  
  /*
   * 2. Parse & get the AST
   */
  
  ast = parse_file(filename, &r);
  if (r) return (r);
  if (ast == NULL)        /* Cannot open file */
  {
    fprintf(stderr, "Error opening file %s for reading!\n", filename);
    return (30);
  }

  if ((!__has_omp || !enableOpenMP) && (!__has_ompix || !enableOmpix)
       && !hasMainfunc && !testingmode && !processmode)
    return (33);          /* Leave it as is */
  
  /*
   * 3. Transform & output
   */
  
  /* The parser has left the symbol table at global scope; we must drain it */
  includes_omph = (symtab_get(stab, Symbol("omp_lock_t"), TYPENAME) != NULL);
  knowMemcpy = (symtab_get(stab, Symbol("memcpy"), FUNCNAME) != NULL);
  knowSize_t = (symtab_get(stab, Symbol("size_t"), TYPENAME) != NULL);
  knowMalloc = (symtab_get(stab, Symbol("malloc"), FUNCNAME) != NULL);
  symtab_drain(stab);
  
  if (hasMainfunc && (enableOpenMP || testingmode || processmode))
  {
    /* Need to declare the ort init/finalize functions */
    p = parse_and_declare_blocklist_string(rtlib_onoff);
    assert(p != NULL);
    ast = BlockList(verbit("# 1 \"ort.onoff.defs\""), BlockList(p, ast));
  }
    
  if ((__has_omp  && enableOpenMP) ||
      (__has_ompix && enableOmpix) || testingmode || processmode)
  {
    aststmt prepend = NULL;
    
    /* Runtime library definitions */
    if (__has_omp && enableOpenMP)
    {
      /* If <omp.h> was not included, then we must define a few things */
      if (includes_omph)
        p = NULL;
      else
      {
        p = parse_and_declare_blocklist_string(
              "typedef void *omp_nest_lock_t;"   /* The only stuff we needed */
              "typedef void *omp_lock_t; "       /* from <omp.h> */
              "typedef enum omp_sched_t { omp_sched_static = 1,"
              "omp_sched_dynamic = 2,omp_sched_guided = 3,omp_sched_auto = 4"
              " } omp_sched_t;"
              "int omp_in_parallel(void); "
              "int omp_get_thread_num(void); "
              "int omp_get_num_threads(void); "
              "int omp_in_final(void); "         /* 3.1 */
            );
        assert(p != NULL);
        p = BlockList(verbit("# 1 \"omp.mindefs\""), p);
      }
      
      /* Notice here that any types in omp.h will be defined *after* this */
      prepend = parse_and_declare_blocklist_string(rtlib_defs);
      assert(prepend != NULL);
      prepend = BlockList(verbit("# 1 \"ort.defs\""), prepend);
      if (p)
        prepend = BlockList(p, prepend);
      if (__has_affinitysched)
        prepend = BlockList(prepend, 
                            parse_and_declare_blocklist_string(
                              "int ort_affine_iteration(int *);"
                            ));
      
      ast = BlockList(prepend, ast);
    }
    
    ast = BlockList(verbit("# 1 \"%s\"", filename), ast);
    ast = BlockList(ast, verbit("\n"));    /* Dummy node @ bottom */
    ast->file = Symbol(filename);

    ast_parentize(ast);    /* Parentize */
    symtab_drain(stab);    /* Empty it; new globals will be traversed again */
    
    ast_xform(&ast);       /* The transformation phase */
    
    p = verbit(tmp);       /* Comment @ top & other stuff */
    if (needLimits)
      p = BlockList(p, verbit("#include <limits.h>"));
    if (needFloat)
      p = BlockList(p, verbit("#include <float.h>"));
    if (needMemcpy && !knowMemcpy)
      p = BlockList(
            p,
            verbit("extern void *memcpy(void*,const void*,unsigned int);")
          );
    ast = BlockList(p, ast);
/*
    ast = BlockList(
            verbit(tmp), 
            (needMemcpy && !knowMemcpy) ?
              BlockList(
                verbit("extern void *memcpy(void*,const void*,unsigned int);"), 
                ast
              ) : 
              ast
          );
*/
  }
  append_new_main();

  ast_show(ast);

  if (testingmode)
  {          /* Clean up (not needed actually; we do it only when testing)  */
    ast_free(ast);
    symtab_drain(stab);
    symbols_allfree();
    xt_free_retired();
  }
  return (0);
}


void exit_error(int exitvalue, char *format, ...)
{
  va_list ap;
  va_start(ap, format);
  vfprintf(stderr, format, ap);
  va_end(ap);
  exit(exitvalue);
}


void warning(char *format, ...)
{
  va_list ap;
  va_start(ap, format);
  vfprintf(stderr, format, ap);
  va_end(ap);
}

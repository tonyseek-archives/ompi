
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     START_SYMBOL_EXPRESSION = 258,
     START_SYMBOL_BLOCKLIST = 259,
     IDENTIFIER = 260,
     TYPE_NAME = 261,
     CONSTANT = 262,
     STRING_LITERAL = 263,
     PTR_OP = 264,
     INC_OP = 265,
     DEC_OP = 266,
     LEFT_OP = 267,
     RIGHT_OP = 268,
     LE_OP = 269,
     GE_OP = 270,
     EQ_OP = 271,
     NE_OP = 272,
     AND_OP = 273,
     OR_OP = 274,
     MUL_ASSIGN = 275,
     DIV_ASSIGN = 276,
     MOD_ASSIGN = 277,
     ADD_ASSIGN = 278,
     SUB_ASSIGN = 279,
     LEFT_ASSIGN = 280,
     RIGHT_ASSIGN = 281,
     AND_ASSIGN = 282,
     XOR_ASSIGN = 283,
     OR_ASSIGN = 284,
     SIZEOF = 285,
     TYPEDEF = 286,
     EXTERN = 287,
     STATIC = 288,
     AUTO = 289,
     REGISTER = 290,
     RESTRICT = 291,
     CHAR = 292,
     SHORT = 293,
     INT = 294,
     LONG = 295,
     SIGNED = 296,
     UNSIGNED = 297,
     FLOAT = 298,
     DOUBLE = 299,
     CONST = 300,
     VOLATILE = 301,
     VOID = 302,
     INLINE = 303,
     UBOOL = 304,
     UCOMPLEX = 305,
     UIMAGINARY = 306,
     STRUCT = 307,
     UNION = 308,
     ENUM = 309,
     ELLIPSIS = 310,
     CASE = 311,
     DEFAULT = 312,
     IF = 313,
     ELSE = 314,
     SWITCH = 315,
     WHILE = 316,
     DO = 317,
     FOR = 318,
     GOTO = 319,
     CONTINUE = 320,
     BREAK = 321,
     RETURN = 322,
     __BUILTIN_VA_ARG = 323,
     __BUILTIN_OFFSETOF = 324,
     __BUILTIN_TYPES_COMPATIBLE_P = 325,
     __ATTRIBUTE__ = 326,
     PRAGMA_OMP = 327,
     PRAGMA_OMP_THREADPRIVATE = 328,
     OMP_PARALLEL = 329,
     OMP_SECTIONS = 330,
     OMP_NOWAIT = 331,
     OMP_ORDERED = 332,
     OMP_SCHEDULE = 333,
     OMP_STATIC = 334,
     OMP_DYNAMIC = 335,
     OMP_GUIDED = 336,
     OMP_RUNTIME = 337,
     OMP_AUTO = 338,
     OMP_SECTION = 339,
     OMP_AFFINITY = 340,
     OMP_SINGLE = 341,
     OMP_MASTER = 342,
     OMP_CRITICAL = 343,
     OMP_BARRIER = 344,
     OMP_ATOMIC = 345,
     OMP_FLUSH = 346,
     OMP_PRIVATE = 347,
     OMP_FIRSTPRIVATE = 348,
     OMP_LASTPRIVATE = 349,
     OMP_SHARED = 350,
     OMP_DEFAULT = 351,
     OMP_NONE = 352,
     OMP_REDUCTION = 353,
     OMP_COPYIN = 354,
     OMP_NUMTHREADS = 355,
     OMP_COPYPRIVATE = 356,
     OMP_FOR = 357,
     OMP_IF = 358,
     OMP_TASK = 359,
     OMP_UNTIED = 360,
     OMP_TASKWAIT = 361,
     OMP_COLLAPSE = 362,
     OMP_FINAL = 363,
     OMP_MERGEABLE = 364,
     OMP_TASKYIELD = 365,
     OMP_READ = 366,
     OMP_WRITE = 367,
     OMP_CAPTURE = 368,
     OMP_UPDATE = 369,
     OMP_MIN = 370,
     OMP_MAX = 371,
     PRAGMA_OMPIX = 372,
     OMPIX_TASKDEF = 373,
     OMPIX_IN = 374,
     OMPIX_OUT = 375,
     OMPIX_INOUT = 376,
     OMPIX_TASKSYNC = 377,
     OMPIX_UPONRETURN = 378,
     OMPIX_ATNODE = 379,
     OMPIX_DETACHED = 380,
     OMPIX_ATWORKER = 381,
     OMPIX_TASKSCHEDULE = 382,
     OMPIX_STRIDE = 383,
     OMPIX_START = 384,
     OMPIX_SCOPE = 385,
     OMPIX_NODES = 386,
     OMPIX_WORKERS = 387,
     OMPIX_LOCAL = 388,
     OMPIX_GLOBAL = 389,
     OMPIX_TIED = 390
   };
#endif
/* Tokens.  */
#define START_SYMBOL_EXPRESSION 258
#define START_SYMBOL_BLOCKLIST 259
#define IDENTIFIER 260
#define TYPE_NAME 261
#define CONSTANT 262
#define STRING_LITERAL 263
#define PTR_OP 264
#define INC_OP 265
#define DEC_OP 266
#define LEFT_OP 267
#define RIGHT_OP 268
#define LE_OP 269
#define GE_OP 270
#define EQ_OP 271
#define NE_OP 272
#define AND_OP 273
#define OR_OP 274
#define MUL_ASSIGN 275
#define DIV_ASSIGN 276
#define MOD_ASSIGN 277
#define ADD_ASSIGN 278
#define SUB_ASSIGN 279
#define LEFT_ASSIGN 280
#define RIGHT_ASSIGN 281
#define AND_ASSIGN 282
#define XOR_ASSIGN 283
#define OR_ASSIGN 284
#define SIZEOF 285
#define TYPEDEF 286
#define EXTERN 287
#define STATIC 288
#define AUTO 289
#define REGISTER 290
#define RESTRICT 291
#define CHAR 292
#define SHORT 293
#define INT 294
#define LONG 295
#define SIGNED 296
#define UNSIGNED 297
#define FLOAT 298
#define DOUBLE 299
#define CONST 300
#define VOLATILE 301
#define VOID 302
#define INLINE 303
#define UBOOL 304
#define UCOMPLEX 305
#define UIMAGINARY 306
#define STRUCT 307
#define UNION 308
#define ENUM 309
#define ELLIPSIS 310
#define CASE 311
#define DEFAULT 312
#define IF 313
#define ELSE 314
#define SWITCH 315
#define WHILE 316
#define DO 317
#define FOR 318
#define GOTO 319
#define CONTINUE 320
#define BREAK 321
#define RETURN 322
#define __BUILTIN_VA_ARG 323
#define __BUILTIN_OFFSETOF 324
#define __BUILTIN_TYPES_COMPATIBLE_P 325
#define __ATTRIBUTE__ 326
#define PRAGMA_OMP 327
#define PRAGMA_OMP_THREADPRIVATE 328
#define OMP_PARALLEL 329
#define OMP_SECTIONS 330
#define OMP_NOWAIT 331
#define OMP_ORDERED 332
#define OMP_SCHEDULE 333
#define OMP_STATIC 334
#define OMP_DYNAMIC 335
#define OMP_GUIDED 336
#define OMP_RUNTIME 337
#define OMP_AUTO 338
#define OMP_SECTION 339
#define OMP_AFFINITY 340
#define OMP_SINGLE 341
#define OMP_MASTER 342
#define OMP_CRITICAL 343
#define OMP_BARRIER 344
#define OMP_ATOMIC 345
#define OMP_FLUSH 346
#define OMP_PRIVATE 347
#define OMP_FIRSTPRIVATE 348
#define OMP_LASTPRIVATE 349
#define OMP_SHARED 350
#define OMP_DEFAULT 351
#define OMP_NONE 352
#define OMP_REDUCTION 353
#define OMP_COPYIN 354
#define OMP_NUMTHREADS 355
#define OMP_COPYPRIVATE 356
#define OMP_FOR 357
#define OMP_IF 358
#define OMP_TASK 359
#define OMP_UNTIED 360
#define OMP_TASKWAIT 361
#define OMP_COLLAPSE 362
#define OMP_FINAL 363
#define OMP_MERGEABLE 364
#define OMP_TASKYIELD 365
#define OMP_READ 366
#define OMP_WRITE 367
#define OMP_CAPTURE 368
#define OMP_UPDATE 369
#define OMP_MIN 370
#define OMP_MAX 371
#define PRAGMA_OMPIX 372
#define OMPIX_TASKDEF 373
#define OMPIX_IN 374
#define OMPIX_OUT 375
#define OMPIX_INOUT 376
#define OMPIX_TASKSYNC 377
#define OMPIX_UPONRETURN 378
#define OMPIX_ATNODE 379
#define OMPIX_DETACHED 380
#define OMPIX_ATWORKER 381
#define OMPIX_TASKSCHEDULE 382
#define OMPIX_STRIDE 383
#define OMPIX_START 384
#define OMPIX_SCOPE 385
#define OMPIX_NODES 386
#define OMPIX_WORKERS 387
#define OMPIX_LOCAL 388
#define OMPIX_GLOBAL 389
#define OMPIX_TIED 390




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 1676 of yacc.c  */
#line 70 "parser.y"

  char      name[2048];  /* A general string */
  int       type;        /* A general integer */
  char     *string;      /* A dynamically allocated string (only for 2 rules) */
  symbol    symb;        /* A symbol */
  astexpr   expr;        /* An expression node in the AST */
  astspec   spec;        /* A declaration specifier node in the AST */
  astdecl   decl;        /* A declarator node in the AST */
  aststmt   stmt;        /* A statement node in the AST */
  ompcon    ocon;        /* An OpenMP construct */
  ompdir    odir;        /* An OpenMP directive */
  ompclause ocla;        /* An OpenMP clause */

  oxcon     xcon;        /* OMPi extensions */
  oxdir     xdir;
  oxclause  xcla;



/* Line 1676 of yacc.c  */
#line 342 "parser.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;



%{
/*
  OMPi OpenMP Compiler
  == Copyright since 2001 the OMPi Team
  == Department of Computer Science, University of Ioannina

  This file is part of OMPi.

  OMPi is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  OMPi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OMPi; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* parser.y */

/* 
 * 2010/11/10:
 *   dropped OpenMP-specific for parsing; fewer rules, less code
 * 2009/05/11:
 *   added AUTO schedule type
 * 2009/05/03:
 *   added ATNODE ompix clause
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <stdarg.h>
#include <ctype.h>
#include <assert.h>
#include "scanner.h"
#include "ompi.h"
#include "ast.h"
#include "symtab.h"
#include "ast_free.h"
#include "ast_copy.h"
#include "ast_vars.h"
#include "x_arith.h"

void    check_uknown_var(char *name);
void    parse_error(int exitvalue, char *format, ...);
void    parse_warning(char *format, ...);
void    yyerror(char *s);
void    check_for_main_and_declare(astspec s, astdecl d);
void    add_declaration_links(astspec s, astdecl d);
astdecl fix_known_typename(astspec s);

aststmt pastree = NULL;       /* The generated AST */
aststmt pastree_stmt = NULL;  /* For when parsing statment strings */
astexpr pastree_expr = NULL;  /* For when parsing expression strings */
int     checkDecls = 1;       /* 0 when scanning strings (no context check) */
int     tempsave;
int     isTypedef  = 0;       /* To keep track of typedefs */

char    *parsingstring;       /* For error reporting when parsing string */
%}

%union {
  char      name[2048];  /* A general string */
  int       type;        /* A general integer */
  char     *string;      /* A dynamically allocated string (only for 2 rules) */
  symbol    symb;        /* A symbol */
  astexpr   expr;        /* An expression node in the AST */
  astspec   spec;        /* A declaration specifier node in the AST */
  astdecl   decl;        /* A declarator node in the AST */
  aststmt   stmt;        /* A statement node in the AST */
  ompcon    ocon;        /* An OpenMP construct */
  ompdir    odir;        /* An OpenMP directive */
  ompclause ocla;        /* An OpenMP clause */

  oxcon     xcon;        /* OMPi extensions */
  oxdir     xdir;
  oxclause  xcla;
}

%error-verbose

/* expect 3 shift/reduce */
/* %expect 4 */

/* Start-symbol tokens (trick from bison manual) */
%token START_SYMBOL_EXPRESSION START_SYMBOL_BLOCKLIST
%type <type> start_trick

/* C tokens */
%token <name> IDENTIFIER TYPE_NAME CONSTANT STRING_LITERAL
%token <name> PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token <name> AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token <name> SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token <name> XOR_ASSIGN OR_ASSIGN SIZEOF

%token <name> TYPEDEF EXTERN STATIC AUTO REGISTER RESTRICT
%token <name> CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE
%token <name> CONST VOLATILE VOID INLINE
%token <name> UBOOL UCOMPLEX UIMAGINARY
%token <name> STRUCT UNION ENUM ELLIPSIS

%token <name> CASE DEFAULT IF ELSE SWITCH WHILE DO FOR 
%token <name> GOTO CONTINUE BREAK RETURN

/* Hacks */
%token <name> __BUILTIN_VA_ARG __BUILTIN_OFFSETOF __BUILTIN_TYPES_COMPATIBLE_P
              __ATTRIBUTE__
    
/* OpenMP tokens */
%token <name> PRAGMA_OMP PRAGMA_OMP_THREADPRIVATE OMP_PARALLEL OMP_SECTIONS
%token <name> OMP_NOWAIT OMP_ORDERED OMP_SCHEDULE OMP_STATIC OMP_DYNAMIC
%token <name> OMP_GUIDED OMP_RUNTIME OMP_AUTO OMP_SECTION OMP_AFFINITY
%token <name> OMP_SINGLE OMP_MASTER OMP_CRITICAL OMP_BARRIER OMP_ATOMIC
%token <name> OMP_FLUSH OMP_PRIVATE OMP_FIRSTPRIVATE
%token <name> OMP_LASTPRIVATE OMP_SHARED OMP_DEFAULT OMP_NONE OMP_REDUCTION
%token <name> OMP_COPYIN OMP_NUMTHREADS OMP_COPYPRIVATE OMP_FOR OMP_IF
       /* added @ OpenMP 3.0 */
%token <name> OMP_TASK OMP_UNTIED OMP_TASKWAIT OMP_COLLAPSE
       /* added @ OpenMP 3.1 */
%token <name> OMP_FINAL OMP_MERGEABLE OMP_TASKYIELD OMP_READ OMP_WRITE
%token <name> OMP_CAPTURE OMP_UPDATE OMP_MIN OMP_MAX

/* C non-terminals */
%type <symb>   enumeration_constant
%type <string> string_literal
%type <expr>   primary_expression
%type <expr>   postfix_expression
%type <expr>   argument_expression_list
%type <expr>   unary_expression
%type <type>   unary_operator
%type <expr>   cast_expression
%type <expr>   multiplicative_expression
%type <expr>   additive_expression
%type <expr>   shift_expression
%type <expr>   relational_expression
%type <expr>   equality_expression
%type <expr>   AND_expression
%type <expr>   exclusive_OR_expression
%type <expr>   inclusive_OR_expression
%type <expr>   logical_AND_expression
%type <expr>   logical_OR_expression
%type <expr>   conditional_expression
%type <expr>   assignment_expression
%type <type>   assignment_operator
%type <expr>   expression
%type <expr>   constant_expression
%type <stmt>   declaration
%type <spec>   declaration_specifiers
%type <decl>   init_declarator_list
%type <decl>   init_declarator
%type <spec>   storage_class_specifier
%type <spec>   type_specifier
%type <spec>   struct_or_union_specifier
%type <type>   struct_or_union
%type <decl>   struct_declaration_list
%type <decl>   struct_declaration
%type <spec>   specifier_qualifier_list
%type <decl>   struct_declarator_list
%type <decl>   struct_declarator
%type <spec>   enum_specifier
%type <spec>   enumerator_list
%type <spec>   enumerator
%type <spec>   type_qualifier
%type <spec>   function_specifier
%type <decl>   declarator
%type <decl>   direct_declarator
%type <spec>   pointer
%type <spec>   type_qualifier_list
%type <decl>   parameter_type_list
%type <decl>   parameter_list
%type <decl>   parameter_declaration
%type <decl>   identifier_list
%type <decl>   type_name
%type <decl>   abstract_declarator
%type <decl>   direct_abstract_declarator
%type <symb>   typedef_name
%type <expr>   initializer
%type <expr>   initializer_list
%type <expr>   designation
%type <expr>   designator_list
%type <expr>   designator
%type <stmt>   statement
%type <stmt>   labeled_statement
%type <stmt>   compound_statement
%type <stmt>   block_item_list
%type <stmt>   block_item
%type <stmt>   expression_statement
%type <stmt>   selection_statement
%type <stmt>   iteration_statement
%type <stmt>   iteration_statement_for
%type <stmt>   jump_statement
%type <stmt>   translation_unit
%type <stmt>   external_declaration
%type <stmt>   function_definition
%type <stmt>   normal_function_definition
%type <stmt>   oldstyle_function_definition
%type <stmt>   declaration_list

/* OpenMP non-terminals */
%type <ocon>   openmp_construct
%type <ocon>   openmp_directive
%type <stmt>   structured_block
%type <ocon>   parallel_construct
%type <odir>   parallel_directive
%type <ocla>   parallel_clause_optseq
%type <ocla>   parallel_clause
%type <ocla>   unique_parallel_clause
%type <ocon>   for_construct
%type <ocla>   for_clause_optseq
%type <odir>   for_directive
%type <ocla>   for_clause
%type <ocla>   unique_for_clause
%type <type>   schedule_kind
%type <ocon>   sections_construct
%type <ocla>   sections_clause_optseq
%type <odir>   sections_directive
%type <ocla>   sections_clause
%type <stmt>   section_scope
%type <stmt>   section_sequence
%type <odir>   section_directive
%type <ocon>   single_construct
%type <ocla>   single_clause_optseq
%type <odir>   single_directive
%type <ocla>   single_clause
%type <ocon>   parallel_for_construct
%type <ocla>   parallel_for_clause_optseq
%type <odir>   parallel_for_directive
%type <ocla>   parallel_for_clause
%type <ocon>   parallel_sections_construct
%type <ocla>   parallel_sections_clause_optseq
%type <odir>   parallel_sections_directive
%type <ocla>   parallel_sections_clause
%type <ocon>   master_construct
%type <odir>   master_directive
%type <ocon>   critical_construct
%type <odir>   critical_directive
%type <symb>   region_phrase
%type <odir>   barrier_directive
%type <ocon>   atomic_construct
%type <odir>   atomic_directive
%type <odir>   flush_directive
%type <decl>   flush_vars
%type <ocon>   ordered_construct
%type <odir>   ordered_directive
%type <odir>   threadprivate_directive 
%type <ocla>   data_clause
%type <type>   reduction_operator
%type <decl>   variable_list
%type <decl>   thrprv_variable_list
    /* added @ OpenMP V3.0 */
%type <ocon>   task_construct
%type <odir>   task_directive
%type <ocla>   task_clause_optseq
%type <ocla>   task_clause
%type <ocla>   unique_task_clause
%type <odir>   taskwait_directive
%type <odir>   taskyield_directive

/* 
 * OMPi-extensions
 */

/* Tokens */
%token <name> PRAGMA_OMPIX OMPIX_TASKDEF OMPIX_IN OMPIX_OUT OMPIX_INOUT
%token <name> OMPIX_TASKSYNC OMPIX_UPONRETURN OMPIX_ATNODE OMPIX_DETACHED
%token <name> OMPIX_ATWORKER OMPIX_TASKSCHEDULE OMPIX_STRIDE OMPIX_START
%token <name> OMPIX_SCOPE OMPIX_NODES OMPIX_WORKERS OMPIX_LOCAL OMPIX_GLOBAL
%token <name> OMPIX_TIED

/* Non-terminals */
%type <xcon>   ompix_construct
%type <xcon>   ompix_directive
%type <xcon>   ox_taskdef_construct
%type <xdir>   ox_taskdef_directive
%type <xcla>   ox_taskdef_clause_optseq
%type <xcla>   ox_taskdef_clause
%type <decl>   ox_variable_size_list
%type <decl>   ox_variable_size_elem
%type <xcon>   ox_task_construct
%type <xdir>   ox_task_directive
%type <xcla>   ox_task_clause_optseq
%type <xcla>   ox_task_clause
%type <expr>   ox_funccall_expression
%type <xdir>   ox_tasksync_directive
%type <xdir>   ox_taskschedule_directive
%type <xcla>   ox_taskschedule_clause_optseq
%type <xcla>   ox_taskschedule_clause
%type <type>   ox_scope_spec

%%

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                               *
 *     THE RULES                                                 *
 *                                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

start_trick:
    translation_unit                       { /* to avoid warnings */ }
  | START_SYMBOL_EXPRESSION expression     { pastree_expr = $2; }
  | START_SYMBOL_BLOCKLIST block_item_list { pastree_stmt = $2; }
;



/* -------------------------------------------------------------------------
 * ---------- ISO/IEC 9899:1999 A.1 Lexical grammar ------------------------
 * -------------------------------------------------------------------------
 */

/* -------------------------------------------------------------------------
 * ---------- ISO/IEC 9899:1999 A.1.5 Constants ----------------------------
 * -------------------------------------------------------------------------
 */

/* ISO/IEC 9899:1999 6.4.4.3 */
enumeration_constant:
    IDENTIFIER
    {
      symbol s = Symbol($1);
      if (checkDecls) 
      {
        if ( symtab_get(stab, s, LABELNAME) )  /* NOT a type name */
          parse_error(-1, "enum symbol '%s' is already in use.", $1);
        symtab_put(stab, s, LABELNAME);
      }
      $$ = s;
    }
;


/* -------------------------------------------------------------------------
 * ---------- ISO/IEC 9899:1999 A.1.6 String literals ----------------------
 * -------------------------------------------------------------------------
 */

/* ISO/IEC 9899:1999 6.4.5 */
string_literal:
    STRING_LITERAL
    {
      $$ = strdup($1);
    }
  | string_literal STRING_LITERAL
    {
      /* Or we could leave it as is (as a SpaceList) */
      if (($1 = realloc($1, strlen($1) + strlen($2))) == NULL)
        parse_error(-1, "string out of memory\n");
      strcpy(($1)+(strlen($1)-1),($2)+1);  /* Catenate on the '"' */
      $$ = $1;
    }
;


/* -------------------------------------------------------------------------
 * ------ ISO/IEC 9899:1999 A.2 Phrase structure grammar -------------------
 * -------------------------------------------------------------------------
 */

/* -------------------------------------------------------------------------
 * ------- ISO/IEC 9899:1999 A.2.1 Expressions -----------------------------
 * -------------------------------------------------------------------------
 */

/*  ISO/IEC 9899:1999 6.5.1 */
primary_expression:
    IDENTIFIER
    {
      symbol  id = Symbol($1);
      stentry e;
      int     chflag = 0;
    
      if (checkDecls)
      {
        check_uknown_var($1);
        /* The parser constructs the original AST; this is the only
         * place it doesn't (actually there is one more place, when replacing
         * the "main" function): threadprivate variables are replaced on the
         * fly by pointers. This makes the job of later stages much smoother,
         * but the produced AST is semantically incorrect.
         */
        if ((e = symtab_get(stab, id, IDNAME)) != NULL) /* could be enum name */
          if (istp(e) && threadmode)
            chflag = 1;
      }
      $$ = chflag ? UnaryOperator(UOP_paren,
                             UnaryOperator(UOP_star, Identifier(id)))
                  : Identifier(id);
    }
  | CONSTANT
    {
      $$ = Constant( strdup($1) );
    }
  | string_literal
    {
      $$ = String($1);
    }
  | '(' expression ')'
    {
      $$ = UnaryOperator(UOP_paren, $2);
    }
;

/*  ISO/IEC 9899:1999 6.5.2 */
postfix_expression:		
    primary_expression
    {
      $$ = $1;
    }
  | postfix_expression '[' expression ']'
    {
      $$ = ArrayIndex($1, $3);
    }
  /* The following 2 rules were added so that calling undeclared functions
   * does not result in "unknown identifier" messages (it was matched by
   * the IDENTIFIER rule in primary_expression.
   * They account for 2 shift/reduce conflicts.
   * (VVD)
   */
  | IDENTIFIER '(' ')' 
    {
      /* Catch calls to "main()" (unlikely but possible) */
      $$ = strcmp($1, "main") ?
             FunctionCall(Identifier(Symbol($1)), NULL) :
             FunctionCall(Identifier(Symbol(MAIN_NEWNAME)), NULL);
    }
  | IDENTIFIER '(' argument_expression_list ')'  
    {
      /* Catch calls to "main()" (unlikely but possible) */
      $$ = strcmp($1, "main") ?
             FunctionCall(Identifier(Symbol($1)), $3) :
             FunctionCall(Identifier(Symbol(MAIN_NEWNAME)), $3);
    }
  | postfix_expression '(' ')'
    {
      $$ = FunctionCall($1, NULL);
    }
  | postfix_expression '(' argument_expression_list ')'
    {
      $$ = FunctionCall($1, $3);
    }
  | postfix_expression '.' IDENTIFIER
    {
      $$ = DotField($1, Symbol($3));
    }
  | postfix_expression PTR_OP IDENTIFIER
    {
      $$ = PtrField($1, Symbol($3));
    }
    /* The next two are artificial rules, to cover the cases where
     * a struct field name is identical to one of user type names;
     * the scanner returns TYPE_NAME in this case, which would causes
     * a syntax error.
     */
  | postfix_expression '.' typedef_name
    {
      $$ = DotField($1, $3);
    }
  | postfix_expression PTR_OP typedef_name
    {
      $$ = PtrField($1, $3);
    }
  | postfix_expression INC_OP
    {
      $$ = PostOperator($1, UOP_inc);
    }
  | postfix_expression DEC_OP
    {
      $$ = PostOperator($1, UOP_dec);
    }
  | '(' type_name ')' '{' initializer_list '}'
    {
      $$ = CastedExpr($2, BracedInitializer($5));
    }
  | '(' type_name ')' '{' initializer_list ',' '}'
    {
      $$ = CastedExpr($2, BracedInitializer($5));
    }
;

/*  ISO/IEC 9899:1999 6.5.2 */
argument_expression_list:  
    assignment_expression
    {
      $$ = $1; 
    }
  | argument_expression_list ',' assignment_expression
    {
      $$ = CommaList($1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.5.3 */
unary_expression:		
    postfix_expression
    {
      $$ = $1;
    }
  | INC_OP unary_expression
    {
      $$ = PreOperator($2, UOP_inc);
    }
  | DEC_OP unary_expression
    {
      $$ = PreOperator($2, UOP_dec);
    }
  | unary_operator cast_expression
    {
      if ($1 == -1)
        $$ = $2;                    /* simplify */
      else
        $$ = UnaryOperator($1, $2);
    }
  | SIZEOF unary_expression
    {
      $$ = Sizeof($2);
    }
  | SIZEOF '(' type_name ')'
    {
      $$ = Sizeoftype($3);
    }
  /* The following are hacks to let some functions accept an
   * argument that is a type, not an expression. The "TypeTrick"
   * makes it behave like a "Sizeof", which however when the
   * tree is printed is nothing ("").
   */
  | __BUILTIN_VA_ARG '(' assignment_expression ',' type_name ')'
    {
      $$ = FunctionCall(Identifier(Symbol("__builtin_va_arg")),
                        CommaList($3, TypeTrick($5)));
    }
  | __BUILTIN_OFFSETOF '(' type_name ',' IDENTIFIER ')'
    {
      $$ = FunctionCall(Identifier(Symbol("__builtin_offsetof")),
                        CommaList(TypeTrick($3), Identifier(Symbol($5))));
    }
  | __BUILTIN_TYPES_COMPATIBLE_P '(' type_name ',' type_name ')'
    {
      $$ = FunctionCall(Identifier(Symbol("__builtin_types_compatible_p")),
                        CommaList(TypeTrick($3), TypeTrick($5)));
    }
;

/*  ISO/IEC 9899:1999 6.5.3 */
unary_operator:		
    '&'
    {
      $$ = UOP_addr;
    }
  | '*'
    {
      $$ = UOP_star;
    }
  | '+'
    {
      $$ = -1;         /* Ingore this one */
    }
  | '-'
    {
      $$ = UOP_neg;
    }
  | '~'
    {
      $$ = UOP_bnot;
    }
  | '!'
    {
      $$ = UOP_lnot;
    }
;

/*  ISO/IEC 9899:1999 6.5.4 */
cast_expression:		
    unary_expression
    {
      $$ = $1;
    }
  | '(' type_name ')' cast_expression
    {
      $$ = CastedExpr($2, $4);
    }
;

/*  ISO/IEC 9899:1999 6.5.5 */
multiplicative_expression:		
    cast_expression
    {
      $$ = $1;
    }
  | multiplicative_expression '*' cast_expression
    {
      $$ = BinaryOperator(BOP_mul, $1, $3);
    }
  | multiplicative_expression '/' cast_expression
    {
      $$ = BinaryOperator(BOP_div, $1, $3);
    }
  | multiplicative_expression '%' cast_expression
    {
      $$ = BinaryOperator(BOP_mod, $1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.5.6 */
additive_expression:		
    multiplicative_expression
    {
      $$ = $1;
    }
  | additive_expression '+' multiplicative_expression
    {
      $$ = BinaryOperator(BOP_add, $1, $3);
    }
  | additive_expression '-' multiplicative_expression
    {
      $$ = BinaryOperator(BOP_sub, $1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.5.7 */
shift_expression:		
    additive_expression
    {
      $$ = $1;
    }
  | shift_expression LEFT_OP additive_expression
    {
      $$ = BinaryOperator(BOP_shl, $1, $3);
    }
  | shift_expression RIGHT_OP additive_expression
    {
      $$ = BinaryOperator(BOP_shr, $1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.5.8 */
relational_expression:	
    shift_expression
    {
      $$ = $1;
    }
  | relational_expression '<' shift_expression
    {
      $$ = BinaryOperator(BOP_lt, $1, $3);
    }
  | relational_expression '>' shift_expression
    {
      $$ = BinaryOperator(BOP_gt, $1, $3);
    }
  | relational_expression LE_OP shift_expression
    {
      $$ = BinaryOperator(BOP_leq, $1, $3);
     }
  | relational_expression GE_OP shift_expression
    {
      $$ = BinaryOperator(BOP_geq, $1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.5.9 */
equality_expression:		
    relational_expression
    {
      $$ = $1;
    }
  | equality_expression EQ_OP relational_expression
    {
      $$ = BinaryOperator(BOP_eqeq, $1, $3);
    }
  | equality_expression NE_OP relational_expression
    {
      $$ = BinaryOperator(BOP_neq, $1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.5.10 */
AND_expression:		
    equality_expression
    {
      $$ = $1;
    }
  | AND_expression '&' equality_expression
    {
      $$ = BinaryOperator(BOP_band, $1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.5.11 */
exclusive_OR_expression:	
    AND_expression
    {
      $$ = $1;
    }
  | exclusive_OR_expression '^' AND_expression
    {
      $$ = BinaryOperator(BOP_xor, $1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.5.12 */
inclusive_OR_expression:	
    exclusive_OR_expression
    {
      $$ = $1;
    }
  | inclusive_OR_expression '|' exclusive_OR_expression
    {
      $$ = BinaryOperator(BOP_bor, $1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.5.13 */
logical_AND_expression:	
    inclusive_OR_expression
    {
      $$ = $1;
    }
  | logical_AND_expression AND_OP inclusive_OR_expression
    {
      $$ = BinaryOperator(BOP_land, $1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.5.14 */
logical_OR_expression:	
    logical_AND_expression
    {
      $$ = $1;
    }
  | logical_OR_expression OR_OP logical_AND_expression
    {
      $$ = BinaryOperator(BOP_lor, $1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.5.15 */
conditional_expression:	
    logical_OR_expression
    {
      $$ = $1;
    }
  | logical_OR_expression '?' expression ':' conditional_expression
    {
      $$ = ConditionalExpr($1, $3, $5);
    }
;

/*  ISO/IEC 9899:1999 6.5.16 */
assignment_expression:	
    conditional_expression
    {
      $$ = $1;
    }
  | unary_expression assignment_operator assignment_expression
    {
      $$ = Assignment($1, $2, $3);
    }
;

/*  ISO/IEC 9899:1999 6.5.16 */
assignment_operator:	
    '='
    {
      $$ = ASS_eq;  /* Need fix here! */
    }
  | MUL_ASSIGN
    {
      $$ = ASS_mul;
    }
  | DIV_ASSIGN
    {
      $$ = ASS_div;
    }
  | MOD_ASSIGN
    {
      $$ = ASS_mod;
    }
  | ADD_ASSIGN
    {
      $$ = ASS_add;
    }
  | SUB_ASSIGN
    {
      $$ = ASS_sub;
    }
  | LEFT_ASSIGN
    {
      $$ = ASS_shl;
    }
  | RIGHT_ASSIGN
    {
      $$ = ASS_shr;
    }
  | AND_ASSIGN
    {
      $$ = ASS_and;
    }
  | XOR_ASSIGN
    {
      $$ = ASS_xor;
    }
  | OR_ASSIGN
    {
      $$ = ASS_or;
    }
;

/*  ISO/IEC 9899:1999 6.5.17 */
expression:			
    assignment_expression
    {
      $$ = $1;
    }
  | expression ',' assignment_expression
    {
      $$ = CommaList($1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.6 */
constant_expression:
    conditional_expression
    {
      $$ = $1;
    }
;


/* -------------------------------------------------------------------------
 * ------------ ISO/IEC 9899:1999 A.2.2 Declarations -----------------------
 * -------------------------------------------------------------------------
 */

/*  ISO/IEC 9899:1999 6.7 */
declaration:		
    declaration_specifiers ';'
    {
      /* There is a special case which wrongly uses this rule:
       *   typedef xxx alread_known_user_type.
       * In this case the already_known_user_type (T) is re-defined,
       * and because T is known, it is not considered as a declarator,
       * but a "typedef_name", and is part of the specifier.
       * We fix it here.
       */
      if (isTypedef && $1->type == SPECLIST)
        $$ = Declaration($1, fix_known_typename($1));
      else
        $$ = Declaration($1, NULL);
      isTypedef = 0;
    }
  | declaration_specifiers init_declarator_list ';'
    {
      $$ = Declaration($1, $2);
      if (checkDecls) add_declaration_links($1, $2);
      isTypedef = 0;
      
    }
  | threadprivate_directive // OpenMP Version 2.5 ISO/IEC 9899:1999 addition
    {
      $$ = OmpStmt(OmpConstruct(DCTHREADPRIVATE, $1, NULL));
    }
;

/* ISO/IEC 9899:1999 6.7 */
declaration_specifiers:		 
    storage_class_specifier
    {
      $$ = $1;
    }
  | storage_class_specifier declaration_specifiers
    {
      $$ = Speclist_right($1, $2);
    }
  | type_specifier
    {
      $$ = $1;
    }
  | type_specifier declaration_specifiers
    {
      $$ = Speclist_right($1, $2);
    }
  | type_qualifier
    {
      $$ = $1;
    }
  | type_qualifier declaration_specifiers
    {
      $$ = Speclist_right($1, $2);
    }
  | function_specifier
    {
      $$ = $1;
    }
  | function_specifier declaration_specifiers
    {
      $$ = Speclist_right($1, $2);
    }
;

/*  ISO/IEC 9899:1999 6.7 */
init_declarator_list:		
    init_declarator
    {
      $$ = $1;
    }
  | init_declarator_list ',' init_declarator
    {
      $$ = DeclList($1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.7 */
/* This is the only place to come for a full declaration.
 * Other declarator calls are not of particular interest.
 * Also, note that we cannot do it in a parent rule (e.g. in 
 * "declaration" since initializers may use variables defined
 * previously, in the same declarator list.
 */
init_declarator:	
    declarator
    {
      astdecl s = decl_getidentifier($1);
      int     declkind = decl_getkind($1);
      stentry e;
      
      if (!isTypedef && declkind == DFUNC && strcmp(s->u.id->name, "main") == 0)
        s->u.id = Symbol(MAIN_NEWNAME);       /* Catch main()'s declaration */
      if (checkDecls) 
      {
        e = symtab_put(stab, s->u.id, (isTypedef) ? TYPENAME :
                                       (declkind == DFUNC) ? FUNCNAME : IDNAME);
        e->isarray = (declkind == DARRAY);
      }
      $$ = $1;
    }
  | declarator '=' 
    {
      astdecl s = decl_getidentifier($1);
      int     declkind = decl_getkind($1);
      stentry e;
      
      if (!isTypedef && declkind == DFUNC && strcmp(s->u.id->name, "main") == 0)
        s->u.id = Symbol(MAIN_NEWNAME);         /* Catch main()'s declaration */
      if (checkDecls) 
      {
        e = symtab_put(stab, s->u.id, (isTypedef) ? TYPENAME :
                                       (declkind == DFUNC) ? FUNCNAME : IDNAME);
        e->isarray = (declkind == DARRAY);
      }
    }
    initializer
    {
      $$ = InitDecl($1, $4);
    }
;

/*  ISO/IEC 9899:1999 6.7.1 */
storage_class_specifier:
    TYPEDEF
    {
      $$ = StClassSpec(SPEC_typedef);    /* Just a string */
      isTypedef = 1;
    }
  | EXTERN
    {
      $$ = StClassSpec(SPEC_extern);
    }
  | STATIC
    {
      $$ = StClassSpec(SPEC_static);
    }
  | AUTO
    {
      $$ = StClassSpec(SPEC_auto);
    }
  | REGISTER
    {
      $$ = StClassSpec(SPEC_register);
    }
;

/*  ISO/IEC 9899:1999 6.7.2 */
type_specifier:
    VOID
    {
      $$ = Declspec(SPEC_void);
    }
  | CHAR
    {
      $$ = Declspec(SPEC_char);
    }
  | SHORT
    {
      $$ = Declspec(SPEC_short);
    }
  | INT
    {
      $$ = Declspec(SPEC_int);
    }
  | LONG
    {
      $$ = Declspec(SPEC_long);
    }
  | FLOAT
    {
      $$ = Declspec(SPEC_float);
    }
  | DOUBLE
    {
      $$ = Declspec(SPEC_double);
    }
  | SIGNED
    {
      $$ = Declspec(SPEC_signed);
    }
  | UNSIGNED
    { 
      $$ = Declspec(SPEC_unsigned);
    }
  | UBOOL
    {
      $$ = Declspec(SPEC_ubool);
    }
  | UCOMPLEX
    {
      $$ = Declspec(SPEC_ucomplex);
    }
  | UIMAGINARY
    {
      $$ = Declspec(SPEC_uimaginary);
    }
  | struct_or_union_specifier
    {
      $$ = $1;
    }
  | enum_specifier
    {
      $$ = $1;
    }
  | typedef_name
    {
      $$ = Usertype($1);
    }
;

/*  ISO/IEC 9899:1999 6.7.2.1 */
struct_or_union_specifier:	
    struct_or_union '{' struct_declaration_list '}'
    {
      $$ = SUdecl($1, NULL, $3);
    }
  | struct_or_union '{' '}' /* NON-ISO empty declaration (added by SM) */
    {
      $$ = SUdecl($1, NULL, NULL);
    }
  | struct_or_union IDENTIFIER '{' struct_declaration_list '}'
    {
      symbol s = Symbol($2);
      /* Well, struct & union names have their own name space, and
       * their own scopes. I.e. they can be re-declare in nested
       * scopes. We don't do any kind of duplicate checks.
       */
      if (checkDecls) 
        symtab_put(stab, s, SUNAME);
      $$ = SUdecl($1, s, $4);
    }
  /* If we have "typedef struct X X;" then X will become a TYPE_NAME
   * from now on, altough it is also a SUNAME. Thus it won't be matched
   * by the previous rule -- this explains the following one!
   */
  | struct_or_union typedef_name '{' struct_declaration_list '}'
    {
      symbol s = $2;
      if (checkDecls) 
        symtab_put(stab, s, SUNAME);
      $$ = SUdecl($1, s, $4);
    }
  | struct_or_union IDENTIFIER
    {
      symbol s = Symbol($2);
      if (checkDecls) 
        symtab_put(stab, s, SUNAME);
      $$ = SUdecl($1, s, NULL);
    }
  | struct_or_union typedef_name       /* As above! */
    {
      symbol s = $2;
      if (checkDecls) 
        symtab_put(stab, s, SUNAME);
      $$ = SUdecl($1, s, NULL);
    }
;

/*  ISO/IEC 9899:1999 6.7.2.1 */
struct_or_union:
    STRUCT
    {
      $$ = SPEC_struct;
    }
  | UNION
    {
      $$ = SPEC_union;
    }
;

/*  ISO/IEC 9899:1999 6.7.2.1 */
struct_declaration_list:	
    struct_declaration
    {
      $$ = $1;
    }
  | struct_declaration_list struct_declaration
    {
      $$ = StructfieldList($1, $2);
    }
;

/*  ISO/IEC 9899:1999 6.7.2.1 */
struct_declaration:		
    specifier_qualifier_list struct_declarator_list ';'
    {
      $$ = StructfieldDecl($1, $2);
    }
  | specifier_qualifier_list ';'        /* added rule; just for GCC's shake */
    {
      $$ = StructfieldDecl($1, NULL);
    }
;

/*  ISO/IEC 9899:1999 6.7.2.1 */
specifier_qualifier_list:
    type_specifier
    {
      $$ = $1;
    }
  | type_specifier specifier_qualifier_list
    {
      $$ = Speclist_right($1, $2);
    }
  | type_qualifier
    {
      $$ = $1;
    }
  | type_qualifier specifier_qualifier_list
    {
      $$ = Speclist_right($1, $2);
    }
;

/*  ISO/IEC 9899:1999 6.7.2.1 */
struct_declarator_list:
    struct_declarator
    {
      $$ = $1;
    }
  | struct_declarator_list ',' struct_declarator
    {
      $$ = DeclList($1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.7.2.1 */
struct_declarator:
    declarator
    {
      $$ = $1;
    }
  | declarator ':' constant_expression
    {
      $$ = BitDecl($1, $3);
    }
  | ':' constant_expression
    {
      $$ = BitDecl(NULL, $2);
    }
;

/*  ISO/IEC 9899:1999 6.7.2.2 */
enum_specifier:			
    ENUM '{' enumerator_list '}'
    {
      $$ = Enumdecl(NULL, $3);
    }
  | ENUM IDENTIFIER '{' enumerator_list '}'
    {
      symbol s = Symbol($2);

      if (checkDecls) 
      {
        if (symtab_get(stab, s, ENUMNAME))
          parse_error(-1, "enum name '%s' is already in use.", $2);
        symtab_put(stab, s, ENUMNAME);
      }
      $$ = Enumdecl(s, $4);
    }
  | ENUM '{' enumerator_list ',' '}'
    {
      $$ = Enumdecl(NULL, $3);
    }
  | ENUM IDENTIFIER '{' enumerator_list ',' '}'
    {
      symbol s = Symbol($2);

      if (checkDecls) 
      {
        if (symtab_get(stab, s, ENUMNAME))
          parse_error(-1, "enum name '%s' is already in use.", $2);
        symtab_put(stab, s, ENUMNAME);
      }
      $$ = Enumdecl(s, $4);
    }
  | ENUM IDENTIFIER
    {
      /*
      if (symtab_get(stab, s, ENUMNAME))
        parse_error(-1, "enum name '%s' is unknown.", $2);
      */
      $$ = Enumdecl(Symbol($2), NULL);
    }
;

/*  ISO/IEC 9899:1999 6.7.2.2 */
enumerator_list:		
    enumerator
    {
      $$ = $1;
    }
  | enumerator_list ',' enumerator
    {
      $$ = Enumbodylist($1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.7.2.2 */
enumerator:			
    enumeration_constant
    {
      $$ = Enumerator($1, NULL);
    }
  |  enumeration_constant '=' constant_expression
    {
      $$ = Enumerator($1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.7.3 */
type_qualifier:
    CONST
    {
      $$ = Declspec(SPEC_const);
    }
  | RESTRICT
    {
      $$ = Declspec(SPEC_restrict);
    }
  | VOLATILE
    {
      $$ = Declspec(SPEC_volatile);
    }
;

/*  ISO/IEC 9899:1999 6.7.4 */
function_specifier:
    INLINE
    {
      $$ = Declspec(SPEC_inline);
    }
;

/*  ISO/IEC 9899:1999 6.7.5 */
declarator:			
    direct_declarator
    {
      $$ = Declarator(NULL, $1);
    }
  | pointer direct_declarator
    {
      $$ = Declarator($1, $2);
    }
/*  | error
    {
      parse_error(1, "was expecting a declarator here (%s).\n", yylval.name);
    }*/
;


/*  ISO/IEC 9899:1999 6.7.5 */
direct_declarator:
    IDENTIFIER
    {
      $$ = IdentifierDecl( Symbol($1) );
    }
  | '(' declarator ')'
    {
      /* Try to simplify a bit: (ident) -> ident */
      if ($2->spec == NULL && $2->decl->type == DIDENT)
        $$ = $2->decl;
      else
        $$ = ParenDecl($2);
    }
  | direct_declarator '[' ']'
    {
      $$ = ArrayDecl($1, NULL, NULL);
    }
  | direct_declarator '[' type_qualifier_list ']'
    {
      $$ = ArrayDecl($1, $3, NULL);
    }
  | direct_declarator '[' assignment_expression ']'
    {
      $$ = ArrayDecl($1, NULL, $3);
    }
  | direct_declarator '[' type_qualifier_list assignment_expression ']'
    {
      $$ = ArrayDecl($1, $3, $4);
    }
  | direct_declarator '[' STATIC assignment_expression ']'
    {
      $$ = ArrayDecl($1, StClassSpec(SPEC_static), $4);
    }
  | direct_declarator '[' STATIC type_qualifier_list assignment_expression ']'
    {
      $$ = ArrayDecl($1, Speclist_right( StClassSpec(SPEC_static), $4 ), $5);
    }
  | direct_declarator '[' type_qualifier_list STATIC assignment_expression ']'
    {
      $$ = ArrayDecl($1, Speclist_left( $3, StClassSpec(SPEC_static) ), $5);
    }
  | direct_declarator '[' '*' ']'
    {
      $$ = ArrayDecl($1, Declspec(SPEC_star), NULL);
    }
  | direct_declarator '[' type_qualifier_list '*' ']'
    {
      $$ = ArrayDecl($1, Speclist_left( $3, Declspec(SPEC_star) ), NULL);
    }
  | direct_declarator '(' parameter_type_list ')'
    {
      $$ = FuncDecl($1, $3);
    }
  | direct_declarator '(' ')'
    {
      $$ = FuncDecl($1, NULL);
    }
  | direct_declarator '(' identifier_list ')'
    {
      $$ = FuncDecl($1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.7.5 */
pointer:		
    '*'
    {
      $$ = Pointer();
    }
  | '*' type_qualifier_list
    {
      $$ = Speclist_right(Pointer(), $2);
    }
  | '*' pointer
    {
      $$ = Speclist_right(Pointer(), $2);
    }
  | '*' type_qualifier_list pointer
    {
      $$ = Speclist_right( Pointer(), Speclist_left($2, $3) );
    }
;

/*  ISO/IEC 9899:1999 6.7.5 */
type_qualifier_list:	
    type_qualifier
    {
      $$ = $1;
    }
  | type_qualifier_list type_qualifier
    {
      $$ = Speclist_left($1, $2);
    }
;

/*  ISO/IEC 9899:1999 6.7.5 */
parameter_type_list:	
    parameter_list
    {
      $$ = $1;
    }
  | parameter_list ',' ELLIPSIS
    {
      $$ = ParamList($1, Ellipsis());
    }
;

/*  ISO/IEC 9899:1999 6.7.5 */
parameter_list:			
    parameter_declaration
    {
      $$ = $1;
    }
  | parameter_list ',' parameter_declaration
    {
      $$ = ParamList($1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.7.5 */
parameter_declaration:		
    declaration_specifiers declarator
    {
      $$ = ParamDecl($1, $2);
    }
  | declaration_specifiers
    {
      $$ = ParamDecl($1, NULL);
    }
  | declaration_specifiers abstract_declarator
    {
      $$ = ParamDecl($1, $2);
    }
;

/*  ISO/IEC 9899:1999 6.7.5 */
identifier_list:		
    IDENTIFIER
    {
      $$ = IdentifierDecl( Symbol($1) );
    }
  | identifier_list ',' IDENTIFIER
    {
      $$ = IdList($1, IdentifierDecl( Symbol($3) ));
    }
;

/*  ISO/IEC 9899:1999 6.7.6 */
type_name:			
    specifier_qualifier_list
    {
      $$ = Casttypename($1, NULL);
    }
  | specifier_qualifier_list abstract_declarator
    {
      $$ = Casttypename($1, $2);
    }
;

/*  ISO/IEC 9899:1999 6.7.6 */
abstract_declarator:		
    pointer
    {
      $$ = AbstractDeclarator($1, NULL);
    }
  | direct_abstract_declarator
    {
      $$ = AbstractDeclarator(NULL, $1);
    }
  | pointer direct_abstract_declarator
    {
      $$ = AbstractDeclarator($1, $2);
    }
;

/*  ISO/IEC 9899:1999 6.7.6 */
direct_abstract_declarator:
    '(' abstract_declarator ')'
    {
      $$ = ParenDecl($2);
    }
  | '[' ']'
    {
      $$ = ArrayDecl(NULL, NULL, NULL);
    }
  | direct_abstract_declarator '[' ']'
    {
      $$ = ArrayDecl($1, NULL, NULL);
    }
  | '[' assignment_expression ']'
    {
      $$ = ArrayDecl(NULL, NULL, $2);
    }
  | direct_abstract_declarator '[' assignment_expression ']'
    {
      $$ = ArrayDecl($1, NULL, $3);
    }
  | '[' '*' ']'
    {
      $$ = ArrayDecl(NULL, Declspec(SPEC_star), NULL);
    }
  | direct_abstract_declarator '[' '*' ']'
    {
      $$ = ArrayDecl($1, Declspec(SPEC_star), NULL);
    }
  | '(' ')'
    {
      $$ = FuncDecl(NULL, NULL);
    }
  | direct_abstract_declarator '(' ')'
    {
      $$ = FuncDecl($1, NULL);
    }
  | '(' parameter_type_list ')'
    {
      $$ = FuncDecl(NULL, $2);
    }
  | direct_abstract_declarator '(' parameter_type_list ')'
    {
      $$ = FuncDecl($1, $3);
    }
;

/*  ISO/IEC 9899:1999 6.7.7 */
typedef_name:
    TYPE_NAME
    {
      $$ = Symbol($1);
    }
;

/*  ISO/IEC 9899:1999 6.7.8 */
initializer:			
    assignment_expression
    {
      $$ = $1;
    }
  | '{' initializer_list '}'
    {
      $$ = BracedInitializer($2);
    }
  | '{' initializer_list ',' '}'
    {
      $$ = BracedInitializer($2);
    }
;

/*  ISO/IEC 9899:1999 6.7.8 */
initializer_list:
    initializer
    {
      $$ = $1;
    }
  | designation initializer
    {
      $$ = Designated($1, $2);
    }
  | initializer_list ',' initializer
    {
      $$ = CommaList($1, $3);
    }
  | initializer_list ',' designation initializer
    {
      $$ = CommaList($1, Designated($3, $4));
    }
;

/*  ISO/IEC 9899:1999 6.7.8 */
designation:
    designator_list '='
    {
      $$ = $1; 
    }
;

/*  ISO/IEC 9899:1999 6.7.8 */
designator_list:
    designator
    {
      $$ = $1;
    }
  | designator_list designator
    {
      $$ = SpaceList($1, $2);
    }
;

/*  ISO/IEC 9899:1999 6.7.8 */
designator:
    '[' constant_expression ']'
    {
      $$ = IdxDesignator($2);
    }
  | '.' IDENTIFIER
    {
      $$ = DotDesignator( Symbol($2) );
    }
  | '.' typedef_name     /* artificial rule */
    {
      $$ = DotDesignator($2);
    }
;


/* -------------------------------------------------------------------------
 * ------------- ISO/IEC 9899:1999 A.2.3 Statements ------------------------
 * -------------------------------------------------------------------------
 */

/*  ISO/IEC 9899:1999 6.8 */
statement:
    labeled_statement
    {
      $$ = $1;
    }
  | compound_statement
    {
      $$ = $1;
    }
  | expression_statement
    {
      $$ = $1;
    }
  | selection_statement
    {
      $$ = $1;
    }
  | iteration_statement
    {
      $$ = $1;
    }
  | jump_statement
    {
      $$ = $1;
    }
  | openmp_construct // OpenMP Version 2.5 ISO/IEC 9899:1999 addition
    {
      $$ = OmpStmt($1);
      $$->l = $1->l;
    }
  | ompix_construct // OMPi extensions
    {
      $$ = OmpixStmt($1);
      $$->l = $1->l;
    }
;

/*  ISO/IEC 9899:1999 6.8.1 */
labeled_statement:
    IDENTIFIER ':' statement
    {
      $$ = Labeled( Symbol($1), $3 );
    }
  | CASE constant_expression ':' statement
    {
      $$ = Case($2, $4);
    }
  | DEFAULT ':' statement
    {
      $$ = Default($3);
    }
;

/*  ISO/IEC 9899:1999 6.8.2 */
compound_statement:
    '{' '}'
    {
      $$ = Compound(NULL);
    }
  | '{'  { $<type>$ = sc_original_line()-1; scope_start(stab); }
       block_item_list '}'
    {
      $$ = Compound($3);
      scope_end(stab);
      $$->l = $<type>2;     /* Remember 1st line */
    }
;

/*  ISO/IEC 9899:1999 6.8.2 */
block_item_list:
    block_item
    {
      $$ = $1;
    }
  | block_item_list block_item
    {
      $$ = BlockList($1, $2);
      $$->l = $1->l;
    }
;

/*  ISO/IEC 9899:1999 6.8.2 */
block_item:
    declaration
    {
      $$ = $1;
    }
  | statement
    {
      $$ = $1;
    }
  | openmp_directive // OpenMP Version 2.5 ISvO/IEC 9899:1999 addition
    {
      $$ = OmpStmt($1);
      $$->l = $1->l;
    }
  | ompix_directive // ompi extensions
    {
      $$ = OmpixStmt($1);
      $$->l = $1->l;
    }
;

/*  ISO/IEC 9899:1999 6.8.3 */
expression_statement:
    ';'
    {
      $$ = Expression(NULL);
    }
  | expression ';'
    {
      $$ = Expression($1);
      $$->l = $1->l;
    }
;

/*  ISO/IEC 9899:1999 6.8.4 */
selection_statement:
    IF '(' expression ')' statement
    {
      $$ = If($3, $5, NULL);
    }
  | IF '(' expression ')' statement ELSE statement
    {
      $$ = If($3, $5, $7);
    }
  | SWITCH '(' expression ')' statement
    {
      $$ = Switch($3, $5);
    }
;

/*  ISO/IEC 9899:1999 6.8.5 */
/* (VVD) broke off the FOR part */
iteration_statement:
    WHILE '(' expression ')' statement
    {
      $$ = While($3, $5);
    }
  | DO statement WHILE '(' expression ')' ';'
    {
      $$ = Do($2, $5);
    }
  | iteration_statement_for
;

iteration_statement_for:
    FOR '(' ';' ';' ')' statement
    {
      $$ = For(NULL, NULL, NULL, $6);
    }
  | FOR '(' expression ';' ';' ')' statement
    {
      $$ = For(Expression($3), NULL, NULL, $7);
    }
  | FOR '(' ';' expression ';' ')' statement
    {
      $$ = For(NULL, $4, NULL, $7);
    }
  | FOR '(' ';' ';' expression ')' statement
    {
      $$ = For(NULL, NULL, $5, $7);
    }
  | FOR '(' expression ';' expression ';' ')' statement
    {
      $$ = For(Expression($3), $5, NULL, $8);
    }
  | FOR '(' expression ';' ';' expression ')' statement
    {
      $$ = For(Expression($3), NULL, $6, $8);
    }
  | FOR '(' ';' expression ';' expression ')' statement
    {
      $$ = For(NULL, $4, $6, $8);
    }
  | FOR '(' expression ';' expression ';' expression ')' statement
    {
      $$ = For(Expression($3), $5, $7, $9);
    }
  | FOR '(' declaration ';' ')' statement
    {
      $$ = For($3, NULL, NULL, $6);
    }
  | FOR '(' declaration expression ';' ')' statement
    {
      $$ = For($3, $4, NULL, $7);
    }
  | FOR '(' declaration ';' expression ')' statement
    {
      $$ = For($3, NULL, $5, $7);
    }
  | FOR '(' declaration expression ';' expression ')' statement
    {
      $$ = For($3, $4, $6, $8);
    }
;

/*  ISO/IEC 9899:1999 6.8.6 */
jump_statement:
    GOTO IDENTIFIER ';'
    {
      /* We don't keep track of labels -- we leave it to the native compiler */
      $$ = Goto( Symbol($2) );
    }
  | CONTINUE ';'
    {
      $$ = Continue();
    }
  | BREAK ';'
    {
      $$ = Break();
    }
  | RETURN ';'
    {
      $$ = Return(NULL);
    }
  | RETURN expression ';'
    {
      $$ = Return($2);
    }
;


/* -------------------------------------------------------------------------
 * ------ ISO/IEC 9899:1999 A.2.4 External definitions ---------------------
 * -------------------------------------------------------------------------
 */
    
/*  ISO/IEC 9899:1999 6.9 */
translation_unit:
    external_declaration
    {
      $$ = pastree = $1;
    }
  | translation_unit external_declaration
    {
      $$ = pastree = BlockList($1, $2);
    }
;

/*  ISO/IEC 9899:1999 6.9 */
external_declaration:
    function_definition
    {
      $$ = $1;
    }
  | declaration
    {
      $$ = $1;
    }
    /* Actually, although not in the grammar, we support 1 more option
     * here:  Verbatim
     */
  | ox_taskdef_construct
    {
      $$ = OmpixStmt($1);
    }
;

/*  ISO/IEC 9899:1999 6.9.1 */
/* We open a new scope which encloses the compound statement.
 * In there we will only declare the function parameters.
 * We break the rule into two subrules, for modularity. 
 */
function_definition:
    normal_function_definition   { $$ = $1; }
  | oldstyle_function_definition { $$ = $1; }
;

normal_function_definition:
    declaration_specifiers declarator
    {
      if (isTypedef || $2->decl->type != DFUNC)
        parse_error(1, "function definition cannot be parsed.\n");
      if (symtab_get(stab, decl_getidentifier_symbol($2), FUNCNAME) == NULL)
        symtab_put(stab, decl_getidentifier_symbol($2), FUNCNAME);
      
      scope_start(stab);
      ast_declare_function_params($2);
    }
    compound_statement
    {
      scope_end(stab);
      check_for_main_and_declare($1, $2);
      $$ = FuncDef($1, $2, NULL, $4);
    }
  | declarator /* no return type */
    {
      if (isTypedef || $1->decl->type != DFUNC)
        parse_error(1, "function definition cannot be parsed.\n");
      if (symtab_get(stab, decl_getidentifier_symbol($1), FUNCNAME) == NULL)
        symtab_put(stab, decl_getidentifier_symbol($1), FUNCNAME);
      
      scope_start(stab);
      ast_declare_function_params($1);
    }
    compound_statement
    {
      astspec s = Declspec(SPEC_int);  /* return type defaults to "int" */

      scope_end(stab);
      check_for_main_and_declare(s, $1);
      $$ = FuncDef(s, $1, NULL, $3);
    }
;

oldstyle_function_definition:
    declaration_specifiers declarator /* oldstyle: params declared seperately */
    {
      if (isTypedef || $2->decl->type != DFUNC)
        parse_error(1, "function definition cannot be parsed.\n");
      if (symtab_get(stab, decl_getidentifier_symbol($2), FUNCNAME) == NULL)
        symtab_put(stab, decl_getidentifier_symbol($2), FUNCNAME);
      
      scope_start(stab);
      /* Notice here that the function parameters are declared through
       * the declaration_list and we need to do nothing else!
       */
    }
    declaration_list compound_statement
    {
      scope_end(stab);
      check_for_main_and_declare($1, $2);
      $$ = FuncDef($1, $2, $4, $5);
    }
  | declarator /* no return type & oldstyle: params declared seperately */
    {
      if (isTypedef || $1->decl->type != DFUNC)
        parse_error(1, "function definition cannot be parsed.\n");
      if (symtab_get(stab, decl_getidentifier_symbol($1), FUNCNAME) == NULL)
        symtab_put(stab, decl_getidentifier_symbol($1), FUNCNAME);

      scope_start(stab);
      /* Notice here that the function parameters are declared through
       * the declaration_list and we need to do nothing else!
       */
    }
    declaration_list compound_statement
    {
      astspec s = Declspec(SPEC_int);  /* return type defaults to "int" */

      scope_end(stab);
      check_for_main_and_declare(s, $1);
      $$ = FuncDef(s, $1, $3, $4);
    }
;

/*  ISO/IEC 9899:1999 6.9.1 */
declaration_list:
    declaration
    {
      $$ = $1;
    }
  | declaration_list declaration
    {
      $$ = BlockList($1, $2);         /* Same as block list */
    }
;


/* -------------------------------------------------------------------------
 * --- OpenMP Version 2.5 ISO/IEC 9899:1999 additions begin ----------------
 * -------------------------------------------------------------------------
 */

openmp_construct:
    parallel_construct  
    {
      $$ = $1;
    }
  | for_construct
    {
      $$ = $1;
    }
  | sections_construct
    {
      $$ = $1;
    }
  | single_construct    
    {
      $$ = $1;
    }
  | parallel_for_construct
    {
      $$ = $1;
    }
  | parallel_sections_construct
    {
      $$ = $1;
    }
  | master_construct
    {
      $$ = $1;
    }
  | critical_construct 
    {
      $$ = $1;
    }
  | atomic_construct
    {
      $$ = $1;
    }
  | ordered_construct
    {
      $$ = $1;
    }
  | /* OpenMP V3.0 */
    task_construct
    {
      $$ = $1;
    }
;

openmp_directive:
        /*
    pomp_construct
    {
      $$ = $1;
    }
  | */
    /* We create constructs out of the next two directive-only rules,
     * for uniformity.
     */
    barrier_directive 
    {
      $$ = OmpConstruct(DCBARRIER, $1, NULL);
    }
  | flush_directive 
    {
      $$ = OmpConstruct(DCFLUSH, $1, NULL);
    }
  | /* OpenMP V3.0 */
    taskwait_directive
    {
      $$ = OmpConstruct(DCTASKWAIT, $1, NULL);
    }
  | /* OpenMP V3.1 */
    taskyield_directive
    {
      $$ = OmpConstruct(DCTASKYIELD, $1, NULL);
    }
;

structured_block:
    statement
    {
      $$ = $1;
    }
;

parallel_construct:
    parallel_directive structured_block
    {
      $$ = OmpConstruct(DCPARALLEL, $1, $2);
      $$->l = $1->l;
    }
;

parallel_directive:
    PRAGMA_OMP OMP_PARALLEL parallel_clause_optseq '\n'
    {
      $$ = OmpDirective(DCPARALLEL, $3);
    }
;

parallel_clause_optseq:
    // empty
    {
      $$ = NULL;
    }
  | parallel_clause_optseq parallel_clause
    {
      $$ = OmpClauseList($1, $2);
    }
  | parallel_clause_optseq ',' parallel_clause
    {
      $$ = OmpClauseList($1, $3);
    }
;

parallel_clause:
    unique_parallel_clause
    {
      $$ = $1;
    }
  | data_clause
    {
      $$ = $1;
    }
;

unique_parallel_clause:
    OMP_IF '(' { sc_pause_openmp(); } expression ')'
    {
      sc_start_openmp();
      $$ = IfClause($4);
    }
  | OMP_NUMTHREADS '(' { sc_pause_openmp(); } expression ')'
    {
      sc_start_openmp();
      $$ = NumthreadsClause($4);
    }
;

for_construct:
    for_directive iteration_statement_for
    {
      $$ = OmpConstruct(DCFOR, $1, $2);
    }
;

for_directive:
    PRAGMA_OMP OMP_FOR for_clause_optseq '\n'
    {
      $$ = OmpDirective(DCFOR, $3);
    }
;

for_clause_optseq:
    // empty
    {
      $$ = NULL;
    }
  | for_clause_optseq for_clause
    {
      $$ = OmpClauseList($1, $2);
    }
  | for_clause_optseq ',' for_clause
    {
      $$ = OmpClauseList($1, $3);
    }
;

for_clause:
    unique_for_clause
    {
      $$ = $1;
    }
  | data_clause
    {
      $$ = $1;
    }
  | OMP_NOWAIT
    {
      $$ = PlainClause(OCNOWAIT);
    }
;

unique_for_clause:
    OMP_ORDERED
    {
      $$ = PlainClause(OCORDERED);
    }
  | OMP_SCHEDULE '(' schedule_kind ')'
    {
      $$ = ScheduleClause($3, NULL);
    }
  | OMP_SCHEDULE '(' schedule_kind ',' { sc_pause_openmp(); } expression ')'
    {
      sc_start_openmp();
      if ($3 == OC_runtime)
        parse_error(1, "\"runtime\" schedules may not have a chunksize.\n");
      $$ = ScheduleClause($3, $6);
    }
  | OMP_SCHEDULE '(' OMP_AFFINITY ',' 
    {  /* non-OpenMP schedule */
      tempsave = checkDecls;
      checkDecls = 0;   /* Because the index of the loop is usualy involved */
      sc_pause_openmp();
    }
    expression ')'
    {
      sc_start_openmp();
      checkDecls = tempsave;
      $$ = ScheduleClause(OC_affinity, $6);
    }
  | OMP_COLLAPSE '(' expression /* CONSTANT */ ')'   /* OpenMP V3.0 */
    {
      int n = 0, er = 0;
      if (xar_expr_is_constant($3))
      {
        n = xar_calc_int_expr($3, &er);
        if (er) n = 0;
      }
      if (n <= 0) 
        parse_error(1, "invalid number in collapse() clause.\n");
      $$ = CollapseClause(n);
    }
;

schedule_kind:
    OMP_STATIC
    {
      $$ = OC_static;
    }
  | OMP_DYNAMIC
    {
      $$ = OC_dynamic;
    }
  | OMP_GUIDED
    {
      $$ = OC_guided;
    }
  | OMP_RUNTIME
    {
      $$ = OC_runtime;
    }
  | OMP_AUTO      /* OpenMP 3.0 */
    {
      $$ = OC_auto;
    }
  | error { parse_error(1, "invalid openmp schedule type.\n"); }
      
;

sections_construct:
    sections_directive section_scope
    {
      $$ = OmpConstruct(DCSECTIONS, $1, $2);
    }
;

sections_directive:
    PRAGMA_OMP OMP_SECTIONS sections_clause_optseq '\n'
    {
      $$ = OmpDirective(DCSECTIONS, $3);
    }
;

sections_clause_optseq:
    // empty
    {
      $$ = NULL;
    }
  | sections_clause_optseq sections_clause
    {
      $$ = OmpClauseList($1, $2);
    }
  | sections_clause_optseq ',' sections_clause
    {
      $$ = OmpClauseList($1, $3);
    }
;

sections_clause:
    data_clause
    {
      $$ = $1;
    }
  | OMP_NOWAIT
    {
      $$ = PlainClause(OCNOWAIT);
    }
;

section_scope:
    '{' section_sequence '}'
    {
      $$ = Compound($2);
    }
;

section_sequence:
    structured_block  // 1 shift/reduce conflict here
    {
      /* Make it look like it had a section pragma */
      $$ = OmpStmt( OmpConstruct(DCSECTION, OmpDirective(DCSECTION,NULL), $1) );
    }
  | section_directive structured_block
    {
      $$ = OmpStmt( OmpConstruct(DCSECTION, $1, $2) );
    }
  | section_sequence section_directive structured_block
    {
      $$ = BlockList($1, OmpStmt( OmpConstruct(DCSECTION, $2, $3) ));
    }
;

section_directive:
    PRAGMA_OMP OMP_SECTION '\n'
    {
      $$ = OmpDirective(DCSECTION, NULL);
    }
;

single_construct:
    single_directive structured_block
    {
      $$ = OmpConstruct(DCSINGLE, $1, $2);
    }
;

single_directive:
    PRAGMA_OMP OMP_SINGLE single_clause_optseq '\n'
    {
      $$ = OmpDirective(DCSINGLE, $3);
    }
;

single_clause_optseq:
    // empty
    {
      $$ = NULL;
    }
  | single_clause_optseq single_clause
    {
      $$ = OmpClauseList($1, $2);
    }
  | single_clause_optseq ',' single_clause
    {
      $$ = OmpClauseList($1, $3);
    }
;

single_clause:
    data_clause
    {
      $$ = $1;
    }
  | OMP_NOWAIT
    {
      $$ = PlainClause(OCNOWAIT);
    }
;

parallel_for_construct:
    parallel_for_directive iteration_statement_for
    {
      $$ = OmpConstruct(DCPARFOR, $1, $2);
    }
;

parallel_for_directive:
    PRAGMA_OMP OMP_PARALLEL OMP_FOR parallel_for_clause_optseq '\n'
    {
      $$ = OmpDirective(DCPARFOR, $4);
    }
;

parallel_for_clause_optseq:
    // empty
    {
      $$ = NULL;
    }
  | parallel_for_clause_optseq parallel_for_clause
    {
      $$ = OmpClauseList($1, $2);
    }
  | parallel_for_clause_optseq ',' parallel_for_clause
    {
      $$ = OmpClauseList($1, $3);
    }
;

parallel_for_clause:
    unique_parallel_clause
    {
      $$ = $1;
    }
  | unique_for_clause
    {
      $$ = $1;
    }
  | data_clause
    {
      $$ = $1;
    }
;

parallel_sections_construct:
    parallel_sections_directive section_scope
    {
      $$ = OmpConstruct(DCPARSECTIONS, $1, $2);
    }
;

parallel_sections_directive:
    PRAGMA_OMP OMP_PARALLEL OMP_SECTIONS parallel_sections_clause_optseq '\n'
    {
      $$ = OmpDirective(DCPARSECTIONS, $4);
    }
;

parallel_sections_clause_optseq:
    // empty
    {
      $$ = NULL;
    }
  | parallel_sections_clause_optseq parallel_sections_clause
    {
      $$ = OmpClauseList($1, $2);
    }
  | parallel_sections_clause_optseq ',' parallel_sections_clause
    {
      $$ = OmpClauseList($1, $3);
    }
;

parallel_sections_clause:
    unique_parallel_clause
    {
      $$ = $1;
    }
  | data_clause
    {
      $$ = $1;
    }
;

/* OpenMP V3.0 */
task_construct:
    task_directive structured_block
    {
      $$ = OmpConstruct(DCTASK, $1, $2);
      $$->l = $1->l;
    }
;

/* OpenMP V3.0 */
task_directive:
    PRAGMA_OMP OMP_TASK task_clause_optseq '\n'
    {
      $$ = OmpDirective(DCTASK, $3);
    }
;

/* OpenMP V3.0 */
task_clause_optseq:
    // empty
    {
      $$ = NULL;
    }
  | task_clause_optseq task_clause
    {
      $$ = OmpClauseList($1, $2);
    }
  | task_clause_optseq ',' task_clause
    {
      $$ = OmpClauseList($1, $3);
    }
;

/* OpenMP V3.0 */
task_clause:
    unique_task_clause
    {
      $$ = $1;
    }
  | data_clause
    {
      $$ = $1;
    }
;

/* OpenMP V3.0 */
unique_task_clause:
    OMP_IF '(' { sc_pause_openmp(); } expression ')'
    {
      sc_start_openmp();
      $$ = IfClause($4);
    }
  | OMP_UNTIED
    {
      $$ = PlainClause(OCUNTIED);
    }
  | OMP_FINAL '(' { sc_pause_openmp(); } expression ')'
    {
      sc_start_openmp();
      $$ = FinalClause($4);
    }
  | OMP_MERGEABLE
    {
      $$ = PlainClause(OCMERGEABLE);
    };

master_construct:
    master_directive structured_block
    {
      $$ = OmpConstruct(DCMASTER, $1, $2);
    }
;

master_directive:
    PRAGMA_OMP OMP_MASTER '\n'
    {
      $$ = OmpDirective(DCMASTER, NULL);
    }
;

critical_construct:
    critical_directive structured_block
    {
      $$ = OmpConstruct(DCCRITICAL, $1, $2);
    }
;

critical_directive:
    PRAGMA_OMP OMP_CRITICAL '\n'
    {
      $$ = OmpCriticalDirective(NULL);
    }
  | PRAGMA_OMP OMP_CRITICAL region_phrase '\n'
    {
      $$ = OmpCriticalDirective($3);
    }
;

region_phrase:
    '(' IDENTIFIER ')'
    {
      $$ = Symbol($2);
    }
;

/* OpenMP V3.0 */
taskwait_directive:
    PRAGMA_OMP OMP_TASKWAIT '\n'
    {
      $$ = OmpDirective(DCTASKWAIT, NULL);
    }
;

/* OpenMP V3.1 */
taskyield_directive:
    PRAGMA_OMP OMP_TASKYIELD '\n'
    {
      $$ = OmpDirective(DCTASKYIELD, NULL);
    }
;

barrier_directive:
    PRAGMA_OMP OMP_BARRIER '\n'
    {
      $$ = OmpDirective(DCBARRIER, NULL);
    }
;

atomic_construct:
    atomic_directive expression_statement
    {
      $$ = OmpConstruct(DCATOMIC, $1, $2);
    }
;

atomic_directive:
    PRAGMA_OMP OMP_ATOMIC '\n'
    {
      $$ = OmpDirective(DCATOMIC, NULL);
    }
  | PRAGMA_OMP OMP_ATOMIC OMP_WRITE'\n'
    {
      $$ = OmpDirective(DCATOMIC, NULL);
    }
  | PRAGMA_OMP OMP_ATOMIC OMP_READ'\n'
    {
      $$ = OmpDirective(DCATOMIC, NULL);
    }
  | PRAGMA_OMP OMP_ATOMIC OMP_UPDATE'\n'
    {
      $$ = OmpDirective(DCATOMIC, NULL);
    }
;

flush_directive:
    PRAGMA_OMP OMP_FLUSH '\n'
    {
      $$ = OmpFlushDirective(NULL);
    }
  | PRAGMA_OMP OMP_FLUSH flush_vars '\n'
    {
      $$ = OmpFlushDirective($3);
    }
;

flush_vars:
    '(' { sc_pause_openmp(); } variable_list ')'
    {
      sc_start_openmp();
      $$ = $3;
    }
;

ordered_construct:
    ordered_directive structured_block
    {
      $$ = OmpConstruct(DCORDERED, $1, $2);
    }
;

ordered_directive:
    PRAGMA_OMP OMP_ORDERED '\n'
    {
      $$ = OmpDirective(DCORDERED, NULL);
    }
;

threadprivate_directive:
    PRAGMA_OMP_THREADPRIVATE '(' thrprv_variable_list ')' '\n'
    {
      $$ = OmpThreadprivateDirective($3);
    }
;

data_clause:
    OMP_PRIVATE { sc_pause_openmp(); } '(' variable_list ')'
    {
      sc_start_openmp();
      $$ = VarlistClause(OCPRIVATE, $4);
    }
  | /* OpenMP 2.0 */
    OMP_COPYPRIVATE  { sc_pause_openmp(); } '(' variable_list ')'
    {
      sc_start_openmp();
      $$ = VarlistClause(OCCOPYPRIVATE, $4);
    }
  | OMP_FIRSTPRIVATE { sc_pause_openmp(); } '(' variable_list ')'
    {
      sc_start_openmp();
      $$ = VarlistClause(OCFIRSTPRIVATE, $4);
    }
  | OMP_LASTPRIVATE { sc_pause_openmp(); } '(' variable_list ')'
    {
      sc_start_openmp();
      $$ = VarlistClause(OCLASTPRIVATE, $4);
    }
  | OMP_SHARED { sc_pause_openmp(); } '(' variable_list ')'
    {
      sc_start_openmp();
      $$ = VarlistClause(OCSHARED, $4);
    }
  | OMP_DEFAULT '(' OMP_SHARED ')'
    {
      $$ = DefaultClause(OC_defshared);
    }
  | OMP_DEFAULT '(' OMP_NONE ')'
    {
      $$ = DefaultClause(OC_defnone);
    }
  | OMP_REDUCTION '(' reduction_operator { sc_pause_openmp(); } ':' variable_list ')'
    {
      sc_start_openmp();
      $$ = ReductionClause($3, $6);
    }
  | OMP_COPYIN { sc_pause_openmp(); } '(' variable_list ')'
    {
      sc_start_openmp();
      $$ = VarlistClause(OCCOPYIN, $4);
    }
;

reduction_operator:
    '+'
    {
      $$ = OC_plus;
    }
  | '*'
    {
      $$ = OC_times;
    }
  | '-'
    {
      $$ = OC_minus;
    }
  | '&'
    {
      $$ = OC_band;
    }
  | '^'
    {
      $$ = OC_xor;
    }
  | '|'
    {
      $$ = OC_bor;
    }
  | AND_OP
    {
      $$ = OC_land;
    }
  | OR_OP
    {
      $$ = OC_lor;
    }
  | OMP_MIN
    {
      $$ = OC_min;
    }
  | OMP_MAX
    {
      $$ = OC_max;
    }
;

variable_list:
    IDENTIFIER
    {
      if (checkDecls) 
        if (symtab_get(stab, Symbol($1), IDNAME) == NULL)
          parse_error(-1, "unknown identifier `%s'.\n", $1);
      $$ = IdentifierDecl( Symbol($1) );
    }
  | variable_list ',' IDENTIFIER
    {
      if (checkDecls) 
        if (symtab_get(stab, Symbol($3), IDNAME) == NULL)
          parse_error(-1, "unknown identifier `%s'.\n", $3);
      $$ = IdList($1, IdentifierDecl( Symbol($3) ));
    }
;

/* The same as "variable_list" only it checks if the variables
 * are declared @ the *same* scope level and whether they include the
 * "static" specifier. The original variable is also marked as
 * threadprivate.
 */
thrprv_variable_list:
    IDENTIFIER
    {
      if (checkDecls)
      {
        stentry e = symtab_get(stab, Symbol($1), IDNAME);
        if (e == NULL)
          parse_error(-1, "unknown identifier `%s'.\n", $1);
        if (e->scopelevel != stab->scopelevel)
          parse_error(-1, "threadprivate directive appears at different "
                          "scope level\nfrom the one `%s' was declared.\n", $1);
        if (stab->scopelevel > 0)    /* Don't care for globals */
          if (speclist_getspec(e->spec, STCLASSSPEC, SPEC_static) == NULL)
            parse_error(-1, "threadprivate variable `%s' does not have static "
                            "storage type.\n", $1);
        e->isthrpriv = 1;   /* Mark */
      }
      $$ = IdentifierDecl( Symbol($1) );
    }
  | thrprv_variable_list ',' IDENTIFIER
    {
      if (checkDecls) 
      {
        stentry e = symtab_get(stab, Symbol($3), IDNAME);
        if (e == NULL)
          parse_error(-1, "unknown identifier `%s'.\n", $3);
        if (e->scopelevel != stab->scopelevel)
          parse_error(-1, "threadprivate directive appears at different "
                          "scope level\nfrom the one `%s' was declared.\n", $3);
        if (stab->scopelevel > 0)    /* Don't care for globals */
          if (speclist_getspec(e->spec, STCLASSSPEC, SPEC_static) == NULL)
            parse_error(-1, "threadprivate variable `%s' does not have static "
                            "storage type.\n", $3);
        e->isthrpriv = 1;   /* Mark */
      }
      $$ = IdList($1, IdentifierDecl( Symbol($3) ));
    }
;

/* -------------------------------------------------------------------------
 * --- OMPi extensions -----------------------------------------------------
 * -------------------------------------------------------------------------
 */

ompix_directive:
    ox_tasksync_directive
    {
      $$ = OmpixConstruct(OX_DCTASKSYNC, $1, NULL);
    }
  | ox_taskschedule_directive
    {
      $$ = OmpixConstruct(OX_DCTASKSCHEDULE, $1, NULL);
    }

;

ox_tasksync_directive:
    PRAGMA_OMPIX OMPIX_TASKSYNC '\n'
    {
      $$ = OmpixDirective(OX_DCTASKSYNC, NULL);
    }
;

ox_taskschedule_directive:
    PRAGMA_OMPIX OMPIX_TASKSCHEDULE
    { 
      scope_start(stab); 
    }
    ox_taskschedule_clause_optseq '\n'
    {
      scope_end(stab);
      $$ = OmpixDirective(OX_DCTASKSCHEDULE, $4);
    }
;

ox_taskschedule_clause_optseq:
    // empty
    {
      $$ = NULL;
    }
  | ox_taskschedule_clause_optseq ox_taskschedule_clause
    {
      $$ = OmpixClauseList($1, $2);
    }
  | ox_taskschedule_clause_optseq ',' ox_taskschedule_clause
    {
      $$ = OmpixClauseList($1, $3);
    }
;

ox_taskschedule_clause:
    OMPIX_STRIDE '(' assignment_expression')'
    {
      $$ = OmpixStrideClause($3);
    }
  | OMPIX_START '(' assignment_expression ')'
    {
      $$ = OmpixStartClause($3);
    }
  | OMPIX_SCOPE '(' ox_scope_spec ')'
    {
      $$ = OmpixScopeClause($3);
    }
  | OMPIX_TIED
    {
      $$ = OmpixPlainClause(OX_OCTIED);
    }
  | OMP_UNTIED
    {
      $$ = OmpixPlainClause(OX_OCUNTIED);
    }
;

ox_scope_spec:
    OMPIX_NODES
    {
      $$ = OX_SCOPE_NODES;
    }
  | OMPIX_WORKERS
    {
      $$ = OX_SCOPE_WGLOBAL;
    }
  | OMPIX_WORKERS ',' OMPIX_GLOBAL
    {
      $$ = OX_SCOPE_WGLOBAL;
    }
  | OMPIX_WORKERS ',' OMPIX_LOCAL
    {
      $$ = OX_SCOPE_WLOCAL;
    }
;

ompix_construct:
    ox_taskdef_construct  
    {
      $$ = $1;
    }
  | ox_task_construct
    {
      $$ = $1;
    }
;

/* 1 reduce-reduce here; we'll improve it some day.. */
ox_taskdef_construct:
   ox_taskdef_directive normal_function_definition 
    {
      /* Should put the name of the callback function in the stab, too
      if (symtab_get(stab, decl_getidentifier_symbol($2->u.declaration.decl),
            FUNCNAME) == NULL)
        symtab_put(stab, decl_getidentifier_symbol($2->u.declaration.spec),
            FUNCNAME);
      */
      scope_start(stab);   /* re-declare the arguments of the task function */
      ast_declare_function_params($2->u.declaration.decl);
    } 
    compound_statement
    {
      scope_end(stab);
      $$ = OmpixTaskdef($1, $2, $4);
      $$->l = $1->l;
    }
  | ox_taskdef_directive normal_function_definition
    {
      $$ = OmpixTaskdef($1, $2, NULL);
      $$->l = $1->l;
    }
;

ox_taskdef_directive:
    PRAGMA_OMPIX OMPIX_TASKDEF
    { 
      scope_start(stab); 
    }
    ox_taskdef_clause_optseq '\n'
    {
      scope_end(stab);
      $$ = OmpixDirective(OX_DCTASKDEF, $4);
    }
;

ox_taskdef_clause_optseq:
    // empty
    {
      $$ = NULL;
    }
  | ox_taskdef_clause_optseq ox_taskdef_clause
    {
      $$ = OmpixClauseList($1, $2);
    }
  | ox_taskdef_clause_optseq ',' ox_taskdef_clause
    {
      $$ = OmpixClauseList($1, $3);
    }
;

ox_taskdef_clause:
    OMPIX_IN '(' ox_variable_size_list')'
    {
      $$ = OmpixVarlistClause(OX_OCIN, $3);
    }
  | OMPIX_OUT '(' ox_variable_size_list')'
    {
      $$ = OmpixVarlistClause(OX_OCOUT, $3);
    }
  | OMPIX_INOUT '(' ox_variable_size_list')'
    {
      $$ = OmpixVarlistClause(OX_OCINOUT, $3);
    }
  | OMP_REDUCTION '(' reduction_operator ':' ox_variable_size_list ')'
    {
      $$ = OmpixReductionClause($3, $5);
    }
;

ox_variable_size_list:
    ox_variable_size_elem 
    {
      $$ = $1;
    }
  | ox_variable_size_list ',' ox_variable_size_elem
    {
      $$ = IdList($1, $3);
    }
;

ox_variable_size_elem:  
    IDENTIFIER
    {
      $$ = IdentifierDecl( Symbol($1) );
      symtab_put(stab, Symbol($1), IDNAME);
    }
  | IDENTIFIER '[' '?' IDENTIFIER ']'
    {
      if (checkDecls) check_uknown_var($4);
      /* Use extern to differentiate */
      $$ = ArrayDecl(IdentifierDecl( Symbol($1) ), StClassSpec(SPEC_extern), 
                     Identifier(Symbol($4)));
      symtab_put(stab, Symbol($1), IDNAME);
    }
  | IDENTIFIER '[' assignment_expression ']'
    {
      $$ = ArrayDecl(IdentifierDecl( Symbol($1) ), NULL, $3);
      symtab_put(stab, Symbol($1), IDNAME);
    }
;

ox_task_construct:
    ox_task_directive ox_funccall_expression ';'
    {
      $$ = OmpixConstruct(OX_DCTASK, $1, Expression($2));
      $$->l = $1->l;
    }
;

ox_task_directive:
    PRAGMA_OMPIX OMP_TASK ox_task_clause_optseq '\n'
    {
      $$ = OmpixDirective(OX_DCTASK, $3);
    }
;

ox_task_clause_optseq:
    // empty
    {
      $$ = NULL;
    }
  | ox_task_clause_optseq ox_task_clause
    {
      $$ = OmpixClauseList($1, $2);
    }
  | ox_task_clause_optseq ',' ox_task_clause
    {
      $$ = OmpixClauseList($1, $3);
    }
;

ox_task_clause:
    OMPIX_ATNODE '(' '*' ')'
    {
      $$ = OmpixPlainClause(OX_OCATALL);
    }
  | OMPIX_ATNODE '(' assignment_expression ')'
    {
      $$ = OmpixAtnodeClause($3);
    }
  | OMPIX_ATWORKER '(' assignment_expression ')'
    {
      $$ = OmpixAtworkerClause($3);
    }
  | OMPIX_TIED
    {
      $$ = OmpixPlainClause(OX_OCTIED);
    }
  | OMP_UNTIED
    {
      $$ = OmpixPlainClause(OX_OCUNTIED);
    }
  | OMPIX_DETACHED
    {
      $$ = OmpixPlainClause(OX_OCDETACHED);
    }
;

ox_funccall_expression:		
    IDENTIFIER '(' ')' 
    {
      $$ = strcmp($1, "main") ?
             FunctionCall(Identifier(Symbol($1)), NULL) :
             FunctionCall(Identifier(Symbol(MAIN_NEWNAME)), NULL);
    }
  | IDENTIFIER '(' argument_expression_list ')'  
    {
      $$ = strcmp($1, "main") ?
             FunctionCall(Identifier(Symbol($1)), $3) :
             FunctionCall(Identifier(Symbol(MAIN_NEWNAME)), $3);
    }
;


%%


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                               *
 *     CODE                                                      *
 *                                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


void yyerror(char *s)
{
  fprintf(stderr, "(file %s, line %d, column %d):\n\t%s\n",
                  sc_original_file(), sc_original_line(), sc_column(), s);
}


/* Check whether the identifier is known or not
 */
void check_uknown_var(char *name)
{
  symbol s = Symbol(name);
  if (symtab_get(stab, s, IDNAME) == NULL &&
      symtab_get(stab, s, LABELNAME) == NULL &&
      symtab_get(stab, s, FUNCNAME) == NULL)
    parse_error(-1, "unknown identifier `%s'.\n", name);
}



/* See the "declaration" rule: if the last element of the list
 * is a user typename, we remove it, and we return it as an
 * identifier declarator.
 * The list should have 3 elements (typedef xxx type).
 */
astdecl fix_known_typename(astspec s)
{
  astspec prev;
  astdecl d;
  
  if (s->type != SPECLIST || s->u.next->type != SPECLIST) return (NULL);
  
  for (; s->u.next->type == SPECLIST; prev = s, s = s->u.next)  
    ;   /* goto last list node */
  if (s->u.next->type != USERTYPE)         /* nope */
    return (NULL);
    
  prev->u.next = s->body;
  
  d = Declarator(NULL, IdentifierDecl(s->u.next->name));
  if (checkDecls) 
    symtab_put(stab, s->u.next->name, TYPENAME);
  free(s);
  return (d);
}


void check_for_main_and_declare(astspec s, astdecl d)
{
  astdecl n = decl_getidentifier(d);

  assert(d->type == DECLARATOR);
  assert(d->decl->type == DFUNC);
  
  if (strcmp(n->u.id->name, "main") == 0)
  {
    n->u.id = Symbol(MAIN_NEWNAME);         /* Catch main()'s definition */
    hasMainfunc = 1;

    /* Now check for return type and # parameters */
    /* It d != NULL then its parameters is either (id or idlist) or
     * (paramtype or parmatypelist). If it is a list, assume the
     * standard 2 params, otherwise, we guess the single argument
     * must be the type "void" which means no params.
     * In any case, we always force main have (argc, argv[]).
     */
    if (d->decl->u.params == NULL || d->decl->u.params->type != DLIST)
      d->decl->u.params =
          ParamList(
            ParamDecl(
              Declspec(SPEC_int),
              Declarator( NULL, IdentifierDecl( Symbol("_argc_ignored") ) )
            ),
            ParamDecl(
              Declspec(SPEC_char),
              Declarator(Speclist_right( Pointer(), Pointer() ),
                         IdentifierDecl( Symbol("_argv_ignored") ))
            )
          );
   
    mainfuncRettype = 0; /* int */
    if (s != NULL)
    {
      for (; s->type == SPECLIST && s->subtype == SPEC_Rlist; s = s->u.next)
        if (s->body->type == SPEC && s->body->subtype == SPEC_void)
        {
          s = s->body;
          break;
        };
      if (s->type == SPEC && s->subtype == SPEC_void)
        mainfuncRettype = 1; /* void */
    }
  }
  if (symtab_get(stab, n->u.id, FUNCNAME) == NULL)/* From earlier declaration */
    symtab_put(stab, n->u.id, FUNCNAME);
}


/* For each variable/typename in the given declaration, add pointers in the
 * symbol table entries back to the declaration nodes.
 */
void add_declaration_links(astspec s, astdecl d)
{
  astdecl ini = NULL;
  
  if (d->type == DLIST && d->subtype == DECL_decllist)
  {
    add_declaration_links(s, d->u.next);
    d = d->decl;
  }
  if (d->type == DINIT) d = (ini = d)->decl;   /* Skip the initializer */
  assert(d->type == DECLARATOR);
  if (d->decl != NULL && d->decl->type != ABSDECLARATOR)
  {
    symbol  t = decl_getidentifier_symbol(d->decl);
    stentry e = isTypedef ?
                symtab_get(stab,t,TYPENAME) :
                symtab_get(stab,t,(decl_getkind(d)==DFUNC) ? FUNCNAME : IDNAME);
    e->spec  = s;
    e->decl  = d;
    e->idecl = ini;
  }
}


void parse_error(int exitvalue, char *format, ...)
{
  va_list ap;

  va_start(ap, format);
  fprintf(stderr, "(%s, line %d)\n\t", sc_original_file(), sc_original_line());
  vfprintf(stderr, format, ap);
  va_end(ap);
  if (strcmp(sc_original_file(), "injected_code") == 0)
    fprintf(stderr, "\n>>>>>>>\n%s\n>>>>>>>\n", parsingstring);
  _exit(exitvalue);
}


void parse_warning(char *format, ...)
{
  va_list ap;

  va_start(ap, format);
  fprintf(stderr, "[warning] ");
  vfprintf(stderr, format, ap);
  va_end(ap);
}


aststmt parse_file(char *fname, int *error)
{
  *error = 0;
  if ( (yyin = fopen(fname, "r")) == NULL )
    return (NULL);
  sc_set_filename(fname);      /* Inform the scanner */
  *error = yyparse();
  fclose(yyin);                /* No longer needed */
  return (pastree);
}


#define PARSE_STRING_SIZE 8192


astexpr parse_expression_string(char *format, ...)
{
  static char s[PARSE_STRING_SIZE];
  int    savecD;
  
  va_list ap;
  va_start(ap, format);
  vsnprintf(s, PARSE_STRING_SIZE-1, format, ap);
  va_end(ap);
  parsingstring = s;
  sc_scan_string(s);
  sc_set_start_token(START_SYMBOL_EXPRESSION);

  savecD = checkDecls;
  checkDecls = 0;         /* Don't check identifiers & don't declare them */
  yyparse();
  checkDecls = savecD;    /* Reset it */
  return ( pastree_expr );
}


aststmt parse_blocklist_string(char *format, ...)
{
  static char s[PARSE_STRING_SIZE];
  int    savecD;

  va_list ap;
  va_start(ap, format);
  vsnprintf(s, PARSE_STRING_SIZE-1, format, ap);
  va_end(ap);
  parsingstring = s;
  sc_scan_string(s);
  sc_set_start_token(START_SYMBOL_BLOCKLIST);

  savecD = checkDecls;
  checkDecls = 0;         /* Don't check identifiers & don't declare them */
  yyparse();
  checkDecls = savecD;    /* Reset it */
  return ( pastree_stmt );
}


aststmt parse_and_declare_blocklist_string(char *format, ...)
{
  static char s[PARSE_STRING_SIZE];
  int    savecD;
  
  va_list ap;
  va_start(ap, format);
  vsnprintf(s, PARSE_STRING_SIZE-1, format, ap);
  va_end(ap);
  parsingstring = s;
  sc_scan_string(s);
  sc_set_start_token(START_SYMBOL_BLOCKLIST);

  savecD = checkDecls;
  checkDecls = 1;         /* Do check identifiers & do declare them */
  yyparse();
  checkDecls = savecD;    /* Reset it */
  return ( pastree_stmt );
}

/*
  OMPi OpenMP Compiler
  == Copyright since 2001, the OMPi Team
  == Department of Computer Science, University of Ioannina

  This file is part of OMPi.

  OMPi is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  OMPi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with OMPi; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* OMPICC
 * A driver for the OMPi compiler.
 */

/*
 * Oct 2010:
 *   Improvements; correct argument quoting.
 * Apr 2010:
 *   Some improvements
 * Aug 2007:
 *   Major simplifications due to new threadprivate implementation.
 *   Removed POMP support.
 * May 2007:
 *   Fixed some definitions for compiling/linking/preprocessing.
 *   -lrt now determined at configuration time.
 *   Some less important bug fixes.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#if defined(HAVE_REALPATH)
  #include <limits.h>
#endif

#include "config.h"

#define LEN  4096
#define SLEN 1024

/* Definitions (macros) provided extrenally (see Makefile.am)
 *
 * OmpiName
 * CPPcmd
 * CCcmd
 * PreprocFlags
 * CompileFlags
 * LinkFlags
 * IncludeDir
 * LibDir
 */

/* Flags collected from the OMPI_CPP, OMPI_CPPFLAGS,
 * OMPI_CC, OMPI_CFLAGS and OMPI_LDFLAGS, from the ./configure info and the
 * library-specific configuration file.
 */
#ifdef PATH_MAX
  #define PATHSIZE PATH_MAX
  #define FLAGSIZE PATH_MAX
#else
  #define PATHSIZE 4096
  #define FLAGSIZE 4096
#endif
char PREPROCESSOR[PATHSIZE], COMPILER[PATHSIZE],
     CPPFLAGS[FLAGSIZE], CFLAGS[FLAGSIZE], LDFLAGS[FLAGSIZE],
     ORTINFO[FLAGSIZE];
char ortlibname[PATHSIZE],           /* The runtime library needed */
     RealOmpiName[PATHSIZE];

typedef struct arg_s {
  char opt;
  char val[SLEN];
  struct arg_s *next;
} arg_t;

typedef struct {
   arg_t *head, *tail;
} arglist_t;


#define ompi_info() \
    fprintf(stderr,\
      "This is %s using\n  >> system compiler: %s\n  >> runtime library: %s\n",\
      PACKAGE_STRING, COMPILER, *ORTINFO ? ORTINFO : ortlibname)


static void ompicc_error(int exitcode, char *format, ...)
{
  va_list ap;

  va_start(ap, format);
  fprintf(stderr, "[ompicc error]: ");
  vfprintf(stderr, format, ap);
  va_end(ap);

  exit(exitcode);
}


static char *get_basename(char *path)
{
  char *s;

  if (path == NULL || *path == 0)
    return ".";
  else if (path[0] == '/' && path[1] == 0)
    return path;
  else {
    s = path;
    while (*s)
      s++;
    s--;
    while (*s == '/' && s != path)
      *s-- = 0;
    if (s == path)
      return path;
    while (*s != '/' && s != path)
      s--;
    if (*s == '/')
      return s + 1;
    else return path;
  }
}


static arg_t *new_arg(char opt, char *val)
{
  arg_t *p;

  if ((p = (arg_t *) malloc(sizeof(arg_t))) == NULL)
    ompicc_error(-1, "malloc() failed\n");
  p->opt = opt;
  if (val != NULL)
    strcpy(p->val, val);
  else p->val[0] = 0;
  p->next = NULL;
  return p;
}


void arglist_add(arglist_t *l, arg_t *arg)
{
  if (l->head == NULL)
    l->head = l->tail = arg;
  else {
    l->tail->next = arg;
    l->tail = arg;
  }
}


int append_arg(arglist_t *l, int argc, char **argv, int proceed)
{
  char opt, val[SLEN];
  arg_t *p;

  val[0] = 0;
  if (argv[0][0] == 0)
    return 0;
  if (argv[0][0] != '-') {
    p = new_arg(0, *argv);
    arglist_add(l, p);
    return 0;
  }
  opt = argv[0][1];

  if (argv[0][2] != 0) {
    strcpy(val, &argv[0][2]);
    p = new_arg(opt, val);
    arglist_add(l, p);
    return 0;
  }
  else {
    if (proceed && argc > 1)
      strcpy(val, &argv[1][0]);
    p = new_arg(opt, val);
    arglist_add(l, p);
    return proceed && argc > 1;
  }
}


static int quotedlen(char *s)
{
  int len;
  for (len = 0; *s != 0; s++)
    len += (*s == '"' ? 2 : 1);
  return (len);
}


/* Prepare a string from an argument list; all args are quoted */
static void strarglist(char *dest, arglist_t *l, int maxlen)
{
  arg_t *p;
  char  *c, *d = dest;

  for (*d = 0, p = l->head; p != NULL; p = p->next) 
  {
    if ((d-dest) + quotedlen(p->val) + 6 >= maxlen)
      ompicc_error(1, "argument(s) too long; rebuild OMPi with larger LEN.\n");    
    if (p->opt) 
    {
      sprintf(dest, "-%c ", p->opt);
      dest += ( (p->opt == 'o') ? 3 : 2 );
    }
    *(dest++) = '"';
    for (c = p->val; *c != 0; c++) 
    {
      if (*c == '"') *(dest++) = '\\';
      *(dest++) = *c;
    } 
    *(dest++) = '"';
    *(dest++) = ' ';
  }
  *dest = 0;
}


int fok(char *fname)
{
  struct stat buf;

  return ( stat(fname, &buf) == 0 );
}


int disableOpenMP = 0,
    disableOmpix = 0,
    disableCodeDuplication = 0;
int mustlink = 1;           /* Becomes 0 if -c parameter is specified */
int keep = 0;               /* Becomes 1/2 if -k/K parameter is specified */
int verbose = 0;

arglist_t files     = { NULL,NULL };       /* Files to be compiled/linked */
arglist_t goutfile  = { NULL,NULL };       /* Output, -o XXX */
arglist_t prep_args = { NULL,NULL };       /* Preprocessor args */
arglist_t link_args = { NULL,NULL };       /* Linker args */
arglist_t scc_flags = { NULL,NULL };       /* Remaining system compiler args */


void parse_args(int argc, char **argv)
{
  int  d, ortlib = 0;
  char *parameter;

  argv++;
  argc--;

  while (argc) {
    d = 0;
    parameter = argv[0];
    if (strncmp(parameter, "--ort=", 6) == 0)
    {
      strncpy(ortlibname, parameter+6, 511);
      ortlib = 1;
    }
    else if (strncmp(parameter, "--nomp", 6) == 0)
      disableOpenMP = 1;
    else if  (strncmp(parameter, "--nox", 5) == 0)
      disableOmpix = 1;
    else if  (strncmp(parameter, "--nocodup", 9) == 0)
      disableCodeDuplication = 1;
    else
      if (argv[0][0] == '-')              /* option */
        switch (argv[0][1])
        {
          case 'c': mustlink = 0; break;
          case 'l': d = append_arg(&link_args, argc, argv, 1); break;
          case 'L': d = append_arg(&link_args, argc, argv, 1); break;
          case 'I': d = append_arg(&prep_args, argc, argv, 1); break;
          case 'D': d = append_arg(&prep_args, argc, argv, 1); break;
          case 'U': d = append_arg(&prep_args, argc, argv, 1); break;
          case 'o': d = append_arg(&goutfile, argc, argv, 1); break;
          case 'k': keep = 1; d = 0; break;
          case 'K': keep = 2; d = 0; break;
          case 'v': verbose = 1; d = 0; break;
          default:
            d = append_arg(&scc_flags, argc, argv, 0);
            if (strcmp(argv[0], "--version") == 0)
            {
              printf("%s\n", VERSION);
              _exit(0);
            }
        }
      else
      {
        d = append_arg(&files, argc, argv, 0);
        if (!fok(files.tail->val))
          ompicc_error(1, "file %s does not exist\n", files.tail->val);
      };
    argc = argc - 1 - d;
    argv = argv + 1 + d;
  }

  if (!ortlib)
    strcpy(ortlibname, "default");    /* Default lib to use */
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                           *
 *        COMPILING                                          *
 *                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


void ompicc_compile(char *fname)
{
  char *s, preflags[SLEN], noext[SLEN], outfile[SLEN];
  char cmd[LEN], strscc_flags[LEN], strgoutfile[LEN];
  int res;

  if ((s = strrchr(fname, '.')) != NULL)
    if (strcmp(s, ".o") == 0) return;     /* skip .o files */

  strcpy(noext, fname);
  if ((s = strrchr(noext, '.')) != NULL) *s = 0;  /* remove ext */
  sprintf(outfile, "%s_ompi.c", noext);

  /* Preprocess
   */
  strarglist(preflags, &prep_args, SLEN);
#if defined(__SYSOS_cygwin) && defined(__SYSCOMPILER_cygwin)
  /* Hack for CYGWIN gcc */
  sprintf(cmd, "%s -U__CYGWIN__ -D__extension__=  -U__GNUC__ -D_OPENMP=200805"
               " %s -I%s %s \"%s\" > \"%s.pc\"",
               PREPROCESSOR, CPPFLAGS, IncludeDir, preflags, fname, noext);
#else
  sprintf(cmd, "%s -U__GNUC__ -D_OPENMP=200805 %s -I%s %s \"%s\" > \"%s.pc\"",
               PREPROCESSOR, CPPFLAGS, IncludeDir, preflags, fname, noext);
#endif
  if (verbose)
    fprintf(stderr, "====> Preprocessing file (%s.c)\n  [ %s ]\n", noext, cmd);
  if ((res = system(cmd)) != 0)
    _exit(res);

  /* Transform 
   */
  sprintf(cmd, "%s \"%s.pc\" __ompi__%s%s%s%s%s> \"%s\"", RealOmpiName, noext,
          strstr(CFLAGS, "OMPI_MAIN=LIB") ? " --nomain " : " ",
          strstr(CFLAGS, "OMPI_MEMMODEL=PROC") == NULL ? " " :
              strstr(CFLAGS, "OMPI_MEMMODEL=THR") ? " --procs --threads " :
                                                    " --procs ",
          disableOpenMP ? " --nomp " : " ",
          disableOmpix ? " --nox " : " ",
	  disableCodeDuplication ? " --nocodedup " : " ",
          outfile);
  if (verbose)
    fprintf(stderr, "====> Transforming file (%s.c)\n  [ %s ]\n", noext, cmd);
  if ((res = system(cmd)) > 0)
    res = WEXITSTATUS(res);
  if (keep < 2)
  {
    sprintf(cmd, "%s.pc", noext);            /* remove preprocessed file */
    unlink(cmd);
  }
  if (res == 33)                                /* no pragma omp directives */
  {
    FILE *of = fopen(outfile, "w");
    if (of == NULL)
    {
      fprintf(stderr, "Cannot write to intermediate file.\n");
      _exit(1);
    }
    fprintf(of, "# 1 \"%s.c\"\n", noext);     /* Identify the original file */
    fclose(of);
    sprintf(cmd, "cat \"%s.c\" >> \"%s\"", noext, outfile);
    if (system(cmd) != 0)
    {
      unlink(outfile);
      _exit(res);
    }
      
  }
  else if (res != 0)
  {
    if (!keep)
      unlink(outfile);
    _exit(res);
  }

  /* Compile the transformed file
   */
  strarglist(strscc_flags, &scc_flags, LEN);
  sprintf(cmd, "%s \"%s\" -c %s -I%s %s %s",
               COMPILER, outfile, CFLAGS, IncludeDir, preflags, strscc_flags);
  if (verbose)
    fprintf(stderr, "====> Compiling file (%s):\n  [ %s ]\n", outfile, cmd);
  res = system(cmd);
  if (!keep) 
    unlink(outfile);
  if (res != 0)
    _exit(res);

  /* Settle the output file name
   */
  strarglist(strgoutfile, &goutfile, LEN);
  strcpy(noext, get_basename(fname));
  if ((s = strrchr(noext, '.')) != NULL) *s = 0;  /* remove ext */
  if (goutfile.head != NULL && !mustlink)
    strcpy(outfile, goutfile.head->val);
  else
    sprintf(outfile, "%s.o", noext);
  strcat(noext, "_ompi.o");
  if (verbose)
    fprintf(stderr, "====> Renaming file \"%s\" to \"%s\"\n",
                    noext, outfile);
  rename(noext, outfile);
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                           *
 *        LINKING                                            *
 *                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


void ompicc_link()
{
  arg_t *p;
  char cur_obj[SLEN], tmp[SLEN], cmd[LEN];
  char objects[LEN], *obj;
  char strsccargs[LEN], strlinkargs[LEN], strgoutfile[LEN], strprepargs[LEN];
  int len, is_tmp;
  char rm_obj[LEN];

  obj = objects;
  *obj = 0;
  strcpy(rm_obj, "rm -f ");
  for (p = files.head; p != NULL; p = p->next)
  {
    strcpy(cur_obj, p->val);
    is_tmp = 0;
    len = strlen(cur_obj);
    if (cur_obj[len-1] == 'c')
      is_tmp = 1;
    if (is_tmp)
    {
      cur_obj[len-2] = 0;
      strcpy(tmp, cur_obj);
      strcpy(cur_obj, get_basename(tmp));
      strcat(cur_obj, ".o");
      strcat(rm_obj, cur_obj);
      strcat(rm_obj, " ");
    }
    sprintf(obj, "\"%s\" ", cur_obj);
    obj += strlen(cur_obj)+3;
  }

  strarglist(strsccargs,  &scc_flags, LEN);
  strarglist(strlinkargs, &link_args, LEN);
  strarglist(strgoutfile, &goutfile, LEN);
  strarglist(strprepargs, &prep_args, LEN);

  /* We have to include -lort 2 times due to circular dependencies
   * with the threading libraries.
   */
  sprintf(cmd, 
          "%s %s %s -I%s %s %s %s -L%s -L%s/%s -lort %s %s -lort",
          COMPILER, objects, CFLAGS, IncludeDir, strprepargs, strsccargs,
          strgoutfile, LibDir, LibDir, ortlibname, LDFLAGS, strlinkargs);
  if (verbose)
    fprintf(stderr, "====> Linking:\n  [ %s ]\n", cmd);
  if (system(cmd) != 0)
    fprintf(stderr, "Error: could not perform linking.\n");
  system(rm_obj);   /* Remove unnecessary files */
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                           *
 *        THE MAIN() PART                                    *
 *                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


/* Get values from environmental variables if they exist,
 * otherwise grab the configuration values (from Makefile.am)
 */
void ompicc_get_envvars()
{
  char *t;

  if ((t = getenv("OMPI_CPP")) == NULL)
    strncpy(PREPROCESSOR, CPPcmd, 511);          
  else
    strncpy(PREPROCESSOR, t, 511);

  if ((t = getenv("OMPI_CPPFLAGS")) == NULL)
    strncpy(CPPFLAGS, PreprocFlags, 511);
  else
    strncpy(CPPFLAGS, t, 511);
    
  if ((t = getenv("OMPI_CC")) == NULL)
    strncpy(COMPILER, CCcmd, 511);         
  else
    strncpy(COMPILER, t, 511);

  if ((t = getenv("OMPI_CFLAGS")) == NULL)
    strncpy(CFLAGS, CompileFlags, 511);
  else
    strncpy(CFLAGS, t, 511);
    
  if ((t = getenv("OMPI_LDFLAGS")) == NULL)
    strncpy(LDFLAGS, LinkFlags, 511);
  else
    strncpy(LDFLAGS, t, 511);
}


/* Read the runtime library's configuration file
 */
void get_ort_flags()
{
  char confpath[PATHSIZE];
  FILE *fp;
  void setflag(char*,char*), conffile_read(FILE *fp, void (*act)(char*,char*));

  snprintf(confpath, PATHSIZE-1, "%s/%s/ortconf.%s", LibDir, ortlibname, ortlibname);
  if ((fp = fopen(confpath, "r")) == NULL)
    ompicc_error(1, "library `%s' cannot be found\n  (%s is missing)\n",
                    ortlibname,confpath);
  conffile_read(fp, setflag);
  fclose(fp);
}


void setflag(char *key, char *value)
{
  if (strcmp(key, "ORTINFO") == 0 && strlen(value) + strlen(ORTINFO) < FLAGSIZE)
    strcat(*ORTINFO ? strcat(ORTINFO, " ") : ORTINFO, value);
  if (strcmp(key, "CPPFLAGS") == 0 && strlen(value) + strlen(CPPFLAGS) < FLAGSIZE)
    strcat(strcat(CPPFLAGS, " "), value);
  if (strcmp(key, "CFLAGS") == 0 && strlen(value) + strlen(CFLAGS) < FLAGSIZE)
    strcat(strcat(CFLAGS, " "), value);
  if (strcmp(key, "LDFLAGS") == 0 && strlen(value) + strlen(LDFLAGS) < FLAGSIZE)
    strcat(strcat(LDFLAGS, " "), value);
}


void get_path(char *argv0, char *path)
{
  int i;

  memset(path, '\0', PATHSIZE);     

  for (i = strlen(argv0); i >= 0; i--) {
    if (argv0[i] == '/') {
      strncpy(path, argv0, i+1);
      path[i+1]='\0';
      break;                               
    }
  }

}

int main(int argc, char **argv)
{
  arg_t *p;
#if defined(HAVE_REALPATH)
  char  argv0[PATHSIZE];
  char  path[PATHSIZE];           
  char  *res; 

  strcpy(argv0, "");                
  res = realpath(argv[0], argv0);
  if (res == NULL) {
    strcpy(RealOmpiName, OmpiName);
  }
  else {
    get_path(argv0, path);         /* path before ompicc */
    strcpy(RealOmpiName, path);
    strcat(RealOmpiName, OmpiName);
  }
#else
  strcpy(RealOmpiName, OmpiName);
#endif

  ompicc_get_envvars();
  parse_args(argc, argv);
  get_ort_flags();

  if (argc == 1)
  {
      ompi_info();
      fprintf(stderr,
        "\nUsage: %s [ompi options] [system compiler options] programfile(s)\n",
        argv[0]);
      fprintf(stderr,
        "\n"
        "   OMPi options:\n"
        "                  -k: keep intermediate file\n"
        "                  -v: be verbose (show the actual steps)\n"
        "        --ort=<name>: use a specific OMPi runtime library\n"
        "              --nomp: ignore OpenMP constructs\n"
        "               --nox: ignore OMPi extensions\n"
        "\n"
        "Use environmental variables OMPI_CPP, OMPI_CC, OMPI_CPPFLAGS,\n"
        "OMPI_CFLAGS, OMPI_LDFLAGS to have OMPi use a particular base\n"
        "preprocessor and compiler, along with specific flags.\n");
      exit(0);
  }

  if (files.head == NULL)
  {
    fprintf(stderr,
           "No input file specified; run ompicc with no arguments for help.\n");
    exit(0);
  }

  if (verbose) ompi_info();
  for (p = files.head; p != NULL; p = p->next)
    ompicc_compile(p->val);
  if (mustlink)
    ompicc_link();
  return (0);
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                           *
 *        CONFIGURATION FILE READER                          *
 *                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


/* A rudimentary ortconf file reader.
 * The configuration file is assumed to hold lines of the form
 *    keyword = string
 * Any leading & trailing spaces from "string" get removed.
 * Any lines starting with # are considered comments and are ignored.
 * Any other (wrong) lines are ignored.
 *
 * Use it by calling conffile_read(fp, act).
 * For each correct line found, it calls act(keyword, string).
 */

#include <ctype.h>

/*
 * The scanner
 */

 
static FILE *infile;
static int lastchar;


static void sc_init_scanner(FILE *fp)
{
  infile   = fp;
  lastchar = '\n';
}


static void sc_nextchar()
{
  lastchar = fgetc(infile);
}


static void sc_ignore_line()
{
  for (; lastchar != '\n' && lastchar != EOF; sc_nextchar())
    ;
  if (lastchar == '\n')
    sc_nextchar();
}


static void sc_skip_blank_lines()
{
  while (1)
  {
    while (isspace(lastchar))
      sc_nextchar();

    if (lastchar == '#')       /* Comment line */
      sc_ignore_line();
    else
      break;
  }
}


static void sc_skip_spaces()
{
  while ( isspace(lastchar) )
    sc_nextchar();
}


static int sc_get_word(int n, char *word)
{
  int len;

  sc_skip_spaces();

  if (lastchar == EOF)
    return (EOF);

  if (lastchar == '=')
  {
    sc_nextchar();
    word[0] = '=';
    word[1] = 0;
    return (0);
  }

  for (len = 0; !isspace(lastchar) && len < n-1; sc_nextchar())
    if (lastchar == EOF || lastchar == '=')
      break;
    else
      word[len++] = lastchar;

  word[len++] = 0;
  return (0);
}


static void sc_get_rest_of_line(int n, char *buf)
{
  int i = 0;

  for (; lastchar != '\n' && lastchar != EOF && i < n-1; sc_nextchar())
    buf[i++] = lastchar;
  buf[i++] = 0;
  if (lastchar == EOF)
    return;
  sc_ignore_line();           /* Discard remaining line */

  /* Remove trailing spaces */
  for (i -= 2; i >= 0; i--)
    if (!isspace(buf[i]))
      break;
  buf[i+1] = 0;
}


/*
 * The driver
 */

 
#define KEYWORDLEN 512
#define VALUELEN   1024


void conffile_read(FILE *fp, void (*act)(char *, char *))
{
  char keyw[KEYWORDLEN], value[VALUELEN];

  for (sc_init_scanner(fp); ; )
  {
    sc_skip_blank_lines();

    if ( sc_get_word(KEYWORDLEN, keyw) == EOF ) return;
    if ( sc_get_word(VALUELEN, value) == EOF ) return;
    if (strcmp(value, "=")) goto PARSEERROR;

    sc_skip_spaces();
    sc_get_rest_of_line(VALUELEN, value);

    act(keyw, value);
    continue;

    PARSEERROR:  sc_ignore_line();
  }
}
